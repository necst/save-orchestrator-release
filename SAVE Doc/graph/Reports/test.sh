#!/bin/bash
echo "PCC"
(cd ./CorrApp-build && ./CorrApp -n 300 -p 5000 -k 50 -h 5 -v 5 > /dev/null && cd ..) &
sleep 5
echo "BS"
(cd ./BlackScholesApp-build && ./BlackScholesApp -n 300 -d 6000000 > /dev/null && cd ..) &
sleep 3
echo "SQ1"
(cd ./SquareApp-build ./SquareApp -n 200 -d 100000000 > /dev/null && cd ..) &
echo "SQ2"
(cd ./SquareApp2-build ./SquareApp -n 200 -d 100000000 > /dev/null && cd ..) &
sleep 2
echo "Twist"
(cd ./ImageProcessingApp1-build ./ImageProcessingApp1 -i lena -o lena -s 1 -e 1 -t OPENMP -p ../ImageProcessingApp/image > /dev/null && cd ..) &
echo "Motion"
(cd ./ImageProcessingApp2-build ./ImageProcessingApp2 -i frame -o frame -s 0 -e 5 -t OPENMP -p ../ImageProcessingApp/image > /dev/null && cd ..) &
echo "all Done."
exit