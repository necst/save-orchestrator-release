//
//  main.cpp
//  SampleApp
//
//  Created by Raegal on 16/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>
#include <csignal>

#ifdef __APPLE__
#else
#include <omp.h>
#endif

#include <sys/time.h>

#include "OpenCLDevice.h"
#include "AppPerformanceMonitor.h"
#include "ReportMonitor.h"


#define OPT_N               6000000
#define NUM_ITERATIONS      100

float RandFloat(float low, float high) {
    float t = (float)rand() / (float)RAND_MAX;
    return (1.0f - t) * low + t * high;
}


/***************** Options ***********************/

typedef struct globalArgs_t{
    unsigned int numberOfIterations;
    unsigned int numberOfData;
    double minTrhoughput;
    double maxTrhoughput;
    std::string reportFileName;
    bool sync;

} GlobalArgs;

static const char *optString = "n:d:l:u:r:s:";

static struct option longOpts[] = {
    { "numberOfIterations", required_argument, NULL, 'n' },
    { "numberOfData", required_argument, NULL, 'd' },
    { "minTrhoughput", required_argument, NULL, 'l' },
    { "maxTrhoughput", required_argument, NULL, 'u' },
    { "reportFileName", required_argument, NULL, 'r' },
    { "sync", required_argument, NULL, 's' }
};


GlobalArgs globalArgs;


void parseCommmand(int argc, char** argv){
    int longIndex;
    int opt;
    
    while((opt = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1 ) {
        switch( opt ) {
                
            case 'n':
                globalArgs.numberOfIterations = std::atoi(optarg);
                break;
                
            case 'd':
                globalArgs.numberOfData = std::atoi(optarg);
                break;
                
            case 'l':
                globalArgs.minTrhoughput = std::atof(optarg);
                break;
                
            case 'u':
                globalArgs.maxTrhoughput = std::atof(optarg);
                break;
                
            case 'r':
                globalArgs.reportFileName = optarg;
                break;
                
            case 's':
                globalArgs.sync = std::atoi(optarg);
                break;
                
            default:
                break;
        }
    }
    
    
}

/***************** End Options ********************/


/************* OpenCL ****************************/

void initOpenCLKernel(OpenCLDevice::OpenCLDevicePtr device) {
    device->addKernel("blackScholes");
}

/************* End OpenCL ************************/


/************* CPU Kernel ******************+*****/


///////////////////////////////////////////////////////////////////////////////
// Polynomial approximation of cumulative normal distribution function
///////////////////////////////////////////////////////////////////////////////
static double CND(double d)
{
    const double       A1 = 0.31938153;
    const double       A2 = -0.356563782;
    const double       A3 = 1.781477937;
    const double       A4 = -1.821255978;
    const double       A5 = 1.330274429;
    const double RSQRT2PI = 0.39894228040143267793994605993438;
    
    double
    K = 1.0 / (1.0 + 0.2316419 * fabs(d));
    
    double
    cnd = RSQRT2PI * exp(- 0.5 * d * d) *
    (K * (A1 + K * (A2 + K * (A3 + K * (A4 + K * A5)))));
    
    if (d > 0)
        cnd = 1.0 - cnd;
    
    return cnd;
}


///////////////////////////////////////////////////////////////////////////////
// Black-Scholes formula for both call and put
///////////////////////////////////////////////////////////////////////////////
void blackScholesCPU(float &callResult, float &putResult, float stockPrice, float optionStrike, float optionYears, float risklessRate, float volatilityRate) {
    
    double S = stockPrice, X = optionStrike, T = optionYears, R = risklessRate, V = volatilityRate;
    
    double sqrtT = sqrt(T);
    double    d1 = (log(S / X) + (R + 0.5 * V * V) * T) / (V * sqrtT);
    double    d2 = d1 - V * sqrtT;
    double CNDD1 = CND(d1);
    double CNDD2 = CND(d2);
    
    //Calculate Call and Put simultaneously
    double expRT = exp(- R * T);
    callResult   = (float)(S * CNDD1 - X * expRT * CNDD2);
    putResult    = (float)(X * expRT * (1.0 - CNDD2) - S * (1.0 - CNDD1));
}

/************* End CPU Kernel ********************/


/************* GPU OpenCL Kernel *****************/

void blackScholes_OpenCL_GPU(float * callResult, float * putResult, float * stockPrice, float * optionStrike, float * optionYears, float risklessRate, float volatilityRate, OpenCLDevice::OpenCLDevicePtr &openCLInfo, ReportMonitor &report){
    
    cl_int ret;
    
    report.startSubItTimer("clCreateBuffer");
    
    cl_mem mem_callResult = clCreateBuffer(openCLInfo->context, CL_MEM_WRITE_ONLY ,sizeof(cl_float) * globalArgs.numberOfData, NULL, &ret);
    cl_mem mem_putResult = clCreateBuffer(openCLInfo->context, CL_MEM_WRITE_ONLY ,sizeof(cl_float) * globalArgs.numberOfData, NULL, &ret);
    cl_mem mem_stockPrice = clCreateBuffer(openCLInfo->context, CL_MEM_READ_ONLY ,sizeof(cl_float) * globalArgs.numberOfData, NULL, &ret);
    cl_mem mem_optionStrike = clCreateBuffer(openCLInfo->context, CL_MEM_READ_ONLY ,sizeof(cl_float) * globalArgs.numberOfData, NULL, &ret);
    cl_mem mem_optionYears = clCreateBuffer(openCLInfo->context, CL_MEM_READ_ONLY ,sizeof(cl_float) * globalArgs.numberOfData, NULL, &ret);
    cl_mem mem_risklessRate = clCreateBuffer(openCLInfo->context, CL_MEM_READ_ONLY ,sizeof(cl_float), NULL, &ret);
    cl_mem mem_volatilityRate = clCreateBuffer(openCLInfo->context, CL_MEM_READ_ONLY ,sizeof(cl_float), NULL, &ret);
    
    if (globalArgs.sync) {
        ret = clFinish(openCLInfo->command_queue);
    }
    
    report.stopSubItTimer();
    
    report.startSubItTimer("clEnqueueWriteBuffer");

    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_callResult, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), callResult, 0, NULL, NULL);
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_putResult, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), putResult, 0, NULL, NULL);
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_stockPrice, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), stockPrice, 0, NULL, NULL);
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_optionStrike, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), optionStrike, 0, NULL, NULL);
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_optionYears, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), optionYears, 0, NULL, NULL);
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_risklessRate, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), &risklessRate, 0, NULL, NULL);
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_volatilityRate, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), &volatilityRate, 0, NULL, NULL);

    if (globalArgs.sync) {
        ret = clFinish(openCLInfo->command_queue);
    }
    
    report.stopSubItTimer();

    
    report.startSubItTimer("clSetKernelArg");

    ret = clSetKernelArg(openCLInfo->kernels["blackScholes"], 0, sizeof(cl_mem), &mem_callResult);
    ret = clSetKernelArg(openCLInfo->kernels["blackScholes"], 1, sizeof(cl_mem), &mem_putResult);
    ret = clSetKernelArg(openCLInfo->kernels["blackScholes"], 2, sizeof(cl_mem), &mem_stockPrice);
    ret = clSetKernelArg(openCLInfo->kernels["blackScholes"], 3, sizeof(cl_mem), &mem_optionStrike);
    ret = clSetKernelArg(openCLInfo->kernels["blackScholes"], 4, sizeof(cl_mem), &mem_optionYears);
    ret = clSetKernelArg(openCLInfo->kernels["blackScholes"], 5, sizeof(cl_mem), &mem_risklessRate);
    ret = clSetKernelArg(openCLInfo->kernels["blackScholes"], 6, sizeof(cl_mem), &mem_volatilityRate);
    
    if (globalArgs.sync) {
        ret = clFinish(openCLInfo->command_queue);
    }
    
    report.stopSubItTimer();

    report.startSubItTimer("clEnqueueNDRangeKernel");

    /*Execute Kernels*/
    size_t global_work_size[3] = {globalArgs.numberOfData,1,1}; //set number of item per dimension
    
    ret = clEnqueueNDRangeKernel(openCLInfo->command_queue, openCLInfo->kernels["blackScholes"], 1, NULL, global_work_size, NULL, 0, NULL, NULL);
        
    std::cout << "Enqueue status: " << ret << std::endl;
    
    if (globalArgs.sync) {
        ret = clFinish(openCLInfo->command_queue);
    }
    
    report.stopSubItTimer();

    
    report.startSubItTimer("clEnqueueReadBuffer");

    /* Copy output data from the output memory buffer */
    ret = clEnqueueReadBuffer(openCLInfo->command_queue, mem_callResult, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), callResult, 0, NULL, NULL);
    ret = clEnqueueReadBuffer(openCLInfo->command_queue, mem_putResult, CL_FALSE, 0, globalArgs.numberOfData*sizeof(cl_float), putResult, 0, NULL, NULL);
    
    if (globalArgs.sync) {
        ret = clFinish(openCLInfo->command_queue);
    }
    
    report.stopSubItTimer();

    report.startSubItTimer("clReleaseMemObject");

    /* Destroy Buffer Object */
    ret = clReleaseMemObject(mem_callResult);
    ret = clReleaseMemObject(mem_putResult);
    ret = clReleaseMemObject(mem_stockPrice);
    ret = clReleaseMemObject(mem_optionStrike);
    ret = clReleaseMemObject(mem_optionYears);
    ret = clReleaseMemObject(mem_risklessRate);
    ret = clReleaseMemObject(mem_volatilityRate);
    
    ret = clFinish(openCLInfo->command_queue);

    report.stopSubItTimer();
}

/************* End GPU OpenCL Kernel *************/


int main(int argc, char** argv) {
    
    parseCommmand(argc, argv);
    
    OpenCLDevice::OpenCLDeviceMap openCLDeviceList = OpenCLDevice::openCLDeviceList("./kernel.cl");
    OpenCLDevice::OpenCLDevicePtr device = openCLDeviceList[GPUScheduleType*2][0];
    
    ReportMonitor report(globalArgs.reportFileName);
    
    report.startAllocTimer();
    
    int i;
    
    float * h_CallResultCPU = (float *)malloc(globalArgs.numberOfData * sizeof(cl_float));
    float * h_PutResultCPU  = (float *)malloc(globalArgs.numberOfData * sizeof(cl_float));
    
    float * h_CallResultGPU = (float *)malloc(globalArgs.numberOfData * sizeof(cl_float));
    float * h_PutResultGPU  = (float *)malloc(globalArgs.numberOfData * sizeof(cl_float));
    
    float * h_StockPrice    = (float *)malloc(globalArgs.numberOfData * sizeof(cl_float));
    float * h_OptionStrike  = (float *)malloc(globalArgs.numberOfData * sizeof(cl_float));
    float * h_OptionYears   = (float *)malloc(globalArgs.numberOfData * sizeof(cl_float));
    
    const float riskFree = 0.02f;
    const float volatility = 0.30f;
    
    //**** GPU
    initOpenCLKernel(device);
    //********
    
    /*Create Fake data*/
    
    srand48(time(NULL));
    
    //Generate options set
    for (i = 0; i < globalArgs.numberOfData; i++) {
        
        h_CallResultCPU[i] = 0.0f;
        h_PutResultCPU[i]  = -1.0f;
        
        h_CallResultGPU[i] = 0.0f;
        h_PutResultGPU[i]  = -1.0f;
        
        h_StockPrice[i]    = RandFloat(5.0f, 30.0f);
        h_OptionStrike[i]  = RandFloat(1.0f, 100.0f);
        h_OptionYears[i]   = RandFloat(0.25f, 10.0f);
    }
    
    /******************/
    
    AppPerformanceMonitor * performanceMonitor = new AppPerformanceMonitor("blackScholes");
    AppScheduleMonitor * schedulerMonitor = new AppScheduleMonitor("blackScholes");
    
    
    /*CPU Implementation*/
    ImplementationType cpuImpl = [&] () -> void {
        std::cout << "CPU" << std::endl;
        
        unsigned int nThread = schedulerMonitor->getNumberOfThread();
        #pragma omp parallel for num_threads(nThread)
        for (int j = 0; j < globalArgs.numberOfData; ++j) {
            blackScholesCPU(h_CallResultCPU[j], h_PutResultCPU[j], h_StockPrice[j], h_OptionStrike[j], h_OptionYears[j], riskFree, volatility);
        }
    };
    
    /*GPU Implementation*/
    ImplementationType gpuImpl = [&] () -> void {
        std::cout << "GPU" << std::endl;
        blackScholes_OpenCL_GPU(h_CallResultCPU, h_PutResultCPU, h_StockPrice, h_OptionStrike, h_OptionYears, riskFree, volatility, device, report);
    };
    
    
    bool performanceMonitorRegistrated = performanceMonitor->registerToOrchestrator();
    if (!performanceMonitorRegistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorRegistrated = schedulerMonitor->registerToOrchestrator();
    if (!schedulerMonitorRegistrated) {
        delete schedulerMonitor;
    }
    
    schedulerMonitor->registerImplementation(cpuImpl,CPUScheduleType);
    schedulerMonitor->registerImplementation(gpuImpl,GPUScheduleType);
    schedulerMonitor->setFastestImplementation(GPUScheduleType);
    schedulerMonitor->setSlowestImplementation(CPUScheduleType);
    
    
    ApplicationGoal applicationGoal;
    applicationGoal.trhoughput = 3.0;
    
    performanceMonitor->setApplicationGoal(applicationGoal);
    
    ApplicationConstraints applicationConstraint;
    applicationConstraint.trhoughputMin = globalArgs.minTrhoughput;
    applicationConstraint.trhoughputMax = globalArgs.maxTrhoughput;
    
    performanceMonitor->setApplicationGoal(applicationGoal);
    performanceMonitor->setApplicationConstraints(applicationConstraint);
    
    schedulerMonitor->setHasToBeProfiled(true);
    
    report.stopAllocTimer();
    
    report.startTimer();
    
    for (i = 0; i < globalArgs.numberOfIterations; ++i) {
        report.startItTimer();
        ChosenScheduleType type = schedulerMonitor->executeHeterogenousImplementations();
        performanceMonitor->incrementHeartbeat(1);
        report.stopItTimer();
        report.addIterationData(type, performanceMonitor->averageThroughput(), performanceMonitor->getHeartbeat(), schedulerMonitor->getNumberOfThread());
    }
    
    report.stopTimer();

    report.startDeallocTimer();
    
    bool performanceMonitorDeregistrated = performanceMonitor->deregisterToOrchestrator();
    if (performanceMonitorDeregistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorDeregistrated = schedulerMonitor->deregisterToOrchestrator();
    if (schedulerMonitorDeregistrated) {
        delete schedulerMonitor;
    }
    
    
    free(h_OptionYears);
    free(h_OptionStrike);
    free(h_StockPrice);
    free(h_PutResultCPU);
    free(h_CallResultCPU);
    
    report.stopDeallocTimer();

    report.reportOnDisk();
    
    return 0;
    
}
