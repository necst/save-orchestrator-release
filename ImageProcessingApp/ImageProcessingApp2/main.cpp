//
//  main.cpp
//  SampleApp
//
//  Created by Raegal on 16/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>
#include <sys/time.h>

#include "AppPerformanceMonitor.h"

#include "ImageProcessingHeaders.h"

#include "OpenCLImageProcessingKernels.h"
#include "OpenCLDevice.h"

#include "ReportMonitor.h"

#define TWIST_FACTOR 0.9
#define GAUSS_SIGMA 2.5



typedef enum{OPENCL_CPU = 0, OPENMP} CPU_Implementation;

typedef struct globalArgs_t{
    std::string imageNamePattern;
    std::string outputNamePattern;
    std::string pathToImages;
    unsigned int startNum;
    unsigned int endNum;
    CPU_Implementation target;
    double minTrhoughput;
    double maxTrhoughput;
    std::string reportFileName;
    bool sync;
} GlobalArgs;


static const char *optString = "i:o:b:e:t:p:l:u:r:s:";

static struct option longOpts[] = {
    { "imageNamePattern", required_argument, NULL, 'i' },
    { "outputNamePattern", required_argument, NULL, 'o' },
    { "startNum", required_argument, NULL, 'b' },
    { "endNum", required_argument, NULL, 'e' },
    { "target", required_argument, NULL, 't' },
    { "pathToImages", optional_argument, NULL, 'p' },
    { "minTrhoughput", required_argument, NULL, 'l' },
    { "maxTrhoughput", required_argument, NULL, 'u' },
    { "reportFileName", required_argument, NULL, 'r' },
    { "sync", required_argument, NULL, 's' }

};

GlobalArgs globalArgs;

CPU_Implementation cpuImplementationSelected(std::string type)
{
    if (type.compare("OPENCL_CPU") == 0)
        return OPENCL_CPU;
    
    return OPENMP;
}


void parseCommmand(int argc, char** argv){
    int longIndex;
    int opt;
    
    while((opt = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1 ) {
        switch( opt ) {
                
            case 'i':
                globalArgs.imageNamePattern = optarg;
                break;
                
            case 'o':
                globalArgs.outputNamePattern = optarg;
                break;
                
            case 'b':
                globalArgs.startNum = std::atoi(optarg);
                break;
                
            case 'e':
                globalArgs.endNum = std::atoi(optarg);
                break;
                
            case 't':
                globalArgs.target = cpuImplementationSelected(optarg);
                break;
                
            case 'p':
                globalArgs.pathToImages = optarg;
                break;
                
            case 'l':
                globalArgs.minTrhoughput = std::atof(optarg);
                break;
                
            case 'u':
                globalArgs.maxTrhoughput = std::atof(optarg);
                break;
                
            case 'r':
                globalArgs.reportFileName = optarg;
                break;
                
            case 's':
                globalArgs.sync = std::atoi(optarg);
                break;
        }
    }
    
    
}


/***************************************************/
/************* Open MP Implementation **************/
/***************************************************/

void openMPImplementation(GlobalArgs globalArgs, unsigned int nThread, ReportMonitor &report){
    sImage originalImage, outputImage, greyImage, filteredImage, motionImage, erosionImage, edgeImage, bgImage, bgGreyImage, bgFilteredImage;
    sImage *greyImagePtr, *bgGreyImagePtr, *swap;
    
    int rows = 0;
    int cols = 0;
    int depth = 0;
    int img = globalArgs.startNum;
    
    std::string fileName;
    
    if (globalArgs.pathToImages.length() == 0)
        fileName = MAKE_STRING(globalArgs.imageNamePattern << img << ".bmp");
    else
        fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << ".bmp");
    
    readImage(fileName.c_str(), &bgImage);
    
    initImage(&bgFilteredImage,bgImage.rows,bgImage.cols,bgImage.depth, NULL);
    initImage(&bgGreyImage,bgImage.rows,bgImage.cols,1, NULL);
    
    // process background image
    gaussian_filter(&bgImage, &bgFilteredImage, GAUSS_SIGMA, nThread);
    rgb2grey(&bgFilteredImage,&bgGreyImage);
    
    //setup intermediated and final images
    initImage(&outputImage,bgImage.rows,bgImage.cols,bgImage.depth, bgImage.header);
    initImage(&filteredImage,bgImage.rows,bgImage.cols,bgImage.depth, NULL);
    initImage(&greyImage,bgImage.rows,bgImage.cols,1, NULL);
    initImage(&motionImage,bgImage.rows,bgImage.cols,1, NULL);
    initImage(&erosionImage,bgImage.rows,bgImage.cols,1, NULL);
    initImage(&edgeImage,bgImage.rows,bgImage.cols,1, NULL);
    
    greyImagePtr=&greyImage;
    bgGreyImagePtr=&bgGreyImage;
    
    for(img = globalArgs.startNum+1; img <= globalArgs.endNum; img++){
        if (globalArgs.pathToImages.length() == 0)
            fileName = MAKE_STRING(globalArgs.imageNamePattern << img << ".bmp");
        else
            fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << ".bmp");
        
        
        std::cout << "Processing " << fileName << std::endl;
        
        readImage(fileName.c_str(), &originalImage);
        
        if(bgImage.rows!=originalImage.rows || bgImage.cols!=originalImage.cols || bgImage.depth!= originalImage.depth){
            std::cout << "Images with different size" << std::endl;
            exit(0);
        }
        
        //pipeline
        gaussian_filter(&originalImage, &filteredImage, GAUSS_SIGMA, nThread);
        rgb2grey(&filteredImage,greyImagePtr);
        motion_detection(greyImagePtr, bgGreyImagePtr, &motionImage, nThread);
        erosion_filter(&motionImage, &erosionImage, nThread);
        sobel_edge_detection(&erosionImage, &edgeImage, nThread);
        
        
        //grey2rgb(&outputImage, &edgeImage);
        for(int r=0; r < outputImage.rows; r++)
            for(int c=0; c < outputImage.cols; c++){
                if(edgeImage.data[r*outputImage.cols+c]>100){
                    outputImage.data[(r*outputImage.cols+c)*outputImage.depth] = 0;
                    outputImage.data[(r*outputImage.cols+c)*outputImage.depth+1] = 0;
                    outputImage.data[(r*outputImage.cols+c)*outputImage.depth+2] = 255;
                } else{
                    for(int d=0; d < outputImage.depth; d++)
                        outputImage.data[(r*outputImage.cols+c)*outputImage.depth+d] = originalImage.data[(r*outputImage.cols+c)*outputImage.depth+d];
                }
            }
        //writing file
        //std::cout << "writing back results..." << std::endl;
        if (globalArgs.pathToImages.length() == 0)
            fileName = MAKE_STRING(globalArgs.outputNamePattern << img << "_out.bmp");
        else
            fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << "_out.bmp");
        
        writeImage(fileName.c_str(), &outputImage);
        
        swap=greyImagePtr;
        greyImagePtr=bgGreyImagePtr;
        bgGreyImagePtr=swap;
        
        //free allocated memory
        //the original image have to be deleted always since memory is reinstantiated in the load function.
        //Do not delete other objects if they are necessary for other elaborations
        deleteImage(&originalImage);
    }
    
    deleteImage(&greyImage);
    deleteImage(&outputImage);
    deleteImage(&filteredImage);
    deleteImage(&motionImage);
    deleteImage(&erosionImage);
    deleteImage(&edgeImage);
    deleteImage(&bgImage);
    deleteImage(&bgGreyImage);
    deleteImage(&bgFilteredImage);
    
    // SUPPRESSED OUTPUT
    // std::cout << std::endl;
    
}


/***************************************************/
/*********** End Open MP Implementation ************/
/***************************************************/



/***************************************************/
/************* Open CL Implementation **************/
/***************************************************/

void initOpenCLKernel(OpenCLDevice::OpenCLDevicePtr device) {
    device->addKernel("rgb2grey");
    device->addKernel("edge_detection");
    device->addKernel("gaussian_filter");
    device->addKernel("motion_detection");
    device->addKernel("erosion_filter");
}




void openCLImplementation(GlobalArgs globalArgs, OpenCLDevice::OpenCLDevicePtr openCLDevice, ReportMonitor &report) {
    sImage originalImage, greyImage, outputImage, bgImage;
    int rows = 0;
    int cols = 0;
    int depth = 0;
    int img;
    
    std::string fileName;
    
    cl_int status = CL_SUCCESS;
    
    size_t size;
    size_t outSize;
    
    cl_mem inBuffer;
    cl_mem step1Buffer;
    cl_mem step2Buffer;
    cl_mem step3Buffer;
    cl_mem step4Buffer;
    cl_mem outBuffer;
    
    cl_mem bgBuffer;
    cl_mem bgStep1Buffer;
    cl_mem bgStep2Buffer;
    
    
    /* Init BG Image*/
    if (globalArgs.pathToImages.length() == 0)
        fileName = MAKE_STRING(globalArgs.imageNamePattern << globalArgs.startNum << ".bmp");
    else
        fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << globalArgs.startNum << ".bmp");
    
    readImage(fileName.c_str(), &bgImage);
    
    size = bgImage.rows*bgImage.cols*sizeof(unsigned char)*bgImage.depth;
    outSize = bgImage.rows*bgImage.cols*sizeof(unsigned char);
    
    rows = bgImage.rows;
    cols = bgImage.cols;
    depth = bgImage.depth;
    
    bgBuffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
    bgStep1Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
    bgStep2Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, outSize, 0, &status);
    
    status = clEnqueueWriteBuffer(openCLDevice->command_queue, bgBuffer, CL_TRUE, 0, size, bgImage.data, 0, NULL, NULL);
    status = clFinish(openCLDevice->command_queue);
    
    gaussian_filter(openCLDevice, bgBuffer, bgStep1Buffer, rows, cols, depth, GAUSS_SIGMA);
    rgb2grey(openCLDevice, bgStep1Buffer, bgStep2Buffer, rows, cols);
    
    
    inBuffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
    step1Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
    step2Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
    step3Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
    step4Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
    outBuffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, outSize, 0, &status);
    
    initImage(&outputImage,bgImage.rows,bgImage.cols,bgImage.depth, bgImage.header);
    initImage(&greyImage,bgImage.rows,bgImage.cols,1, NULL);
    
    for(img = globalArgs.startNum+1; img <= globalArgs.endNum; img++){
        
        if (globalArgs.pathToImages.length() == 0)
            fileName = MAKE_STRING(globalArgs.imageNamePattern << img << ".bmp");
        else
            fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << ".bmp");
        
        std::cout << "Processing " << fileName << std::endl;
        
        readImage(fileName.c_str(), &originalImage);
        
        if(rows!=bgImage.rows || cols!=bgImage.cols || depth!= bgImage.depth){
            std::cout << "Images with different size" << std::endl;
            exit(0);
        }
        
        /*Copy input buffer*/
        status = clEnqueueWriteBuffer(openCLDevice->command_queue, inBuffer, CL_TRUE, 0, size, originalImage.data, 0, NULL, NULL);
        //status = clFlush(openCLDevice.command_queue);
        status = clFinish(openCLDevice->command_queue);
        
        gaussian_filter(openCLDevice, inBuffer, step1Buffer, rows, cols, depth, GAUSS_SIGMA);
        rgb2grey(openCLDevice, step1Buffer, step2Buffer, rows, cols);
        motion_detection(openCLDevice, step2Buffer, step3Buffer, bgStep2Buffer, rows, cols);
        erosion_filter(openCLDevice, step3Buffer, step4Buffer, rows, cols);
        edge_detection(openCLDevice, step4Buffer, outBuffer, rows, cols);
        
        //status = clEnqueueReadBuffer(openCLDevice.command_queue, step3Buffer, CL_TRUE, 0, size, outputImage.data, 0, NULL, NULL);
        status = clEnqueueReadBuffer(openCLDevice->command_queue, outBuffer, CL_TRUE, 0, outSize, greyImage.data, 0, NULL, NULL);
        status = clFinish(openCLDevice->command_queue);
        
        //grey2rgb(&outputImage, &greyImage);
        
        for(int r=0; r < outputImage.rows; r++)
            for(int c=0; c < outputImage.cols; c++){
                if(greyImage.data[r*outputImage.cols+c]>100){
                    outputImage.data[(r*outputImage.cols+c)*outputImage.depth] = 0;
                    outputImage.data[(r*outputImage.cols+c)*outputImage.depth+1] = 0;
                    outputImage.data[(r*outputImage.cols+c)*outputImage.depth+2] = 255;
                } else{
                    for(int d=0; d < outputImage.depth; d++)
                        outputImage.data[(r*outputImage.cols+c)*outputImage.depth+d] = originalImage.data[(r*outputImage.cols+c)*outputImage.depth+d];
                }
            }
        
        if (globalArgs.pathToImages.length() == 0)
            fileName = MAKE_STRING(globalArgs.outputNamePattern << img << "_out.bmp");
        else
            fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << "_out.bmp");
        
        writeImage(fileName.c_str(), &outputImage);
        
        deleteImage(&originalImage);
        
        cl_mem temp = step2Buffer;
        step2Buffer = bgStep2Buffer;
        bgStep2Buffer = temp;
        
    }
    
    deleteImage(&greyImage);
    deleteImage(&outputImage);
    
}


/***************************************************/
/*********** End Open CL Implementation ************/
/***************************************************/

/******************************************************/
/************* Open CL-MP Implementation **************/
/******************************************************/

ChosenScheduleType previousSchedule;

typedef struct {
    sImage *greyImagePtr;
    sImage *bgGreyImagePtr;
    sImage *bgImagePtr;
    sImage *originalImagePtr;
    sImage *outputImagePtr;
    sImage *filteredImagePtr;
    sImage *motionImagePtr;
    sImage *erosionImagePtr;
    sImage *edgeImagePtr;
    sImage *bgFilteredImagePtr;
    
    cl_mem inBuffer = NULL;
    cl_mem step1Buffer = NULL;
    cl_mem step2Buffer = NULL;
    cl_mem step3Buffer = NULL;
    cl_mem step4Buffer = NULL;
    cl_mem outBuffer = NULL;
    
    cl_mem bgBuffer = NULL;
    cl_mem bgStep1Buffer = NULL;
    cl_mem bgStep2Buffer = NULL;
    
}PipelineStruct;


void createOpenCLBuffers(PipelineStruct &pipeline, OpenCLDevice::OpenCLDevicePtr openCLDevice) {
    
    size_t size = pipeline.bgImagePtr->rows*pipeline.bgImagePtr->cols*sizeof(unsigned char)*pipeline.bgImagePtr->depth;
    size_t outSize = pipeline.bgImagePtr->rows*pipeline.bgImagePtr->cols*sizeof(unsigned char);
    
    cl_int status = CL_SUCCESS;

    if (pipeline.bgBuffer == NULL) {
        
        pipeline.bgBuffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
        pipeline.bgStep1Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
        pipeline.bgStep2Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, outSize, 0, &status);
        
        pipeline.inBuffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
        pipeline.step1Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
        pipeline.step2Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
        pipeline.step3Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
        pipeline.step4Buffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, size, 0, &status);
        pipeline.outBuffer = clCreateBuffer(openCLDevice->context,CL_MEM_READ_WRITE, outSize, 0, &status);
    }
}

void deleteOpenCLBuffers(PipelineStruct &pipeline, OpenCLDevice::OpenCLDevicePtr openCLDevice) {
    cl_int status = CL_SUCCESS;

    if (pipeline.bgBuffer != NULL) {
        status = clReleaseMemObject(pipeline.bgBuffer);
        status = clReleaseMemObject(pipeline.bgStep1Buffer);
        status = clReleaseMemObject(pipeline.bgStep2Buffer);
        
        status = clReleaseMemObject(pipeline.inBuffer);
        status = clReleaseMemObject(pipeline.step1Buffer);
        status = clReleaseMemObject(pipeline.step2Buffer);
        status = clReleaseMemObject(pipeline.step3Buffer);
        status = clReleaseMemObject(pipeline.step4Buffer);
        status = clReleaseMemObject(pipeline.outBuffer);
        
        pipeline.inBuffer = NULL;
        pipeline.step1Buffer = NULL;
        pipeline.step2Buffer = NULL;
        pipeline.step3Buffer = NULL;
        pipeline.step4Buffer = NULL;
        pipeline.outBuffer = NULL;
        
        pipeline.bgBuffer = NULL;
        pipeline.bgStep1Buffer = NULL;
        pipeline.bgStep2Buffer = NULL;
    }
}

void initOpenCLbgImage(PipelineStruct &pipeline, OpenCLDevice::OpenCLDevicePtr openCLDevice, ReportMonitor &report){
    std::string fileName;
    
    int rows = 0;
    int cols = 0;
    int depth = 0;
    
    cl_int status = CL_SUCCESS;
    
    size_t size = pipeline.bgImagePtr->rows*pipeline.bgImagePtr->cols*sizeof(unsigned char)*pipeline.bgImagePtr->depth;

    rows = pipeline.bgImagePtr->rows;
    cols = pipeline.bgImagePtr->cols;
    depth = pipeline.bgImagePtr->depth;
    
    report.startSubItTimer("createOpenCLBuffers_OCL");
    createOpenCLBuffers(pipeline, openCLDevice);
    report.stopSubItTimer();

    report.startSubItTimer("clEnqueueWriteBuffer_OCL");
    status = clEnqueueWriteBuffer(openCLDevice->command_queue, pipeline.bgBuffer, CL_TRUE, 0, size, pipeline.bgImagePtr->data, 0, NULL, NULL);
    status = clFinish(openCLDevice->command_queue);
    report.stopSubItTimer();

    report.startSubItTimer("gaussian_filter_BG_OCL");
    gaussian_filter(openCLDevice, pipeline.bgBuffer, pipeline.bgStep1Buffer, rows, cols, depth, GAUSS_SIGMA);
    report.stopSubItTimer();

    report.startSubItTimer("rgb2grey_BG_OCL");
    rgb2grey(openCLDevice, pipeline.bgStep1Buffer, pipeline.bgStep2Buffer, rows, cols);
    report.stopSubItTimer();

}

void simpleOpenCLImplementation(GlobalArgs globalArgs, PipelineStruct &pipeline, int imgIndex, OpenCLDevice::OpenCLDevicePtr openCLDevice, ReportMonitor &report) {
    
    std::string fileName;
    
    int rows = 0;
    int cols = 0;
    int depth = 0;
    
    cl_int status = CL_SUCCESS;
    
    size_t size = pipeline.bgImagePtr->rows*pipeline.bgImagePtr->cols*sizeof(unsigned char)*pipeline.bgImagePtr->depth;
    size_t outSize = pipeline.bgImagePtr->rows*pipeline.bgImagePtr->cols*sizeof(unsigned char);
    
    rows = pipeline.bgImagePtr->rows;
    cols = pipeline.bgImagePtr->cols;
    depth = pipeline.bgImagePtr->depth;
    
    if (globalArgs.pathToImages.length() == 0)
        fileName = MAKE_STRING(globalArgs.imageNamePattern << imgIndex << ".bmp");
    else
        fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << imgIndex << ".bmp");
    
    std::cout << "Processing " << fileName << std::endl;
    
    report.startSubItTimer("readImage_OCL");
    readImage(fileName.c_str(), pipeline.originalImagePtr);
    report.stopSubItTimer();
    
    if(rows!=pipeline.bgImagePtr->rows || cols!=pipeline.bgImagePtr->cols || depth!= pipeline.bgImagePtr->depth){
        std::cout << "Images with different size" << std::endl;
        exit(0);
    }
    
    /*Copy input buffer*/
    report.startSubItTimer("clEnqueueWriteBuffer_OCL");
    status = clEnqueueWriteBuffer(openCLDevice->command_queue, pipeline.inBuffer, CL_TRUE, 0, size, pipeline.originalImagePtr->data, 0, NULL, NULL);
    //status = clFlush(openCLDevice.command_queue);
    status = clFinish(openCLDevice->command_queue);
    report.stopSubItTimer();

    report.startSubItTimer("gaussian_filter_OCL");
    gaussian_filter(openCLDevice, pipeline.inBuffer, pipeline.step1Buffer, rows, cols, depth, GAUSS_SIGMA);
    report.stopSubItTimer();

    report.startSubItTimer("rgb2grey_OCL");
    rgb2grey(openCLDevice, pipeline.step1Buffer, pipeline.step2Buffer, rows, cols);
    report.stopSubItTimer();

    report.startSubItTimer("motion_detection_OCL");
    motion_detection(openCLDevice, pipeline.step2Buffer, pipeline.step3Buffer, pipeline.bgStep2Buffer, rows, cols);
    report.stopSubItTimer();

    report.startSubItTimer("erosion_filter_OCL");
    erosion_filter(openCLDevice, pipeline.step3Buffer, pipeline.step4Buffer, rows, cols);
    report.stopSubItTimer();

    report.startSubItTimer("edge_detection_OCL");
    edge_detection(openCLDevice, pipeline.step4Buffer, pipeline.outBuffer, rows, cols);
    report.stopSubItTimer();

    //status = clEnqueueReadBuffer(openCLDevice.command_queue, step3Buffer, CL_TRUE, 0, size, outputImage.data, 0, NULL, NULL);

    report.startSubItTimer("clEnqueueReadBuffer_OCL");
    status = clEnqueueReadBuffer(openCLDevice->command_queue, pipeline.outBuffer, CL_TRUE, 0, outSize, pipeline.edgeImagePtr->data, 0, NULL, NULL);
    status = clFinish(openCLDevice->command_queue);
    report.stopSubItTimer();

    report.startSubItTimer("grey2rgb_OCL");

    //grey2rgb(&outputImage, &edgeImage);
    for(int r=0; r < pipeline.outputImagePtr->rows; r++)
        for(int c=0; c < pipeline.outputImagePtr->cols; c++){
            if(pipeline.edgeImagePtr->data[r*pipeline.outputImagePtr->cols+c]>100){
                pipeline.outputImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth] = 0;
                pipeline.outputImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth+1] = 0;
                pipeline.outputImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth+2] = 255;
            } else{
                for(int d=0; d < pipeline.outputImagePtr->depth; d++)
                    pipeline.outputImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth+d] = pipeline.originalImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth+d];
            }
        }
    report.stopSubItTimer();

    //writing file
    //std::cout << "writing back results..." << std::endl;
    if (globalArgs.pathToImages.length() == 0)
        fileName = MAKE_STRING(globalArgs.outputNamePattern << imgIndex << "_out.bmp");
    else
        fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << imgIndex << "_out.bmp");
    
    report.startSubItTimer("writeImage_OCL");
    writeImage(fileName.c_str(), pipeline.outputImagePtr);
    report.stopSubItTimer();

    cl_mem temp = pipeline.step2Buffer;
    pipeline.step2Buffer = pipeline.bgStep2Buffer;
    pipeline.bgStep2Buffer = temp;
    
}

void initOpenMPbgImage(GlobalArgs globalArgs, unsigned int nThread, PipelineStruct &pipeline, ReportMonitor &report){
    
    int img = globalArgs.startNum;
    
    std::string fileName;
    
    if (globalArgs.pathToImages.length() == 0)
        fileName = MAKE_STRING(globalArgs.imageNamePattern << img << ".bmp");
    else
        fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << ".bmp");
    
    report.startSubItTimer("readImage_OMP");
    readImage(fileName.c_str(), pipeline.bgImagePtr);
    report.stopSubItTimer();

    report.startSubItTimer("initImage_BG_OMP");
    initImage(pipeline.bgFilteredImagePtr,pipeline.bgImagePtr->rows,pipeline.bgImagePtr->cols,pipeline.bgImagePtr->depth, NULL);
    initImage(pipeline.bgGreyImagePtr,pipeline.bgImagePtr->rows,pipeline.bgImagePtr->cols,1, NULL);
    report.stopSubItTimer();

    // process background image
    report.startSubItTimer("gaussian_filter_BG_OMP");
    gaussian_filter(pipeline.bgImagePtr, pipeline.bgFilteredImagePtr, GAUSS_SIGMA, nThread);
    report.stopSubItTimer();

    report.startSubItTimer("rgb2grey_BG_OMP");
    rgb2grey(pipeline.bgFilteredImagePtr,pipeline.bgGreyImagePtr);
    report.stopSubItTimer();
    
    
    //setup intermediated and final images
    report.startSubItTimer("initImage_OMP");
    initImage(pipeline.outputImagePtr,pipeline.bgImagePtr->rows,pipeline.bgImagePtr->cols,pipeline.bgImagePtr->depth, pipeline.bgImagePtr->header);
    initImage(pipeline.filteredImagePtr,pipeline.bgImagePtr->rows,pipeline.bgImagePtr->cols,pipeline.bgImagePtr->depth, NULL);
    initImage(pipeline.greyImagePtr,pipeline.bgImagePtr->rows,pipeline.bgImagePtr->cols,1, NULL);
    initImage(pipeline.motionImagePtr,pipeline.bgImagePtr->rows,pipeline.bgImagePtr->cols,1, NULL);
    initImage(pipeline.erosionImagePtr,pipeline.bgImagePtr->rows,pipeline.bgImagePtr->cols,1, NULL);
    initImage(pipeline.edgeImagePtr,pipeline.bgImagePtr->rows,pipeline.bgImagePtr->cols,1, NULL);
    report.stopSubItTimer();

    
}

void simpleOpenMPImplementation(GlobalArgs globalArgs, unsigned int nThread, PipelineStruct &pipeline, int imgIndex, ReportMonitor &report) {
    
    sImage *swap;
    
    std::string fileName;

    if (globalArgs.pathToImages.length() == 0)
        fileName = MAKE_STRING(globalArgs.imageNamePattern << imgIndex << ".bmp");
    else
        fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << imgIndex << ".bmp");
    
    std::cout << "Processing " << fileName << std::endl;
    
    report.startSubItTimer("initImage_OMP");
    readImage(fileName.c_str(), pipeline.originalImagePtr);
    report.stopSubItTimer();

    if(pipeline.bgImagePtr->rows!=pipeline.originalImagePtr->rows || pipeline.bgImagePtr->cols!=pipeline.originalImagePtr->cols || pipeline.bgImagePtr->depth!=pipeline.originalImagePtr->depth){
        std::cout << "Images with different size" << std::endl;
        exit(0);
    }
    
    //pipeline
    report.startSubItTimer("gaussian_filter_OMP");
    gaussian_filter(pipeline.originalImagePtr, pipeline.filteredImagePtr, GAUSS_SIGMA, nThread);
    report.stopSubItTimer();

    report.startSubItTimer("rgb2grey_OMP");
    rgb2grey(pipeline.filteredImagePtr,pipeline.greyImagePtr);
    report.stopSubItTimer();

    report.startSubItTimer("motion_detection_OMP");
    motion_detection(pipeline.greyImagePtr, pipeline.bgGreyImagePtr, pipeline.motionImagePtr, nThread);
    report.stopSubItTimer();

    report.startSubItTimer("erosion_filter_OMP");
    erosion_filter(pipeline.motionImagePtr, pipeline.erosionImagePtr, nThread);
    report.stopSubItTimer();

    report.startSubItTimer("sobel_edge_detection_OMP");
    sobel_edge_detection(pipeline.erosionImagePtr, pipeline.edgeImagePtr, nThread);
    report.stopSubItTimer();

    report.startSubItTimer("grey2rgb_OMP");
    //grey2rgb(&outputImage, &edgeImage);
    for(int r=0; r < pipeline.outputImagePtr->rows; r++)
        for(int c=0; c < pipeline.outputImagePtr->cols; c++){
            if(pipeline.edgeImagePtr->data[r*pipeline.outputImagePtr->cols+c]>100){
                pipeline.outputImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth] = 0;
                pipeline.outputImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth+1] = 0;
                pipeline.outputImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth+2] = 255;
            } else{
                for(int d=0; d < pipeline.outputImagePtr->depth; d++)
                    pipeline.outputImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth+d] = pipeline.originalImagePtr->data[(r*pipeline.outputImagePtr->cols+c)*pipeline.outputImagePtr->depth+d];
            }
        }
    report.stopSubItTimer();
    
    //writing file
    //std::cout << "writing back results..." << std::endl;
    if (globalArgs.pathToImages.length() == 0)
        fileName = MAKE_STRING(globalArgs.outputNamePattern << imgIndex << "_out.bmp");
    else
        fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << imgIndex << "_out.bmp");
    
    report.startSubItTimer("writeImage_OMP");
    writeImage(fileName.c_str(), pipeline.outputImagePtr);
    report.stopSubItTimer();

    swap=pipeline.greyImagePtr;
    pipeline.greyImagePtr=pipeline.bgGreyImagePtr;
    pipeline.bgGreyImagePtr=swap;
    
}

void initPipeline(PipelineStruct &pipeline) {
    pipeline.greyImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.bgGreyImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.bgImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.originalImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.outputImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.filteredImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.motionImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.erosionImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.edgeImagePtr = (sImage*)malloc(sizeof(sImage));
    pipeline.bgFilteredImagePtr = (sImage*)malloc(sizeof(sImage));
}

void deletePipeline(PipelineStruct &pipeline) {
    deleteImage(pipeline.greyImagePtr);
    deleteImage(pipeline.bgGreyImagePtr);
    deleteImage(pipeline.bgImagePtr);
    deleteImage(pipeline.outputImagePtr);
    deleteImage(pipeline.filteredImagePtr);
    deleteImage(pipeline.motionImagePtr);
    deleteImage(pipeline.erosionImagePtr);
    deleteImage(pipeline.edgeImagePtr);
    deleteImage(pipeline.bgFilteredImagePtr);
    
    delete pipeline.greyImagePtr;
    delete pipeline.bgGreyImagePtr;
    delete pipeline.bgImagePtr;
    delete pipeline.originalImagePtr;
    delete pipeline.outputImagePtr;
    delete pipeline.filteredImagePtr;
    delete pipeline.motionImagePtr;
    delete pipeline.erosionImagePtr;
    delete pipeline.edgeImagePtr;
    delete pipeline.bgFilteredImagePtr;
}

/******************************************************/
/*********** End Open CL-MP Implementation ************/
/******************************************************/


int main(int argc, char** argv){
    
    parseCommmand(argc,argv);
    
    ReportMonitor report = ReportMonitor(globalArgs.reportFileName);

    report.startAllocTimer();
    
    OpenCLDevice::OpenCLDeviceMap openCLDeviceList = OpenCLDevice::openCLDeviceList("./kernels.cl");

    int i;
    
    OpenCLDevice::OpenCLDevicePtr deviceCPU;
    if (globalArgs.target == OPENCL_CPU) {
        std::cout << "Init CPU device" << std::endl;
        deviceCPU = openCLDeviceList[CPUScheduleType*2][0];
        initOpenCLKernel(deviceCPU);
    }
    
    std::cout << "Init GPU device" << std::endl;
    
    OpenCLDevice::OpenCLDevicePtr deviceGPU = openCLDeviceList[GPUScheduleType*2][0];
    
    //OpenCLDevice deviceGPU = OpenCLDevice(CL_DEVICE_TYPE_GPU,"./kernels.cl");
    initOpenCLKernel(deviceGPU);

    
    AppPerformanceMonitor * performanceMonitor = new AppPerformanceMonitor("test");
    AppScheduleMonitor * schedulerMonitor = new AppScheduleMonitor("test");
    
    
    ApplicationGoal applicationGoal;
    applicationGoal.trhoughput = 0.7;
    
    performanceMonitor->setApplicationGoal(applicationGoal);
    
    ApplicationConstraints applicationConstraint;
    applicationConstraint.trhoughputMin = globalArgs.minTrhoughput;
    applicationConstraint.trhoughputMax = globalArgs.maxTrhoughput;
    
    performanceMonitor->setApplicationGoal(applicationGoal);
    performanceMonitor->setApplicationConstraints(applicationConstraint);
    
    schedulerMonitor->setHasToBeProfiled(true);
    
    PipelineStruct pipeline;
    cl_int status = CL_SUCCESS;
    
    initPipeline(pipeline);
    initOpenMPbgImage(globalArgs, 4, pipeline, report);
    initOpenCLbgImage(pipeline, deviceGPU, report);
    
    size_t size = pipeline.bgImagePtr->rows*pipeline.bgImagePtr->cols*sizeof(unsigned char)*pipeline.bgImagePtr->depth;
    size_t outSize = pipeline.bgImagePtr->rows*pipeline.bgImagePtr->cols*sizeof(unsigned char);
    
    previousSchedule = schedulerMonitor->getChosenScheduleType();
    
    int img = globalArgs.startNum+1;
    
    /*CPU Implementation*/
    ImplementationType cpuImpl = [&] () -> void {
        std::cout << "CPU" << std::endl;
        
        if (previousSchedule == GPUScheduleType) {
            
            report.startSubItTimer("deleteOpenCLBuffers");
            
            status = clEnqueueReadBuffer(deviceGPU->command_queue, pipeline.bgStep2Buffer, CL_TRUE, 0, outSize, pipeline.bgGreyImagePtr->data, 0, NULL, NULL);
            status = clEnqueueReadBuffer(deviceGPU->command_queue, pipeline.step2Buffer, CL_TRUE, 0, size, pipeline.greyImagePtr->data, 0, NULL, NULL);
            
            deleteOpenCLBuffers(pipeline, deviceGPU);
            
            status = clFinish(deviceGPU->command_queue);
            
            report.stopSubItTimer();
            
        }
        
        previousSchedule = CPUScheduleType;
        
        simpleOpenMPImplementation(globalArgs, schedulerMonitor->getNumberOfThread(), pipeline, img, report);
    };
    
    /*GPU Implementation*/
    ImplementationType gpuImpl = [&] () -> void {
        std::cout << "GPU" << std::endl;
        
        if (previousSchedule == CPUScheduleType) {
            
            report.startSubItTimer("createOpenCLBuffers");
            
            createOpenCLBuffers(pipeline, deviceGPU);
            
            status = clEnqueueWriteBuffer(deviceGPU->command_queue, pipeline.step2Buffer, CL_TRUE, 0, size, pipeline.greyImagePtr->data, 0, NULL, NULL);
            status = clEnqueueWriteBuffer(deviceGPU->command_queue, pipeline.bgStep2Buffer, CL_TRUE, 0, outSize, pipeline.bgGreyImagePtr->data, 0, NULL, NULL);
            status = clFinish(deviceGPU->command_queue);
            
            report.stopSubItTimer();
            
        }
        
        previousSchedule = GPUScheduleType;
        
        simpleOpenCLImplementation(globalArgs, pipeline, img, deviceGPU, report);
    };
    
    
    schedulerMonitor->registerImplementation(cpuImpl,CPUScheduleType);
    schedulerMonitor->registerImplementation(gpuImpl,GPUScheduleType);
    
    
    
    bool performanceMonitorRegistrated = performanceMonitor->registerToOrchestrator();
    if (!performanceMonitorRegistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorRegistrated = schedulerMonitor->registerToOrchestrator();
    if (!schedulerMonitorRegistrated) {
        delete schedulerMonitor;
    }
    
    report.stopAllocTimer();
    
    report.startTimer();
    
    ChosenScheduleType type;
    
    for (i = 0; i<20; i++) {
        
        for(img = globalArgs.startNum+1;img <= globalArgs.endNum; img++){
            report.startItTimer();

            type = schedulerMonitor->executeHeterogenousImplementations();
            performanceMonitor->incrementHeartbeat(1);

            report.stopItTimer();
            
            report.addIterationData(type, performanceMonitor->averageThroughput(), performanceMonitor->getHeartbeat(), schedulerMonitor->getNumberOfThread());
        }
    }
    
    report.stopTimer();

    report.startDeallocTimer();
    
    deletePipeline(pipeline);
    
    deleteOpenCLBuffers(pipeline, deviceGPU);
    
    bool performanceMonitorDeregistrated = performanceMonitor->deregisterToOrchestrator();
    if (performanceMonitorDeregistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorDeregistrated = schedulerMonitor->deregisterToOrchestrator();
    if (schedulerMonitorDeregistrated) {
        delete schedulerMonitor;
    }
    
    report.stopDeallocTimer();
    
    report.reportOnDisk();

    
    return 0;
    
}
