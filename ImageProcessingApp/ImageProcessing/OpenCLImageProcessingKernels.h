//
//  ImageProcessingImplementation.h
//  ImageProcessingApp1
//
//  Created by Raegal on 25/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#ifndef __OpenCLImageProcessing__
#define __OpenCLImageProcessing__

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <vector>
#include <map>
#include <string>

#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include "CL/cl.h"
#endif


#include "OpenCLDevice.h"

#define MAX_VECTORSIZE 16
#define RGB2GREY_VECTORSIZE 4
#define MOTION_DETECTION_VECTORSIZE 16

//round up globalSize to the nearest multiple of baseSize
#define ROUNDUP(baseSize,globalSize)   ((globalSize%baseSize==0) ? (globalSize) : (globalSize + baseSize - globalSize%baseSize))

#define MAX_SOURCE_SIZE (0x100000)

#define NVIDIA_GPU 0
#define ARM_GPU 1

#ifndef TARGET_PLATFORM
#define TARGET_PLATFORM NVIDIA_GPU
#endif

void print_app();

/************* OpenCL ****************************/

cl_int twistFactor(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int rows, int cols, int depth, float factor);

cl_int gaussian_filter(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int rows, int cols, int depth, float sigma);

cl_int rgb2grey(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int imageRows, int imageCols);

cl_int motion_detection(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, cl_mem &bgBuffer, int imageRows, int imageCols);

cl_int edge_detection(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int imageRows, int imageCols);

cl_int erosion_filter(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int imageRows, int imageCols);

#endif
