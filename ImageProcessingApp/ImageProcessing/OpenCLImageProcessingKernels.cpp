//
//  ImageProcessingImplementation.cpp
//  ImageProcessingApp1
//
//  Created by Raegal on 25/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//


#include <stdlib.h>
#include <iostream>

#include "OpenCLImageProcessingKernels.h"
#include "ImageProcessingUtils.hpp"

void print_app(){
    std::cout << std::endl;
    std::cout << "******************************************************************************" << std::endl;
    std::cout << "APPLICATION: image_twisting -> image_twisting-1 -> gaussian_filter -> rgb2grey" << std::endl;
    std::cout << "******************************************************************************" << std::endl;
}



/************* OpenCL ****************************/

cl_int twistFactor(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int rows, int cols, int depth, float factor){
    
    cl_int status;
    
    status = clSetKernelArg(openCLDevice->kernels["image_twisting"], 0, sizeof(cl_mem), &inBuffer);
    status = clSetKernelArg(openCLDevice->kernels["image_twisting"], 1, sizeof(cl_mem), &outBuffer);
    status = clSetKernelArg(openCLDevice->kernels["image_twisting"], 2, sizeof(int), &depth);
    status = clSetKernelArg(openCLDevice->kernels["image_twisting"], 3, sizeof(float), &factor);
    
    size_t globalThreads[] = {(size_t)cols,(size_t)rows};
    //size_t localThreads[] = {res2,res1};
    
    //enqueue a kernel run call.
    cl_event ndrEvt;
    status = clEnqueueNDRangeKernel(openCLDevice->command_queue, openCLDevice->kernels["image_twisting"], 2, NULL, globalThreads, NULL /*localThreads*/, 0, NULL, &ndrEvt);
    
    status = clFinish(openCLDevice->command_queue); //clFinish
    
    return status;
    
}

cl_int gaussian_filter(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int rows, int cols, int depth, float sigma){
    cl_int status = CL_SUCCESS;
    
    //generate the filter mask
    int maskSize = (int)ceil(3.0f*sigma);
    float * mask = new float[(maskSize * 2 + 1)*(maskSize * 2 + 1)];
    float    sum = 0.0f;
    for (int a = -maskSize; a < maskSize + 1; a++) {
        for (int b = -maskSize; b < maskSize + 1; b++) {
            float temp = exp(-((float)(a*a + b*b) / (2 * sigma*sigma)));
            sum += temp;
            mask[a + maskSize + (b + maskSize)*(maskSize * 2 + 1)] = temp;
        }
    }
    // Normalize the mask
    for (int i = 0; i < (maskSize * 2 + 1)*(maskSize * 2 + 1); i++)
        mask[i] = mask[i] / sum;
    
    //instantiate a memory object for the mask
    cl_mem maskbuffer;
    maskbuffer = clCreateBuffer(openCLDevice->context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR /*CL_MEM_READ_WRITE*/, (maskSize*2+1)*(maskSize*2+1)*sizeof(float), mask /*0*/, &status);
    
    //set input parameters
    status = clSetKernelArg(openCLDevice->kernels["gaussian_filter"], 0, sizeof(cl_mem), &inBuffer);
    status = clSetKernelArg(openCLDevice->kernels["gaussian_filter"], 1, sizeof(cl_mem), &maskbuffer);
    status = clSetKernelArg(openCLDevice->kernels["gaussian_filter"], 2, sizeof(int), &maskSize);
    status = clSetKernelArg(openCLDevice->kernels["gaussian_filter"], 3, sizeof(cl_mem), &outBuffer);
    status = clSetKernelArg(openCLDevice->kernels["gaussian_filter"], 4, sizeof(int), &depth);
    
    size_t globalThreads[] = {(size_t)cols,(size_t)rows};
    //size_t localThreads[] = {res2,res1};
    
    //enqueue a kernel run call.
    cl_event ndrEvt;
    status = clEnqueueNDRangeKernel(openCLDevice->command_queue, openCLDevice->kernels["gaussian_filter"], 2, NULL, globalThreads, NULL /*localThreads*/, 0, NULL, &ndrEvt);
    
    status = clFlush(openCLDevice->command_queue); //clFinish
    //clWaitForEvents(1 , &ndrEvt);
    
    clReleaseMemObject(maskbuffer);
    delete [] mask;
    
    return status;
}

cl_int rgb2grey(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int imageRows, int imageCols)
{
    cl_int status = CL_SUCCESS;
    
    //set input parameters
    status = clSetKernelArg(openCLDevice->kernels["rgb2grey"], 0, sizeof(cl_mem), &inBuffer);
    
    //set outBuffer imager
    status = clSetKernelArg(openCLDevice->kernels["rgb2grey"], 1, sizeof(cl_mem), &outBuffer);
    
#if TARGET_PLATFORM == ARM_GPU
    size_t globalThreads[] = {(size_t)  (ROUNDUP(RGB2GREY_VECTORSIZE,imageCols * imageRows) / RGB2GREY_VECTORSIZE)};
#else
    size_t globalThreads[] = {(size_t)imageCols * imageRows};
#endif
    //size_t localThreads[] = {res2};
    
    //enqueue a kernel run call.
    cl_event ndrEvt;
    status = clEnqueueNDRangeKernel(openCLDevice->command_queue, openCLDevice->kernels["rgb2grey"], 1, NULL, globalThreads, NULL /*localThreads*/, 0, NULL, &ndrEvt);
    
    status = clFlush(openCLDevice->command_queue); //clFinish
    //clWaitForEvents(1 , &ndrEvt);
    
    return status;
}



/*
 * Runs motion_detection kernel
 */
cl_int motion_detection(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, cl_mem &bgBuffer, int imageRows, int imageCols){
    cl_int status = CL_SUCCESS;
    
    //set input parameters
    status = clSetKernelArg(openCLDevice->kernels["motion_detection"], 0, sizeof(cl_mem), &inBuffer);
    
    status = clSetKernelArg(openCLDevice->kernels["motion_detection"], 1, sizeof(cl_mem), &bgBuffer);
    
    //set outBuffer imager
    status = clSetKernelArg(openCLDevice->kernels["motion_detection"], 2, sizeof(cl_mem), &outBuffer);

#if TARGET_PLATFORM == ARM_GPU
    size_t globalThreads[] = {(size_t)  (ROUNDUP(MOTION_DETECTION_VECTORSIZE,imageCols * imageRows) / MOTION_DETECTION_VECTORSIZE)};
#else
    size_t globalThreads[] = {(size_t) imageCols * imageRows};
#endif
    //size_t localThreads[] = {res2};
    
    //enqueue a kernel run call.
    cl_event ndrEvt;
    status = clEnqueueNDRangeKernel(openCLDevice->command_queue, openCLDevice->kernels["motion_detection"], 1, NULL, globalThreads, NULL /*localThreads*/, 0, NULL, &ndrEvt);
    
    status = clFlush(openCLDevice->command_queue); //clFinish
    //clWaitForEvents(1 , &ndrEvt);
    
    return status;
}

cl_int image_downsampling(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer,int imageRows, int imageCols, int imageDepth, float factor){
    //TODO this kernel has a bug: it does not work with a factor >= 3. There should be a rounding error...
    
    cl_int status = CL_SUCCESS;
    int original_ImageCols = imageCols;
    int original_ImageRows = imageRows;
    
    imageCols = imageCols/factor;
    imageRows = imageRows/factor;
    
    //set input parameters
    status = clSetKernelArg(openCLDevice->kernels["image_downsampling"], 0, sizeof(cl_mem), &inBuffer);
    
    //set outBuffer imager
    status = clSetKernelArg(openCLDevice->kernels["image_downsampling"], 1, sizeof(cl_mem), &outBuffer);
    
    //other parameters
    status = clSetKernelArg(openCLDevice->kernels["image_downsampling"], 2, sizeof(float), &factor);
    
    status = clSetKernelArg(openCLDevice->kernels["image_downsampling"], 3, sizeof(int), &original_ImageCols);
    
    status = clSetKernelArg(openCLDevice->kernels["image_downsampling"], 4, sizeof(int), &original_ImageRows);
    status = clSetKernelArg(openCLDevice->kernels["image_downsampling"], 5, sizeof(int), &imageDepth);
    
    size_t globalThreads[] = {(size_t)imageCols,(size_t)imageRows};
    //size_t localThreads[] = {res2,res1};
    
    //enqueue a kernel run call.
    cl_event ndrEvt;
    status = clEnqueueNDRangeKernel(openCLDevice->command_queue, openCLDevice->kernels["image_downsampling"], 2, NULL, globalThreads, NULL /*localThreads*/, 0, NULL, &ndrEvt);
    
    status = clFlush(openCLDevice->command_queue); //clFinish
    //clWaitForEvents(1 , &ndrEvt);
    
    return status;
}

/*
 * Runs edge detection kernel
 */
cl_int edge_detection(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int imageRows, int imageCols){
    cl_int status = CL_SUCCESS;

    //set input parameters
    status = clSetKernelArg(openCLDevice->kernels["edge_detection"], 0, sizeof(cl_mem), &inBuffer);
    
    //set outBuffer imager
    status = clSetKernelArg(openCLDevice->kernels["edge_detection"], 1, sizeof(cl_mem), &outBuffer);
    
    size_t globalThreads[] = {(size_t)imageCols,(size_t)imageRows};
    //size_t localThreads[] = {res2,res1};
    
    //enqueue a kernel run call.
    cl_event ndrEvt;
    status = clEnqueueNDRangeKernel(openCLDevice->command_queue, openCLDevice->kernels["edge_detection"], 2, NULL, globalThreads, NULL /*localThreads*/, 0, NULL, &ndrEvt);
    
    status = clFlush(openCLDevice->command_queue); //clFinish
    //clWaitForEvents(1 , &ndrEvt);
    
    return status;
}

/*
 * Runs erosion filter kernel
 */
cl_int erosion_filter(OpenCLDevice::OpenCLDevicePtr openCLDevice, cl_mem &inBuffer, cl_mem &outBuffer, int imageRows, int imageCols){
    cl_int status = CL_SUCCESS;
    
    //set input parameters
    status = clSetKernelArg(openCLDevice->kernels["erosion_filter"], 0, sizeof(cl_mem), &inBuffer);
    //set outBuffer imager
    status = clSetKernelArg(openCLDevice->kernels["erosion_filter"], 1, sizeof(cl_mem), &outBuffer);
    
    size_t globalThreads[] = {(size_t)imageCols,(size_t)imageRows};
    //size_t localThreads[] = {res2,res1};
    
    //enqueue a kernel run call.
    cl_event ndrEvt;
    status = clEnqueueNDRangeKernel(openCLDevice->command_queue, openCLDevice->kernels["erosion_filter"], 2, NULL, globalThreads, NULL /*localThreads*/, 0, NULL, &ndrEvt);
    
    status = clFlush(openCLDevice->command_queue); //clFinish
    //clWaitForEvents(1 , &ndrEvt);
    
    return status;
}


/************* End OpenCL ************************/