
//#include <omp.h>

#ifndef __OpenMPImageProcessing__
#define __OpenMPImageProcessing__


#include "ImageProcessingUtils.hpp"


//image twisting macros
#define PI 3.1415265359
#define DRAD (180.0f/PI)

//motion detection macros
#define MD_THRESHOLD 10
#define EROSION_MAP_SIZE 7 /*must be an odd number*/
#define FG 0
#define BG 255
#define ER_THRESHOLD 0.7


/*
 * Performs the edge detection according to the Sobel algorithm
 */
void sobel_edge_detection(sImage* greyImage, sImage* edgeImage, unsigned int nThread);

/*
 * Performs the image downsampling by using the bilinear interpolation
 */
void image_downsample(sImage* image, sImage* outputImage, double factor, unsigned int nThread);

/*
 * Performs an image twisting
 */ 
void image_twisting(sImage* image, sImage* outputImage, float factor, unsigned int nThread);
/*
 * Performs the motion detection
 */
void motion_detection(sImage* originalImage, sImage* backgroundImage, sImage* outputImage, unsigned int nThread);
/*
 * Performs the gaussian filter
 */
void gaussian_filter(sImage* originalImage, sImage* finalImage, float sigma, unsigned int nThread);
/*
 * Performs the erosion filter
 */
void erosion_filter(sImage* originalImage, sImage* outputImage, unsigned int nThread);


#endif