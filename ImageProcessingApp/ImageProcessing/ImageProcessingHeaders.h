//
//  ImageProcessingHeaders.h
//  ImageProcessingApp1
//
//  Created by Raegal on 25/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#ifndef _ImageProcessingHeaders_h
#define _ImageProcessingHeaders_h


#include "ImageProcessingUtils.hpp"
#include "OpenCLImageProcessingKernels.h"
#include "OpenMPImageProcessingKernels.hpp"


#endif
