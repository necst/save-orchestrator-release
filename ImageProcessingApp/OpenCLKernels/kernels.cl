/*************************************/
/******* Edge Detection Filter *******/
/*************************************/


__kernel void edge_detection(__global uchar* inputImage, __global uchar* outputImage)
{
    uint x = get_global_id(0);
    uint y = get_global_id(1);
    
    uint width = get_global_size(0);
    uint height = get_global_size(1);
    
    int Gx = 0;
    int Gy = 0;
    
    int c = x + y * width;
    
    if(!(x==0||x==width-1) && !(y==0||y==height-1))
    {
        int i00 = convert_int(inputImage[c - 1 - width]);
        int i10 = convert_int(inputImage[c - width]);
        int i20 = convert_int(inputImage[c + 1 - width]);
        int i01 = convert_int(inputImage[c - 1]);
        int i11 = convert_int(inputImage[c]);
        int i21 = convert_int(inputImage[c + 1]);
        int i02 = convert_int(inputImage[c - 1 + width]);
        int i12 = convert_int(inputImage[c + width]);
        int i22 = convert_int(inputImage[c + 1 + width]);
        
        Gx =   i00 + 2 * i10 + i20 - i02 - 2 * i12 - i22;
        Gy =   i00 - i20 + 2 * i01 - 2 * i21 + i02 - i22;
        
        float SUM = sqrt(convert_float(Gx*Gx+Gy*Gy));
        
        outputImage[c] = convert_uchar(SUM);
    } else{
        outputImage[c] = convert_uchar(0);
    }
}



/******************************/
/******* Erosion Filter *******/
/******************************/

__constant float tsh = 0.7;
__constant int erosionSize = 7; //matrix 7X7

__kernel void erosion_filter( __global uchar* inputImage,__global uchar* outputImage)
{
    int cols = get_global_id(0);
    int rows = get_global_id(1);
    int width = get_global_size(0);
    int height = get_global_size(1);
    int r1, c1;
    
    int marker=0;
    int count=0;
    
    //for each pixel, we count how many pixels in the neighbor positions (we consider a 7x7 surrounding matrix)
    //belong to the foreground (we have to accurately manage pixels on the borders)
    for(r1=(rows-(erosionSize-1)/2>0)?(rows-(erosionSize-1)/2):0; r1<rows+(erosionSize-1)/2 && r1<height; r1++){
        for(c1=(cols-(erosionSize-1)/2>0)?cols-(erosionSize-1)/2:0; c1<cols+(erosionSize-1)/2 && c1<width; c1++){
            count++;
            marker+=(inputImage[r1*width+c1]==255)? 1:0;
        }
    }
    
    //if the percentage of foreground pixels is lower than a given threshold the pixel is set as background
    if(convert_float(marker)/count>tsh){
        outputImage[rows*width+cols]=convert_uchar(255);
    } else {
        outputImage[rows*width+cols]=convert_uchar(0);
    }
}

/******************************/
/****** Gaussian Filter *******/
/******************************/

__kernel void gaussian_filter(__global uchar* inputImage, __global float* mask, int mask_size, __global uchar* outputImage, int imageDepth)
{
    int c = get_global_id(0);
    int r = get_global_id(1);
    int width = get_global_size(0);
    int height = get_global_size(1);
    
    float sum;
    int color;
    int I, J;
    
    for(color=0; color<imageDepth; color++) {
        sum=0;
        /* image boundaries */
        if(c <= mask_size || c>=width-mask_size || r<=mask_size || r>=height-mask_size)
            sum = convert_float(inputImage[(c+r*width)*imageDepth + color]);
        else {/* Convolution starts here */
            for(I=-mask_size; I<=mask_size; I++)  {
                for(J=-mask_size; J<=mask_size; J++)  {
                    sum = sum + convert_float(inputImage[((c+I) + (r+J)*width)*imageDepth + color])
                    * mask[I+mask_size+(J+mask_size)*(mask_size*2+1)];
                }
            }
        }
        outputImage[(c+r*width)*imageDepth + color] = convert_uchar(sum);
    }
}

/*********************************/
/****** Image Downsampling *******/
/*********************************/

__kernel void image_downsampling(__global uchar* inputImage, __global uchar* outputImage, float factor, int original_width, int original_height, int image_depth)
{
    uint width = get_global_size(0);
    uint height = get_global_size(1);
    
    uint r, c; //current coordinates of the output image
    
    int olc, ohc, olr, ohr; //coordinates of the original image used for bilinear interpolation
    int index; //linearized index of the point
    uchar q11, q12, q21, q22;
    float accurate_c, accurate_r; //the exact scaled point
    int k;
    
    c = get_global_id(0);
    r = get_global_id(1);
    
    accurate_c = (float)c*factor;
    olc=accurate_c;
    ohc=olc+1;
    if(!(ohc<original_width))
        ohc=olc;
    
    accurate_r = (float)r*factor;
    olr=accurate_r;
    ohr=olr+1;
    if(!(ohr<original_height))
        ohr=olr;
    
    index = (c + r*width)*image_depth; //image_depth bytes per pixel
    for(k=0; k<image_depth; k++){
        q11=inputImage[(olc + olr*original_width)*image_depth+k];
        q12=inputImage[(olc + ohr*original_width)*image_depth+k];
        q21=inputImage[(ohc + olr*original_width)*image_depth+k];
        q22=inputImage[(ohc + ohr*original_width)*image_depth+k];
        outputImage[index+k] = convert_uchar( (q11*(ohc-accurate_c)*(ohr-accurate_r) +
                                               q21*(accurate_c-olc)*(ohr-accurate_r) +
                                               q12*(ohc-accurate_c)*(accurate_r-olr) +
                                               q22*(accurate_c-olc)*(accurate_r-olr)));
    }
}


/*****************************/
/****** Image Twisting *******/
/*****************************/

constant float PI=3.1415265359;
__kernel void image_twisting(__global uchar* inputImage, __global uchar* twistedImage, int imageDepth, float factor)
{
    uint width = get_global_size(0);
    uint height = get_global_size(1);
    float PI = 3.1415265359, DRAD = 180.0f/PI, DISTORTION_GAIN = 1000.0*factor; //a reasonable factor is 0.5
    
    int r, c; //current coordinates
    int cr, cc; //coordinates of the center of the image
    float x, y, radius, theta; //cartesian coordinates and polar ones of the twisted point (x=column number, y=row number)
    int index; //linearized index of the point
    
    int lx, ly, hx, hy; //4 coordinates for bilinear interpolation
    unsigned char q11, q12, q21, q22;
    int k;
    
    cr=height/2;
    cc=width/2;
    
    c = get_global_id(0);
    r = get_global_id(1);
    
    radius = sqrt((float)((c - cc) * (c - cc) + (r - cr) * (r - cr)));
    theta = radius/DRAD/height*DISTORTION_GAIN;
    
    x = cos(theta) * (c - cc) + sin(theta) * (r - cr) + cc;
    y = -sin(theta) * (c - cc) + cos(theta) * (r - cr) + cr;
    
    index= (c + r*width)*imageDepth; //imageDepth bytes per pixel
    
    if(x>=0 && y>=0 && y<height-1 && x<width-1) {  //bilinear interpolation
        lx=x;
        hx=x+1;
        ly=y;
        hy=y+1;
        
        for(k=0; k<imageDepth; k++){
            q11=inputImage[(lx + ly*width)*imageDepth+k];
            q12=inputImage[(lx + hy*width)*imageDepth+k];
            q21=inputImage[(hx + ly*width)*imageDepth+k];
            q22=inputImage[(hx + hy*width)*imageDepth+k];
            twistedImage[index+k]= (unsigned char) q11*(hx-x)*(hy-y) +
            q21*(x-lx)*(hy-y) +
            q12*(hx-x)*(y-ly) +
            q22*(x-lx)*(y-ly);
        }
    } else{
        for(k=0; k<imageDepth; k++){
            twistedImage[index+k] = 0;
        }
    }
}

/**************************/
/****** RGB To Grey *******/
/**************************/

#define NVIDIA_GPU 0
#define ARM_GPU 1

__kernel void rgb2grey(__global uchar* inputImage, __global uchar* outputImage)
{
#if TARGET_PLATFORM==ARM_GPU
    //the commented code uses ushort16 datatype; it requires less memory but sometimes it is not precise (overflow errors). the below code uses uint16
    /*    int i = get_global_id(0) * 4; //We process 4 pixels per time
     uchar16 pixels = vload16(0,inputImage + 3*i);
     ushort16 spixels = convert_ushort16(pixels);
     ushort4 res = ((ushort)153 * spixels.s258b)/(ushort)512 + ((ushort)300 * spixels.s147a)/(ushort)512 + ((ushort)58 * spixels.s0369)/(ushort)512;
     uchar4 resc = convert_uchar4(res);
     vstore4(resc, 0, outputImage + i);
     */
    int i = get_global_id(0) * 4; //We process 4 pixels per time
    uchar16 pixels = vload16(0,inputImage + 3*i);
    uint16 spixels = convert_uint16(pixels);
    uint4 res = ((uint)153 * spixels.s258b)/(uint)512 + ((uint)300 * spixels.s147a)/(uint)512 + ((uint)58 * spixels.s0369)/(uint)512;
    uchar4 resc = convert_uchar4(res);
    vstore4(resc, 0, outputImage + i);
    
    
#else
    int i = get_global_id(0);
    
    //the commented code avoid using floats but obviously presents rounding errors. On NVIDIA GPU the use of float variables should not be a problem
    /*    ushort currentElRed = convert_ushort(inputImage[3*i+2]);
     ushort currentElGreen = convert_ushort(inputImage[3*i+1]);
     ushort currentElBlue = convert_ushort(inputImage[3*i]);
     outputImage[i] = convert_uchar(((ushort)(306)*currentElRed + (ushort)(601)*currentElGreen + (ushort)(117)*currentElBlue)>>10);
     */
    uchar currentElRed = inputImage[3*i+2];
    uchar currentElGreen = inputImage[3*i+1];
    uchar currentElBlu = inputImage[3*i];
    
    float x1 = convert_float(currentElRed);
    float y1 = convert_float(currentElGreen);
    float z1 = convert_float(currentElBlu);
    
    outputImage[i] = convert_uchar(0.299*x1 + 0.587*y1 + 0.114*z1);
#endif
}



/**************************************/
/****** Motion Detection Kernel *******/
/**************************************/

#if TARGET_PLATFORM==ARM_GPU
__constant uchar16 threshold = (uchar16)10;
#else
__constant uchar threshold = (uchar)10;
#endif

__kernel void motion_detection(__global uchar* motionImage, __global uchar* bgImage, __global uchar* outputImage)
{
#if TARGET_PLATFORM==ARM_GPU
    int i = get_global_id(0) * 16; //We process 16 pixels per time
    uchar16 bg = vload16(0,bgImage + i);
    uchar16 mo = vload16(0,motionImage + i);
    uchar16 a = (bg > mo) ? (bg - mo) : (mo - bg);
    uchar16 res = (a <(uchar16) threshold) ? (uchar16)255 : (uchar16)0;
    vstore16(res, 0, outputImage + i);
#else
    int i = get_global_id(0);
    uchar bg = bgImage[i];
    uchar mo = motionImage[i];
    outputImage[i] = (abs(motionImage[i]-bgImage[i])>threshold) ? (uchar)0 : (uchar)255;
#endif
}














