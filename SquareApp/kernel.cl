

kernel void square(global int* input, global int* output) {
    
    //int2 globalId = (int2)(get_global_id(0), get_global_id(1));
    //int2 localId = (int2)(get_local_id(0), get_local_id(1));
    //int2 groupId = (int2)(get_group_id (0), get_group_id (1));
    //int2 globalSize = (int2)(get_global_size(0), get_global_size(1));
    //int2 locallSize = (int2)(get_local_size(0), get_local_size(1));
    //int2 numberOfGrp = (int2)(get_num_groups (0), get_num_groups (1));
    
    //printf("globalId: %d localId: %d groupId: %d globalSize: %d locallSize: %d numberOfGrp: %d \n",globalId,localId,groupId,globalSize,locallSize,numberOfGrp);
    
    //output[id] = input[id]*input[id];
    
    int index = get_global_id(0);
    
    output[index] = input[index]*input[index];
}