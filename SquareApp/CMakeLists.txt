project(SquareApp)
cmake_minimum_required(VERSION 2.8)

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pthread -fopenmp")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -pthread -fopenmp")
else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

if (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")	
	FIND_LIBRARY(OPENCL_LIBRARIES OpenCL DOC "OpenCL lib for OSX")
	FIND_PATH(OPENCL_INCLUDE_DIRS OpenCL/cl.h DOC "Include for OpenCL on OSX")
	FIND_PATH(_OPENCL_CPP_INCLUDE_DIRS OpenCL/cl.hpp DOC "Include for OpenCL CPP bindings on OSX")
elseif (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    	FIND_LIBRARY(OPENCL_LIBRARIES OpenCL DOC "OpenCL lib for Linux")
	FIND_PATH(OPENCL_INCLUDE_DIRS CL/cl.h DOC "Include for OpenCL on Linux")
	FIND_PATH(_OPENCL_CPP_INCLUDE_DIRS CL/cl.hpp DOC "Include for OpenCL CPP bindings on Linux")
endif()

include_directories(${OPENCL_INCLUDE_DIRS})

include_directories(../Orchestrator/CSocket)
include_directories(../Orchestrator/SHMObject)
include_directories(../Orchestrator/Manager)
include_directories(../Orchestrator/Monitor)
include_directories(../Orchestrator/OrchestratorClass)
include_directories(../Orchestrator/JSONCPP)
include_directories(../Orchestrator/BinarySemaphore)
include_directories(../Orchestrator/OpenCLDevice)


add_library(CSocket ../Orchestrator/CSocket/CSocket.cpp)
add_library(SHMObject ../Orchestrator/SHMObject/SHMObject.cpp)
add_library(Manager ../Orchestrator/Manager/Manager.cpp)
add_library(BinarySemaphore ../Orchestrator/BinarySemaphore/BinarySemaphore.cpp) 
add_library(Monitor ../Orchestrator/Monitor/Monitor.cpp ../Orchestrator/Monitor/AppPerformanceMonitor.cpp ../Orchestrator/Monitor/AppScheduleMonitor.cpp ../Orchestrator/Monitor/ReportMonitor.cpp)
add_library(OpenCLDevice ../Orchestrator/OpenCLDevice/OpenCLDevice.cpp)


configure_file(kernel.cl kernel.cl COPYONLY)


aux_source_directory(. SRC_LIST)
add_executable(${PROJECT_NAME} ${SRC_LIST})

target_link_libraries( ${PROJECT_NAME} ${OPENCL_LIBRARIES} Monitor CSocket SHMObject BinarySemaphore Manager OpenCLDevice)

