//
//  main.cpp
//  SampleApp
//
//  Created by Raegal on 16/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>

#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include <omp.h>
#include "CL/cl.h"
#endif

#include <sys/time.h>

#include "OpenCLDevice.h"

#include "AppPerformanceMonitor.h"
#include "ReportMonitor.h"

//#include "kernel.cl.h"


#define NUM_VALUES 1024*1024*100
//define NUM_VALUES 10*10

#define MAX_SOURCE_SIZE (0x100000)


/***************** Options ***********************/

typedef struct globalArgs_t{
    unsigned int numberOfIterations;
    unsigned int numberOfData;
    double minTrhoughput;
    double maxTrhoughput;
    std::string reportFileName;
} GlobalArgs;

static const char *optString = "n:d:l:u:r:";

static struct option longOpts[] = {
    { "numberOfIterations", required_argument, NULL, 'n' },
    { "numberOfData", required_argument, NULL, 'd' },
    { "minTrhoughput", required_argument, NULL, 'l' },
    { "maxTrhoughput", required_argument, NULL, 'u' },
    { "reportFileName", required_argument, NULL, 'r' },

};


GlobalArgs globalArgs;


void parseCommmand(int argc, char** argv){
    int longIndex;
    int opt;
    
    while((opt = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1 ) {
        switch( opt ) {
                
            case 'n':
                globalArgs.numberOfIterations = std::atoi(optarg);
                break;
                
            case 'd':
                globalArgs.numberOfData = std::atoi(optarg);
                break;
                
            case 'l':
                globalArgs.minTrhoughput = std::atof(optarg);
                break;
                
            case 'u':
                globalArgs.maxTrhoughput = std::atof(optarg);
                break;
                
            case 'r':
                globalArgs.reportFileName = optarg;
                break;
                
            default:
                break;
        }
    }
    
    
}

/***************** End Options ********************/



/************* OpenCL ****************************/

void initOpenCLKernel(OpenCLDevice::OpenCLDevicePtr device) {
    device->addKernel("square");
}

/************* End OpenCL ************************/


/************* CPU Kernel ******************+*****/
void squareCPU(int * input, int * output, unsigned int nThread){
    #pragma omp parallel for num_threads(nThread)
    for (int i = 0; i < globalArgs.numberOfData; ++i) {
        output[i] = input[i]*input[i];
    }
}

/************* End CPU Kernel ********************/


/************* GPU OpenCL Kernel *****************/
void squareOpenCLGPU(int * input, int * output, OpenCLDevice::OpenCLDevicePtr openCLInfo) {
    
    cl_int ret;
    /* Create Buffer Object */
    
    cl_mem mem_in = clCreateBuffer(openCLInfo->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR ,sizeof(cl_int) * globalArgs.numberOfData, input, &ret);
    cl_mem mem_out = clCreateBuffer(openCLInfo->context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR ,sizeof(cl_int) * globalArgs.numberOfData, output, &ret);
    
    ret = clFinish(openCLInfo->command_queue);
    
    ret = clSetKernelArg(openCLInfo->kernels["square"], 0, sizeof(cl_mem), &mem_in);
    ret = clSetKernelArg(openCLInfo->kernels["square"], 1, sizeof(cl_mem), &mem_out);
    
    /*Execute Kernel*/
    size_t global_work_size[3] = {globalArgs.numberOfData,1,1}; //set number of item per dimension
    ret = clEnqueueNDRangeKernel(openCLInfo->command_queue, openCLInfo->kernels["square"], 1, NULL, global_work_size, NULL, 0, NULL, NULL);
    
    ret = clFlush(openCLInfo->command_queue);
    ret = clFinish(openCLInfo->command_queue);
    
    /* Copy output data from the output memory buffer */
    ret = clEnqueueReadBuffer(openCLInfo->command_queue, mem_out, CL_TRUE, 0, globalArgs.numberOfData*sizeof(cl_int), output, 0, NULL, NULL);
    
    /* Destroy Buffer Object */
    ret = clReleaseMemObject(mem_in);
    ret = clReleaseMemObject(mem_out);
    
}

/************* End GPU OpenCL Kernel *************/


int main(int argc, char** argv){
    
    
    parseCommmand(argc, argv);
    
    ReportMonitor report(globalArgs.reportFileName);
    
    OpenCLDevice::OpenCLDeviceMap openCLDeviceList = OpenCLDevice::openCLDeviceList("./kernel.cl");
    OpenCLDevice::OpenCLDevicePtr device = openCLDeviceList[GPUScheduleType*2][0];
    
    report.startAllocTimer();
    
    int i;
    
    int * input = (int*)malloc(sizeof(cl_int) * globalArgs.numberOfData); //aligned
    int * output = (int*)malloc(sizeof(cl_int) * globalArgs.numberOfData); //aligned
    int * outputGPU = (int*)malloc(sizeof(cl_int) * globalArgs.numberOfData); //aligned
    
    
    //**** GPU
    initOpenCLKernel(device);
    //********
    
    
    /*Create Fake data*/
    for (int i = 0; i < globalArgs.numberOfData; ++i) {
        input[i] = i;
    }
    
    /******************/
/*
    unsigned long t0CPU, t1CPU, tCPU;
    unsigned long t0GPU, t1GPU, tGPU = 0;
    t0CPU = getTime();
    squareCPU(input,output);
    t1CPU = getTime();
    tCPU = t1CPU - t0CPU;
    
    std::cout << "CPU time: " << tCPU << std::endl;
    
    
    clFlush(openCLInfo.command_queue);
    
    t0GPU = getTime();
    squareOpenCLGPU(input,outputGPU);
    t1GPU = getTime();
    tGPU = t1GPU - t0GPU;
    
    std::cout << "GPU time: " << tGPU << std::endl;
 
    for (int i = 0; i < globalArgs.numberOfData; ++i) {
        if (output[i] != outputGPU[i]) {
            std::cout << "Error on index: " << i << " CPU: " << output[i] << " GPU " << outputGPU[i] << std::endl;
        }
    }
    */
    AppPerformanceMonitor * performanceMonitor = new AppPerformanceMonitor("test");
    AppScheduleMonitor * schedulerMonitor = new AppScheduleMonitor("test");
    

    
    /*CPU Implementation*/
    ImplementationType cpuImpl = [&] () -> void {
        std::cout << "CPU" << std::endl;
        squareCPU(input,output, schedulerMonitor->getNumberOfThread());
    };
    
    /*GPU Implementation*/
    ImplementationType gpuImpl = [&] () -> void {
        std::cout << "GPU" << std::endl;
        squareOpenCLGPU(input,outputGPU,device);
    };
    
    
    bool performanceMonitorRegistrated = performanceMonitor->registerToOrchestrator();
    if (!performanceMonitorRegistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorRegistrated = schedulerMonitor->registerToOrchestrator();
    if (!schedulerMonitorRegistrated) {
        delete schedulerMonitor;
    }
    
    report.stopAllocTimer();
    
    report.startTimer();
    
    schedulerMonitor->registerImplementation(cpuImpl,CPUScheduleType);
    schedulerMonitor->registerImplementation(gpuImpl,GPUScheduleType);
    schedulerMonitor->setFastestImplementation(CPUScheduleType);
    schedulerMonitor->setSlowestImplementation(GPUScheduleType);
    
    
    
    ApplicationGoal applicationGoal;
    applicationGoal.trhoughput = 0.8;
    
    performanceMonitor->setApplicationGoal(applicationGoal);
    
    ApplicationConstraints applicationConstraint;
    applicationConstraint.trhoughputMin = globalArgs.minTrhoughput;
    applicationConstraint.trhoughputMax = globalArgs.maxTrhoughput;
    
    performanceMonitor->setApplicationGoal(applicationGoal);
    performanceMonitor->setApplicationConstraints(applicationConstraint);
    
    schedulerMonitor->setHasToBeProfiled(true);
    
    report.startTimer();
    
    for (i = 0; i<globalArgs.numberOfIterations; i++) {
        
        report.startItTimer();
        
        ChosenScheduleType type = schedulerMonitor->executeHeterogenousImplementations();
        performanceMonitor->incrementHeartbeat(1);
        
        report.stopItTimer();
        
        report.addIterationData(type, performanceMonitor->averageThroughput(), performanceMonitor->getHeartbeat(), schedulerMonitor->getNumberOfThread());
    }
    
    report.stopTimer();

    report.startDeallocTimer();

    bool performanceMonitorDeregistrated = performanceMonitor->deregisterToOrchestrator();
    if (performanceMonitorDeregistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorDeregistrated = schedulerMonitor->deregisterToOrchestrator();
    if (schedulerMonitorDeregistrated) {
        delete schedulerMonitor;
    }
 
    report.reportOnDisk();
    
    free(input);
    free(output);
    free(outputGPU);
    
    report.stopDeallocTimer();
    
    return 0;
    
}
