//
//  main.cpp
//  SampleApp
//
//  Created by Raegal on 16/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>

#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include <omp.h>
#include "CL/cl.h"
#endif

#include <sys/time.h>

#include "OpenCLDevice.h"
#include "AppPerformanceMonitor.h"
#include "ReportMonitor.h"

//#include "kernel.cl.h"



#define NUM_OF_COLUMNS          50
#define NUM_OF_ROWS             10000 //22000
#define NUM_OF_CHUNKS_PER_DIM   5 //22000


#define MAX_SOURCE_SIZE (0x100000)

/***************** Options ***********************/

typedef struct globalArgs_t{
    unsigned int numberOfIterations;
    unsigned int numberOfPixels;
    unsigned int numberOfSuccFrame;
    unsigned int numberOfHorizontalChunk;
    unsigned int numberOfVerticalChunk;
    double minTrhoughput;
    double maxTrhoughput;
    std::string reportFileName;
    bool sync;
} GlobalArgs;

static const char *optString = "n:p:k:h:v:l:u:r:s:";

static struct option longOpts[] = {
    { "numberOfIterations", required_argument, NULL, 'n' },
    { "numberOfPixels", required_argument, NULL, 'p' },
    { "numberOfSuccFrame", required_argument, NULL, 'k' },
    { "numberOfHorizontalCut", required_argument, NULL, 'h' },
    { "numberOfVerticalCut", required_argument, NULL, 'v' },
    { "minTrhoughput", required_argument, NULL, 'l' },
    { "maxTrhoughput", required_argument, NULL, 'u' },
    { "reportFileName", required_argument, NULL, 'r' },
    { "sync", required_argument, NULL, 's' },
};


GlobalArgs globalArgs;


void parseCommmand(int argc, char** argv){
    int longIndex;
    int opt;
    
    while((opt = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1 ) {
        switch( opt ) {
                
            case 'n':
                globalArgs.numberOfIterations = std::atoi(optarg);
                break;
                
            case 'p':
                globalArgs.numberOfPixels = std::atoi(optarg);
                break;
                
            case 'k':
                globalArgs.numberOfSuccFrame = std::atoi(optarg);
                break;
                
            case 'h':
                globalArgs.numberOfHorizontalChunk = std::atoi(optarg);
                break;
                
            case 'v':
                globalArgs.numberOfVerticalChunk = std::atoi(optarg);
                break;
                
            case 'l':
                globalArgs.minTrhoughput = std::atof(optarg);
                break;
                
            case 'u':
                globalArgs.maxTrhoughput = std::atof(optarg);
                break;
                
            case 'r':
                globalArgs.reportFileName = optarg;
                break;
                
            case 's':
                globalArgs.sync = std::atoi(optarg);
                break;
                
            default:
                break;
        }
    }
    
    
}

/***************** End Options ********************/


/************* OpenCL ****************************/

void initOpenCLKernel(OpenCLDevice::OpenCLDevicePtr device) {
    device->addKernel("Correlation_2D");
}

/************* End OpenCL ************************/


/************* CPU Kernel ******************+*****/

void Correlation_CPU_2D(float * inputMatrix, float * inputStdDev, float * outputMatrix, unsigned int nThread){
    
    #pragma omp parallel for num_threads(nThread)

    for (int row = 0; row < globalArgs.numberOfPixels; ++row) {
        for (int col = 0; col < globalArgs.numberOfPixels; ++col) {
            
            if (col > row) {
                outputMatrix[row*globalArgs.numberOfPixels + col] = 0.0;
                break;
            } else {
                
                float temp = 0;
                
                float tot_dev = inputStdDev[row] * inputStdDev[col] * globalArgs.numberOfSuccFrame;
                
                for (int k = 0; k < globalArgs.numberOfSuccFrame; ++k) {
                    temp = inputMatrix[row*globalArgs.numberOfSuccFrame + k] * inputMatrix[col*globalArgs.numberOfSuccFrame + k];
                }
                
                temp /= tot_dev;
                
                if (row == col)
                    temp = 1.0;
                
                outputMatrix[row*globalArgs.numberOfPixels + col] = temp;
            }
        }
    }
}

/************* End CPU Kernel ********************/


/************* GPU OpenCL Kernel *****************/

void Correlation_OpenCL_GPU_2D(float * inputMatrix, float * inputStdDev, float * outputMatrix, OpenCLDevice::OpenCLDevicePtr openCLInfo, ReportMonitor &report){
    
    cl_int ret;
    
    report.startSubItTimer("clCreateBuffer");
    
    cl_mem mem_inputMatrix = clCreateBuffer(openCLInfo->context, CL_MEM_READ_ONLY /*| CL_MEM_USE_HOST_PTR*/ ,sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfSuccFrame, NULL, &ret);
    cl_mem mem_inputStdDev = clCreateBuffer(openCLInfo->context, CL_MEM_READ_ONLY /*| CL_MEM_USE_HOST_PTR*/ ,sizeof(cl_float) * globalArgs.numberOfPixels, NULL, &ret);
    cl_mem mem_outputMatrix = clCreateBuffer(openCLInfo->context, CL_MEM_WRITE_ONLY /*| CL_MEM_COPY_HOST_PTR*/ ,sizeof(cl_float) * globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk * globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk, NULL, &ret);
    
    size_t global_work_size[3] = {globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk, globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk, 0}; //set number of item per dimension
    size_t region[3] = {globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk*sizeof(float), globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk, 1};
    size_t buffer_origin[3] = {0,0,0};
    
    int * offset = (int*)malloc(2*sizeof(int));
    size_t host_origin[3] = {0,0,0};
    
    cl_mem mem_offset = clCreateBuffer(openCLInfo->context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(int)*2, offset, &ret);
    
    report.stopSubItTimer();

    report.startSubItTimer("clEnqueueWriteBuffer");
    
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_inputMatrix, CL_FALSE, 0, sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfSuccFrame, inputMatrix, 0, NULL, NULL);
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_inputStdDev, CL_FALSE, 0, sizeof(cl_float) * globalArgs.numberOfPixels, inputStdDev, 0, NULL, NULL);
    ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_outputMatrix, CL_FALSE, 0, sizeof(cl_float) * globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk * globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk, outputMatrix, 0, NULL, NULL);

    if (globalArgs.sync) {
        ret = clFinish(openCLInfo->command_queue);
    }
    
    report.stopSubItTimer();

    
    report.startSubItTimer("clSetKernelArg");
    
    ret = clSetKernelArg(openCLInfo->kernels["Correlation_2D"], 0, sizeof(cl_mem), &mem_inputMatrix);
    ret = clSetKernelArg(openCLInfo->kernels["Correlation_2D"], 1, sizeof(cl_mem), &mem_inputStdDev);
    ret = clSetKernelArg(openCLInfo->kernels["Correlation_2D"], 2, sizeof(cl_mem), &mem_outputMatrix);
    
    if (globalArgs.sync) {
        ret = clFinish(openCLInfo->command_queue);
    }
    
    report.stopSubItTimer();
    
    for (int i = 0; i < globalArgs.numberOfVerticalChunk; ++i) {
        for (int j = 0; j < globalArgs.numberOfHorizontalChunk; ++j) {
            
            report.startSubItTimer("clEnqueueNDRangeKernel");

            offset[0] = i*globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk; // in kernel the [0] is used for col
            offset[1] = j*globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk; // in kernel in [1] is used for row
            
            std::cout << "offset[0] " << offset[0] << " offset[1] " << offset[1] << std::endl;
            
            ret = clEnqueueWriteBuffer(openCLInfo->command_queue, mem_offset, CL_TRUE, 0, 2*sizeof(cl_int), offset, 0, NULL, NULL);
            
            ret = clSetKernelArg(openCLInfo->kernels["Correlation_2D"], 3, sizeof(cl_mem), &mem_offset);
            
            ret = clEnqueueNDRangeKernel(openCLInfo->command_queue, openCLInfo->kernels["Correlation_2D"], 2, NULL, global_work_size, NULL, 0, NULL, NULL);
            
            if (globalArgs.sync) {
                ret = clFinish(openCLInfo->command_queue);
            }
            
            report.stopSubItTimer();

            host_origin[0] = (size_t)offset[0]*sizeof(float);
            host_origin[1] = (size_t)offset[1];
            
            //std::cout << "offset: " << host_origin[0]+host_origin[1]*globalArgs.numberOfPixels*sizeof(float) << std::endl;
            
            report.startSubItTimer("clEnqueueReadBufferRect");

            ret = clEnqueueReadBufferRect(openCLInfo->command_queue, mem_outputMatrix, CL_FALSE, buffer_origin, host_origin, region, 0, 0, globalArgs.numberOfPixels*sizeof(float) , 0, outputMatrix, 0, NULL, NULL);
            
            if (globalArgs.sync) {
                ret = clFinish(openCLInfo->command_queue);
            }
            
            report.stopSubItTimer();
        }
    }
    
    /* Destroy Buffer Object */
    
    report.startSubItTimer("clReleaseMemObject");

    delete offset;
    
    ret = clReleaseMemObject(mem_inputMatrix);
    ret = clReleaseMemObject(mem_inputStdDev);
    ret = clReleaseMemObject(mem_outputMatrix);
    ret = clReleaseMemObject(mem_offset);
    
    ret = clFinish(openCLInfo->command_queue);

    report.stopSubItTimer();
}

/************* End GPU OpenCL Kernel *************/


int main(int argc, char** argv)
{    
    parseCommmand(argc, argv);
    
    
    //OpenCLDevice::OpenCLDeviceMap openCLDeviceMap = OpenCLDevice::openCLDeviceList("./kernel.cl");
    
    
    ReportMonitor report(globalArgs.reportFileName);

    //OpenCLDevice device = OpenCLDevice(CL_DEVICE_TYPE_GPU,"./kernel.cl");
    OpenCLDevice::OpenCLDeviceMap openCLDeviceList = OpenCLDevice::openCLDeviceList("./kernel.cl");
    OpenCLDevice::OpenCLDevicePtr device = openCLDeviceList[GPUScheduleType*2][0];
    
    report.startAllocTimer();

    
    int i;
    
    float * corrInput = (float*)malloc(sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfSuccFrame); //aligned
    float * corrOutput = (float*)malloc(sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfPixels); //aligned
    float * corrOutputGPU = (float*)malloc(sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfPixels); //aligned
    
    float * corrInputStdDev = (float*)malloc(sizeof(cl_float) * globalArgs.numberOfPixels); //aligned
    
    //**** GPU
    initOpenCLKernel(device);
    //********
    
    
    /*Create Fake data*/
    srand48(time(NULL));
    
    for (int i = 0; i < globalArgs.numberOfPixels * globalArgs.numberOfSuccFrame; ++i) {
        corrInput[i] = drand48();
    }
    
    for (int i = 0; i < globalArgs.numberOfPixels; ++i) {
        corrInputStdDev[i] = 1.0;
    }
    
    /******************/

/*
    Correlation_CPU_2D(corrInput, corrInputStdDev, corrOutput, 4);
    Correlation_OpenCL_GPU_2D(corrInput, corrInputStdDev, corrOutputGPU, device, report);
    
    for (int t0 = 0; t0 < globalArgs.numberOfPixels; ++t0) {
        for (int t1 = 0; t1 < globalArgs.numberOfPixels; ++t1) {
            
            if ((corrOutput[t1*globalArgs.numberOfPixels+t0] - corrOutputGPU[t1*globalArgs.numberOfPixels+t0]) > 0.00001) {
                std::cout << "CPU[" << t1 << "][" << t0 << "] = " << corrOutput[t1*globalArgs.numberOfPixels+t0] << " [" << t1*globalArgs.numberOfPixels+t0 << "]" << std::endl;
                std::cout << "GPU[" << t1 << "][" << t0 << "] = " << corrOutputGPU[t1*globalArgs.numberOfPixels+t0] << " [" << t1*globalArgs.numberOfPixels+t0 << "]" << std::endl;
            }
        }
    }
    */
    AppPerformanceMonitor * performanceMonitor = new AppPerformanceMonitor("test");
    AppScheduleMonitor * schedulerMonitor = new AppScheduleMonitor("test");
    
    /*CPU Implementation*/
    ImplementationType cpuImpl = [&] () -> void {
        std::cout << "CPU" << std::endl;
        unsigned int nThread = schedulerMonitor->getNumberOfThread();
        Correlation_CPU_2D(corrInput, corrInputStdDev, corrOutput, nThread);
    };
    
    /*GPU Implementation*/
    ImplementationType gpuImpl = [&] () -> void {
        std::cout << "GPU" << std::endl;
        Correlation_OpenCL_GPU_2D(corrInput, corrInputStdDev, corrOutputGPU, device, report);
    };
    
    schedulerMonitor->registerImplementation(cpuImpl,CPUScheduleType);
    schedulerMonitor->registerImplementation(gpuImpl,GPUScheduleType);
    schedulerMonitor->setFastestImplementation(CPUScheduleType);
    schedulerMonitor->setSlowestImplementation(GPUScheduleType);
    
    
    ApplicationGoal applicationGoal;
    applicationGoal.trhoughput = 0.1;
    
    ApplicationConstraints applicationConstraint;
    applicationConstraint.trhoughputMin = globalArgs.minTrhoughput;
    applicationConstraint.trhoughputMax = globalArgs.maxTrhoughput;
    
    performanceMonitor->setApplicationGoal(applicationGoal);
    performanceMonitor->setApplicationConstraints(applicationConstraint);
    
    schedulerMonitor->setHasToBeProfiled(true);
    
    bool performanceMonitorRegistrated = performanceMonitor->registerToOrchestrator();
    if (!performanceMonitorRegistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorRegistrated = schedulerMonitor->registerToOrchestrator();
    if (!schedulerMonitorRegistrated) {
        delete schedulerMonitor;
    }
    
    report.stopAllocTimer();
    
    report.startTimer();

    
    for (i = 0; i<globalArgs.numberOfIterations; i++) {
        
        report.startItTimer();
        
        ChosenScheduleType type = schedulerMonitor->executeHeterogenousImplementations();
        performanceMonitor->incrementHeartbeat(1);
        
        report.stopItTimer();
        
        report.addIterationData(type, performanceMonitor->averageThroughput(), performanceMonitor->getHeartbeat(), schedulerMonitor->getNumberOfThread());
    }
    
    report.stopTimer();

    report.startDeallocTimer();

    bool performanceMonitorDeregistrated = performanceMonitor->deregisterToOrchestrator();
    if (performanceMonitorDeregistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorDeregistrated = schedulerMonitor->deregisterToOrchestrator();
    if (schedulerMonitorDeregistrated) {
        delete schedulerMonitor;
    }
    
    free(corrInput);
    free(corrInputStdDev);
    free(corrOutput);
    free(corrOutputGPU);
    
    report.stopDeallocTimer();
    
    report.reportOnDisk();

    
    return 0;
    
}
