
kernel void Correlation_2D(global float * inputMatrix, global float * inputStdDev, global float * outputMatrix, global int * offset) {
    
    int col = get_global_id(1);
    int row = get_global_id(0);
    
    int outindex = row * get_global_size(1) + col;
    
    int inputRow = row + offset[1];
    int inputCol = col + offset[0];
    
    int linearIndex = inputRow * 2 + inputCol;
    
    if (inputCol > inputRow) {
        float temp = 0;
        outputMatrix[outindex] = 0.0;
        //printf("corr[%d][%d] = %f [%d] [index: %d]\n", inputRow, inputCol, temp, linearIndex, outindex);
        return;
    }
    
    if (inputCol == inputRow) {
        float temp = 1.0;
        outputMatrix[outindex] = 1.0;
        //printf("corr[%d][%d] = %f [%d] [index: %d]\n", inputRow, inputCol, temp, linearIndex, outindex);
        return;
    }
    float temp = 0;
    float tot_dev = inputStdDev[inputRow] * inputStdDev[inputCol] * 50;
    for (int k = 0; k < 50; ++k) {
        temp = inputMatrix[inputRow * 50 + k] * inputMatrix[inputCol * 50 + k];
    }
    temp /= tot_dev;
    //printf("corr[%d][%d] = %f [%d] [index: %d]\n", inputRow, inputCol, temp, linearIndex, outindex);
    outputMatrix[outindex] = temp;
    
}
