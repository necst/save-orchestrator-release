//
//  UDPMonitor.h
//  SampleMonitor
//
//  Created by Raegal on 15/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __SHMAppMonitorObject__
#define __SHMAppMonitorObject__



#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>

#include <string>


template <class T> class SHMObject {
    
    
private:

public:
    
    int shmId;

    T* shmPtr;
    
    SHMObject();
    SHMObject(int shmIdToAttach);
    ~SHMObject();
    
    unsigned int numberOfProcessAttachedToSHMObject(int shmIdToAttach);
    
};


template <class T> SHMObject<T>::SHMObject() {
    
    //struct shmid_ds shmbuffer;
    //unsigned long segment_size;
    
    shmId = shmget (IPC_PRIVATE, sizeof(T), IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
    
    if (shmId == -1) {
        std::cout << "Error opening shared memory with error:" << errno << std::endl;
        exit(-1);
    }
    
    shmPtr = (T*) shmat (shmId, 0, 0);
    
    if (shmPtr == (void *) -1) {
        std::cout << "Error attaching to shared memory with error:" << errno << std::endl;
        exit(-1);
    }
    
    std::cout << "shared memory attached at address " << shmPtr << std::endl;
    
    
    
    /* Determine the segment’s size. */
    //    shmctl (shmId, IPC_STAT, &shmbuffer);
    //    segment_size = shmbuffer.shm_segsz;
    //
    //    printf ("segment size: %lu\n", segment_size);
    //    /* Write a string to the shared memory segment. */
    //    sprintf (shmPtr, "Hello, world.");
    
}

template <class T> SHMObject<T>::SHMObject(int shmIdToAttach)
{
    shmId = shmIdToAttach;
    shmPtr = (T*) shmat (shmId, 0, 0);
    
    if (shmPtr == (void*) -1) {
        std::cout << "Error attaching to shared memory with error:" << errno << std::endl;
        exit(-1);
    }
}

template <class T> SHMObject<T>::~SHMObject()
{
    /* Detach the shared memory segment. */
    int ret = shmdt(shmPtr);
    
    if (ret) {
        std::cout << "Error detaching to shared memory with error:" << errno << std::endl;
        exit(-1);
    }
    
    /* Deallocate the shared memory segment. */
    ret = shmctl(shmId, IPC_RMID, 0);
    
    if (ret) {
        std::cout << "Error deallocating shared memory with error:" << errno << std::endl;
        exit(-1);
    }
    
    std::cout << "Detached and deallocated memory correctly" << std::endl;
}


template <class T> unsigned int SHMObject<T>::numberOfProcessAttachedToSHMObject(int shmIdToAttach) {
    struct shmid_ds shm_segment;

    int ret = shmctl(shmId, IPC_STAT, &shm_segment);

    return shm_segment.shm_nattch;
    
}



#endif /* defined(__SampleMonitor__SHMAppMonitorObject__) */
