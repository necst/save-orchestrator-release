//
//  UDPAppScheduleMonitor.cpp
//  UDPOrchestrator
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include "Defines.h"
#include "AppScheduleMonitor.h"

AppScheduleMonitor::AppScheduleMonitor():Monitor<AppScheduleMonitorParameters>() {
}

AppScheduleMonitor::AppScheduleMonitor(std::string name):Monitor<AppScheduleMonitorParameters>(name) {
    
    sharedMemory->shmPtr->pid = getpid();
    semaphore.open(std::to_string(getpid()));
}

AppScheduleMonitor::AppScheduleMonitor(std::string name, ChosenScheduleType type):AppScheduleMonitor(name) {
    sharedMemory->shmPtr->chosenScheduleType = type;
    
    if (type == CPUScheduleType)
        sharedMemory->shmPtr->numberOfThreadsCurrentlyUsed = 4;
    else
        sharedMemory->shmPtr->numberOfThreadsCurrentlyUsed = 1;
}


AppScheduleMonitor::~AppScheduleMonitor(){

}

bool AppScheduleMonitor::registerToOrchestrator(){
    return Monitor<AppScheduleMonitorParameters>::actionToOrchestrator(SMRegistration);
}


bool AppScheduleMonitor::deregisterToOrchestrator(){
    return Monitor<AppScheduleMonitorParameters>::actionToOrchestrator(SMDeregistration);
}


void AppScheduleMonitor::setSlowestImplementation(ChosenScheduleType slowestScheduleImplementation) {
    sharedMemory->shmPtr->slowestImplementation = slowestScheduleImplementation;
}

void AppScheduleMonitor::setFastestImplementation(ChosenScheduleType fastestScheduleImplementation) {
    sharedMemory->shmPtr->fastestImplementation = fastestScheduleImplementation;
}

void AppScheduleMonitor::setHasToBeProfiled(bool profile){
    sharedMemory->shmPtr->hasToBeProfiled = profile;
}

unsigned int AppScheduleMonitor::getNumberOfThread() {
    return sharedMemory->shmPtr->numberOfThreadsCurrentlyUsed;
}

ChosenScheduleType AppScheduleMonitor::getChosenScheduleType() {
    return sharedMemory->shmPtr->chosenScheduleType;
}


void AppScheduleMonitor::registerImplementation(ImplementationType f, ChosenScheduleType forSchedule) {
    
    switch (forSchedule) {
            
        case NOScheduleType:
            break;
            
        case CPUScheduleType:
            CPUImplementation = f;
            sharedMemory->shmPtr->CPUImplementationAvailable = true;
            sharedMemory->shmPtr->availableImplementationMask |= CPUScheduleType;
            break;
        case GPUScheduleType:
            GPUImplementation = f;
            sharedMemory->shmPtr->GPUImplementationAvailable = true;
            sharedMemory->shmPtr->availableImplementationMask |= GPUScheduleType;

            break;
        case DFEScheduleType:
            DFEImplementation = f;
            sharedMemory->shmPtr->DFEImplementationAvailable = true;
            sharedMemory->shmPtr->availableImplementationMask |= DFEScheduleType;

            break;
            
        default:
            break;
    }
}

void AppScheduleMonitor::deregisterImplementation(ChosenScheduleType forSchedule) {
    
    switch (forSchedule) {
        case NOScheduleType:
            break;
            
        case CPUScheduleType:
            CPUImplementation = nullptr;
            break;
        case GPUScheduleType:
            GPUImplementation = nullptr;
            break;
        case DFEScheduleType:
            DFEImplementation = nullptr;
            break;
            
        default:
            break;
    }
}

ChosenScheduleType AppScheduleMonitor::executeHeterogenousImplementations() {

    ChosenScheduleType type = sharedMemory->shmPtr->chosenScheduleType;

    while (type == NOScheduleType) {
        std::cout << "NOScheduleType, I'm going to sleep..." << std::endl;
        semaphore.wait();
        type = sharedMemory->shmPtr->chosenScheduleType;
        
        if (sharedMemory->shmPtr->noAvailableResources) {
            std::cout << "Orchestrator shut me down... Harakiri." << std::endl;
            exit(0);
        }
        
    }

    switch (type) {
        case CPUScheduleType:
            if (CPUImplementation)
                CPUImplementation();
            
            std::cout << "CPU" << std::endl;
            break;
        case GPUScheduleType:
            if (GPUImplementation)
                GPUImplementation();
            
            std::cout << "GPU" << std::endl;
            break;
        case DFEScheduleType:
            if (DFEImplementation)
                DFEImplementation();
            
            std::cout << "DFE" << std::endl;
            break;
        default:
            break;
    }
    return type;
}

