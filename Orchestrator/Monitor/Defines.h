//
//  Defines.h
//  UDPOrchestrator
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef _Defines_h
#define _Defines_h

#include <vector>
#include <string>
#include <memory>
#include <map>

#include "BinarySemaphore.h"
#include "AppPerformanceMonitor.h"
#include "AppScheduleMonitor.h"

#define PMRegistration          "PMRegistration"
#define PMDeregistration        "PMDeregistration"
#define SMRegistration          "SMRegistration"
#define SMDeregistration        "SMDeregistration"


typedef enum {
    ActionTypePerformanceMonitorRegistration,
    ActionTypePerformanceMonitorDeregistration,
    ActionTypeScheduleMonitorRegistration,
    ActionTypeScheduleMonitorDeregistration
} RegistrationActionType;


std::string registrationActionTypeToString(RegistrationActionType action);
RegistrationActionType registrationActionTypeFromString(std::string action);
std::vector<std::string> stringsSeparatedByToken(std::string stringToBeSeparated, std::string token);


typedef enum {
    AppStateNoState,
    AppStateRegistering,
    AppStateRegistered,
    AppStateDispatched,
    AppStateDeregistering
}AppState;

typedef enum {
    PolicyTypeNoPolicy,
    PolicyTypeSimplePolicy,
    PolicyTypePerformance,
    PolicyTypePower,
    PolicyTypeOnlyCPU,
    PolicyTypeOnlyGPU
}PolicyType;

PolicyType policyTypeFromString(std::string policy);

typedef SHMObject<AppPerformanceMonitorParameters> PerformanceMonitorSHM;
typedef SHMObject<AppScheduleMonitorParameters> ScheduleMonitorSHM;
typedef std::shared_ptr<PerformanceMonitorSHM> PerformanceMonitorSHMPtr;
typedef std::shared_ptr<ScheduleMonitorSHM> ScheduleMonitorSHMPtr;
typedef std::shared_ptr<BinarySemaphore> BinarySemaphorePtr;

#define ALPHA   0.7

typedef std::map<ChosenScheduleType, std::map<long, std::vector<double>>> ClustersInformation;

typedef struct {
    ClustersInformation clustersInformation;
    ChosenScheduleType profilingState = NOScheduleType;
    int profilingIndex = 0;
    
}ProfilingInformation;

//TODO:
typedef struct {
    AppState appState = AppStateNoState;
    PerformanceMonitorSHMPtr performanceMonitor;
    ScheduleMonitorSHMPtr heterogeneusScheduleMonitor;
    
    BinarySemaphorePtr semaphorePtr;
    
    ProfilingInformation profilingInformation;
    
    int appGlobalCycleCount;
    
    bool notManageableByCurrentManager;
        
    bool registrationCompleted() {
        if(performanceMonitor && heterogeneusScheduleMonitor)
            return true;
        
        return false;
    };
    
    int getAvailableImplementationMask() {
        return this->heterogeneusScheduleMonitor->shmPtr->availableImplementationMask;
    }
    
    int getGlobalCycleCount() {
        return this->performanceMonitor->shmPtr->globalCycleCount;
    }
    
    unsigned int getNumberOfThreadUsed() {
        return this->heterogeneusScheduleMonitor->shmPtr->numberOfThreadsCurrentlyUsed;
    }
    
    unsigned int getNumberOfCoresUsed() {
        return this->heterogeneusScheduleMonitor->shmPtr->numberOfCoresCurrentlyUsed;
    }
    
    void setNumberOfThreadUsed(unsigned int newNumberOfThread) {
        this->heterogeneusScheduleMonitor->shmPtr->numberOfThreadsCurrentlyUsed = newNumberOfThread;
    }
    
    void setNumberOfCoresUsed(unsigned int newNumberOfCores) {
        this->heterogeneusScheduleMonitor->shmPtr->numberOfCoresCurrentlyUsed = newNumberOfCores;
    }
    
    void setChosenScheduleType(ChosenScheduleType newScheduleType) {
        this->heterogeneusScheduleMonitor->shmPtr->chosenScheduleType = newScheduleType;
    }
    
    ChosenScheduleType getScheduledType() {
        return this->heterogeneusScheduleMonitor->shmPtr->chosenScheduleType;
    }
    
    double getAverageThroughput() {
        return this->performanceMonitor->shmPtr->averageThroughput;
    }
    
    double getInstantThroughput() {
        return this->performanceMonitor->shmPtr->instantThroughput;
    }
    
    double getMinThroughputConstraint() {
        return this->performanceMonitor->shmPtr->applicationConstraints.trhoughputMin;
    }
    
    double getMaxThroughputConstraint() {
        return this->performanceMonitor->shmPtr->applicationConstraints.trhoughputMax;
    }
    
    bool hasCPUImplementation() {
        ChosenScheduleTypeMask mask = this->heterogeneusScheduleMonitor->shmPtr->availableImplementationMask;
        return ((mask | CPUScheduleType) == CPUScheduleType);
    }
    
    bool hasGPUImplementation() {
        ChosenScheduleTypeMask mask = this->heterogeneusScheduleMonitor->shmPtr->availableImplementationMask;
        return ((mask | GPUScheduleType) == GPUScheduleType);
    }
    
    bool hasDFEImplementation() {
        ChosenScheduleTypeMask mask = this->heterogeneusScheduleMonitor->shmPtr->availableImplementationMask;
        return ((mask | DFEScheduleType) == DFEScheduleType);
    }
    
    bool hasOnlyOneClustersType() {
        ChosenScheduleTypeMask mask = this->heterogeneusScheduleMonitor->shmPtr->availableImplementationMask;
        
        return (mask == CPUScheduleType) || (mask == GPUScheduleType) || (mask == DFEScheduleType);
    }
    
    bool hasOpenMPThreadCustomizationType() {
        ChosenThreadCustomizationType mask = this->heterogeneusScheduleMonitor->shmPtr->availableThreadCustomizationMask;
        return (mask == OpenMPThreadCustomizationType);
    }
    
    bool hasNormalThreadCustomizationType() {
        ChosenThreadCustomizationType mask = this->heterogeneusScheduleMonitor->shmPtr->availableThreadCustomizationMask;
        return (mask == NormalThreadCustomizationType);
    }
    
    bool hasNoThreadCustomizationType() {
        ChosenThreadCustomizationType mask = this->heterogeneusScheduleMonitor->shmPtr->availableThreadCustomizationMask;
        return (mask == NOThreadCustomizationType);
    }
    
    void setHasNoResourcesFlag(bool hasNoResources) {
        this->heterogeneusScheduleMonitor->shmPtr->noAvailableResources = hasNoResources;
    }
    
    bool getHasNoResourcesFlag() {
        return this->heterogeneusScheduleMonitor->shmPtr->noAvailableResources;
    }
    
    
}MonitorInformation;


/*

*/

#endif
