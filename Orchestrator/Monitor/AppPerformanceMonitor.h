//
//  UDPAppPerformanceMonitor.h
//  SampleMonitor
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __AppPerformanceMonitor__
#define __AppPerformanceMonitor__

#include <chrono>
#include <mutex>
#include <vector>
#include "Monitor.h"
#include "AppScheduleMonitor.h"

#define DEFAULT_THROUGHPUT_VALUE    (-1)
#define TIME_WINDOW                 17



typedef struct {
    double trhoughput = 0.0;
}ApplicationGoal;

typedef struct {
    double trhoughputMin = 0.0;
    double trhoughputMax = 0.0;
}ApplicationConstraints;


typedef struct {
    double trhoughputCPU = DEFAULT_THROUGHPUT_VALUE;
    double trhoughputGPU = DEFAULT_THROUGHPUT_VALUE;
    double trhoughputDFE = DEFAULT_THROUGHPUT_VALUE;
}ApplicationManifest;



typedef struct {
    pid_t pid;
    unsigned short priority;
    unsigned int trip_count = 0;

    double instantThroughput = 0;
    double averageThroughput = 0;

    long totalHeartBeat = 0;
    long lastHeartBeat = 0;
    
    unsigned int cycleSinceLastManagerChange = 0;
    unsigned int globalCycleCount = 0;
    
    std::chrono::steady_clock::time_point lastTime;

    ApplicationGoal applicationGoal;
    ApplicationConstraints applicationConstraints;
    ApplicationManifest applicationManifest;
    
}AppPerformanceMonitorParameters;


typedef struct {
    long heartBeat;
    std::chrono::steady_clock::time_point heartBeatTime;
}AppThroughputHistory;


class AppPerformanceMonitor : Monitor<AppPerformanceMonitorParameters> {
    
    int timeWindow = TIME_WINDOW;
    std::vector<AppThroughputHistory> throughputHistory;

public:
    
    AppPerformanceMonitor();
    AppPerformanceMonitor(std::string name);

    bool registerToOrchestrator();
    bool deregisterToOrchestrator();
    
    void incrementHeartbeat(long rate);
    void setApplicationGoal(ApplicationGoal applicationGoal);
    void setApplicationConstraints(ApplicationConstraints applicationConstraints);
    void setApplicationManifest(ApplicationManifest applicationManifest);

    double instantThroughput();
    double averageThroughput();
    double getHeartbeat();
};


#endif /* defined(__UDPAppPerformanceMonitor__) */
