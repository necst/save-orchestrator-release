//
//  UDPAppPerformanceMonitor.cpp
//  SampleMonitor
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//
#include <functional>
#include <numeric>
#include <iostream>
#include <fstream>

#include "ReportMonitor.h"

#include <sys/time.h>
uint64_t getTime(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000 * 1000 + tv.tv_usec;
}


ReportMonitor::ReportMonitor():ReportMonitor("Report.txt") {
}

ReportMonitor::ReportMonitor(std::string reportFileName) {
    this->reportFileName = reportFileName;
}

ReportMonitor::~ReportMonitor() {
}


void ReportMonitor::startTimer() {
    startTime = std::chrono::steady_clock::now();
    startGian = getTime();
}


void ReportMonitor::stopTimer() {
    stopTime = std::chrono::steady_clock::now();
    stopGian = getTime();
}

void ReportMonitor::startAllocTimer() {
    startAllocTime = std::chrono::steady_clock::now();
}

void ReportMonitor::stopAllocTimer() {
    stopAllocTime = std::chrono::steady_clock::now();
}

void ReportMonitor::startDeallocTimer() {
    startDeallocTime = std::chrono::steady_clock::now();
}


void ReportMonitor::stopDeallocTimer() {
    stopDeallocTime = std::chrono::steady_clock::now();
}

void ReportMonitor::startItTimer() {
    startItTime = std::chrono::steady_clock::now();
}


void ReportMonitor::stopItTimer() {
    stopItTime = std::chrono::steady_clock::now();
}

void ReportMonitor::startSubItTimer(std::string msg) {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    tempSubData.startSubTimeSinceStart = (std::chrono::duration_cast<std::chrono::microseconds>(now - startTime).count());
    tempSubData.subMsg = msg;
}


void ReportMonitor::stopSubItTimer() {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    tempSubData.stopSubTimeSinceStart = (std::chrono::duration_cast<std::chrono::microseconds>(now - startTime).count());

    tempSubIterationDataVector.push_back(tempSubData);
}


void ReportMonitor::addIterationData(ChosenScheduleType withType, double averageThoughput, int numberOfHeartbeat, unsigned int numberOfThread){

    IterationData data;
    data.numberOfThread = numberOfThread;
    data.averageThroughput = averageThoughput;
    data.type = withType;
    
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    data.deltaTimeSinceStart = (std::chrono::duration_cast<std::chrono::microseconds>(now - startTime).count());
    data.startItTimeSinceStart = (std::chrono::duration_cast<std::chrono::microseconds>(startItTime - startTime).count());
    data.stopItTimeSinceStart = (std::chrono::duration_cast<std::chrono::microseconds>(stopItTime - startTime).count());
    
    data.throughputSinceStart = (numberOfHeartbeat/(data.deltaTimeSinceStart*1.0))*1000000;
    
    data.subIterationDataVector = std::vector<SubIterationData>(tempSubIterationDataVector);
    
    iterationDataVector.push_back(data);
    
    tempSubIterationDataVector.clear();
}

std::string ReportMonitor::convertChoosenScheduleTypeToString(ChosenScheduleType type) {
    switch (type) {
        case CPUScheduleType:
            return "CPU";
        case GPUScheduleType:
            return "GPU";
        case DFEScheduleType:
            return "DFE";
        default:
            break;
    }
    return "NO HW";
}

void ReportMonitor::reportOnDisk()
{
    if (iterationDataVector.size() > 0)
    {
        std::ofstream report(reportFileName);
        
        std::time_t sATime = std::chrono::duration_cast<std::chrono::microseconds>(startAllocTime.time_since_epoch()).count();
        report << "StartAllocTime:\t" << sATime << std::endl;

        std::time_t eATime = std::chrono::duration_cast<std::chrono::microseconds>(stopAllocTime.time_since_epoch()).count();
        report << "EndAllocTime:\t" << eATime << std::endl;
        
        double totalATime = (eATime - sATime)/1000000.0;
        report << "TotalAllocTime:\t" << totalATime << std::endl;
        
        std::time_t sDTime = std::chrono::duration_cast<std::chrono::microseconds>(startDeallocTime.time_since_epoch()).count();
        report << "StartDeallocTime:\t" << sDTime << std::endl;
        
        std::time_t eDTime = std::chrono::duration_cast<std::chrono::microseconds>(stopDeallocTime.time_since_epoch()).count();
        report << "EndDeallocTime:\t" << eDTime << std::endl;
        
        double totalDTime = (eDTime - sDTime)/1000000.0;
        report << "TotalDeallocTime:\t" << totalDTime << std::endl;
        
        std::time_t sTime = std::chrono::duration_cast<std::chrono::microseconds>(startTime.time_since_epoch()).count();
        report << "StartExecTime:\t" << sTime << std::endl;
        
        std::time_t eTime = std::chrono::duration_cast<std::chrono::microseconds>(stopTime.time_since_epoch()).count();
        report << "EndExecTime:\t" << eTime << std::endl;
        
        double totalTime = (eTime - sTime)/1000000.0;
        report << "TotalExecTime:\t" << totalTime << std::endl;
        
        for (auto iter = iterationDataVector.begin(); iter != iterationDataVector.end(); ++iter)
        {
            report << "Iteration " << std::distance(iterationDataVector.begin(), iter) << std::endl;
            report << "HW:\t" << convertChoosenScheduleTypeToString(iter->type) << "\t";
            report << "TSS:\t" << iter->throughputSinceStart << "\t";
            report << "AT:\t" << iter->averageThroughput << "\t";
            report << "#T:\t" << iter->numberOfThread << "\t";
            report << "DSTSS:\t" << iter->startItTimeSinceStart << "\t";
            report << "DETSS:\t" << iter->stopItTimeSinceStart << "\t";
            report << "DTSS:\t" << iter->deltaTimeSinceStart;
            report << std::endl;
            
            for (auto subIter = iter->subIterationDataVector.begin(); subIter != iter->subIterationDataVector.end(); ++subIter) {
                
                double totalSubTime = (subIter->stopSubTimeSinceStart - subIter->startSubTimeSinceStart)/1000000.0;
                
                report << "SubIteration " << std::distance(iter->subIterationDataVector.begin(), subIter) << std::endl;
                report << "SubStart " << subIter->startSubTimeSinceStart << "\t";
                report << "SubStop: " << subIter->stopSubTimeSinceStart << "\t";
                report << "TotalSubTime: " << totalSubTime << "\t";
                report << "SubMessage: " << subIter->subMsg;
                report << std::endl;
            }
        }
    }
}
