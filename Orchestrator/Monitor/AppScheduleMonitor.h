//
//  UDPAppScheduleMonitor.h
//  UDPOrchestrator
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __AppScheduleMonitor__
#define __AppScheduleMonitor__


#ifdef __APPLE__
#include "BinarySemaphore.h"

#else
#include <BinarySemaphore.h>

#endif

#include "Monitor.h"


typedef enum {
    NOScheduleType = 0,
    CPUScheduleType = 1 << 0,
    GPUScheduleType = 1 << 1,
    DFEScheduleType = 1 << 2
} ChosenScheduleType;

typedef enum {
    NOThreadCustomizationType = 0,
    OpenMPThreadCustomizationType = 1 << 0,
    NormalThreadCustomizationType = 1 << 1,
} ChosenThreadCustomizationType;

typedef int ChosenScheduleTypeMask;


typedef struct {
    pid_t pid;
    ChosenScheduleType chosenScheduleType = NOScheduleType;
    
    ChosenScheduleType slowestImplementation = NOScheduleType;
    ChosenScheduleType fastestImplementation = NOScheduleType;
    
    ChosenScheduleTypeMask availableImplementationMask;
    
    ChosenThreadCustomizationType availableThreadCustomizationMask;
    
    unsigned int numberOfThreadsCurrentlyUsed = 0;
    unsigned int numberOfCoresCurrentlyUsed = 0;
    
    bool CPUImplementationAvailable;
    bool GPUImplementationAvailable;
    bool DFEImplementationAvailable;
    
    bool hasToBeProfiled = true;
    
    bool noAvailableResources = false;
    
}AppScheduleMonitorParameters;


//we will use this functions as wrapper to the application code
typedef std::function<void(void)> ImplementationType;

class AppScheduleMonitor : Monitor<AppScheduleMonitorParameters> {
        
private:
    ImplementationType CPUImplementation = nullptr;
    ImplementationType GPUImplementation = nullptr;
    ImplementationType DFEImplementation = nullptr;
    
    BinarySemaphore semaphore;

    
public:
    
    AppScheduleMonitor();
    AppScheduleMonitor(std::string name);
    AppScheduleMonitor(std::string name, ChosenScheduleType type);
    
    ~AppScheduleMonitor();
    
    
    bool registerToOrchestrator();
    bool deregisterToOrchestrator();
    
    ChosenScheduleType executeHeterogenousImplementations();
    
    void registerImplementation(ImplementationType f, ChosenScheduleType forSchedule);
    void deregisterImplementation(ChosenScheduleType forSchedule);
    
    void setSlowestImplementation(ChosenScheduleType slowestScheduleImplementation);
    void setFastestImplementation(ChosenScheduleType fastetScheduleImplementation);
    
    unsigned int getNumberOfThread();
    ChosenScheduleType getChosenScheduleType();

    
    void setHasToBeProfiled(bool profile);
    
};

#endif
