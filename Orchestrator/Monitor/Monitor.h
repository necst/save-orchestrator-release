//
//  UDPMonitor.h
//  SampleMonitor
//
//  Created by Raegal on 15/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __Monitor__
#define __Monitor__

#include <iostream>
#include <stdio.h>
#include <string>
#include <memory>
#include <sys/types.h>

#ifdef __APPLE__
#include "CSocket.h"
#include "SHMObject.h"
#else
#include <CSocket.h>
#include <SHMObject.h>
#endif




template <class T> class Monitor {
    
protected:
    std::unique_ptr<SHMObject<T>> sharedMemory = nullptr;
    
public:
    Monitor();
    Monitor(std::string name);
    
    ~Monitor();
    
	bool actionToOrchestrator(std::string action);
    bool sendSocketWithMessage(std::string message);
};



template <class T> Monitor<T>::Monitor():Monitor("NO_NAME"){
}

template <class T> Monitor<T>::Monitor(std::string name) {
    sharedMemory = std::unique_ptr<SHMObject<T>> (new SHMObject<T>());
}

template <class T> Monitor<T>::~Monitor()
{
    if (sharedMemory != NULL) {
        sharedMemory.release();
    }
}

template <class T> bool Monitor<T>::sendSocketWithMessage(std::string message)
{
    CSocket socket;
    
    std::string host_name="127.0.0.1";
    
    if (!socket.connect(host_name)) {
        socket.closeAll("Error connecting socket", 0);
        exit(3); // TODO
    }
    
    //Now lets do the client related stuff
    size_t buffer_len = message.length();
    char buffer[buffer_len];
    
    memset(buffer, 0, buffer_len);
    
    strcpy(buffer, message.c_str());
    
    size_t bytecount;
    if( (bytecount = send(socket.currSocket(), buffer, buffer_len,0))== -1){
        socket.closeAll("Error sending data", 0);
        return false;
    }
    
    std::cout << "Sent bytes " << bytecount << std::endl;
    
    if((bytecount = recv(socket.currSocket(), buffer, buffer_len, 0))== -1){
        socket.closeAll("Error receiving data", 0);
        return false;
    }
    
    std::cout << "Recieved bytes " << bytecount << std::endl;
    std::cout << "Received string " << buffer << std::endl;
    
    socket.closeAll("",0);
    
    return true;
}


template <class T> bool Monitor<T>::actionToOrchestrator(std::string action)
{
    std::string buff = action + "_" + std::to_string(getpid()) + "_" + std::to_string(sharedMemory->shmId);
    //printf("buff %ld\n", buff.size());
    return sendSocketWithMessage(buff);
}


#endif
