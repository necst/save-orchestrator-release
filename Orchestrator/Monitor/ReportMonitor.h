//
//  UDPAppPerformanceMonitor.h
//  SampleMonitor
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __ReportMonitor__
#define __ReportMonitor__

#include <chrono>
#include <mutex>
#include <vector>


#include "Defines.h"

typedef struct {
    long long startSubTimeSinceStart;
    long long stopSubTimeSinceStart;
    std::string subMsg;
    
} SubIterationData;

typedef struct {
    ChosenScheduleType type;
    double averageThroughput;
    double throughputSinceStart;
    long long deltaTimeSinceStart;
    long long startItTimeSinceStart;
    long long stopItTimeSinceStart;
    unsigned int numberOfThread;
    
    std::vector<SubIterationData> subIterationDataVector;
    
} IterationData;

class ReportMonitor {

    std::chrono::steady_clock::time_point startTime;
    std::chrono::steady_clock::time_point stopTime;
    
    std::chrono::steady_clock::time_point startAllocTime;
    std::chrono::steady_clock::time_point stopAllocTime;
    
    std::chrono::steady_clock::time_point startDeallocTime;
    std::chrono::steady_clock::time_point stopDeallocTime;
    
    std::chrono::steady_clock::time_point startItTime;
    std::chrono::steady_clock::time_point stopItTime;
    
    uint64_t startGian;
    uint64_t stopGian;
    
    std::vector<IterationData> iterationDataVector;
    
    SubIterationData tempSubData;
    std::vector<SubIterationData> tempSubIterationDataVector;
    
    std::string reportFileName;
    
public:
    
    ReportMonitor();
    ReportMonitor(std::string reportFileName);
    ~ReportMonitor();

    void startTimer();
    void stopTimer();
    
    void startAllocTimer();
    void stopAllocTimer();
    void startDeallocTimer();
    void stopDeallocTimer();
    void startItTimer();
    void stopItTimer();
    void startSubItTimer(std::string msg);
    void stopSubItTimer();
    
    void addIterationData(ChosenScheduleType withType, double averageThoughput, int numberOfHeartbeat, unsigned int numberOfThread);
    std::string convertChoosenScheduleTypeToString(ChosenScheduleType type);

    void reportOnDisk();
};


#endif /* defined(__UDPAppPerformanceMonitor__) */
