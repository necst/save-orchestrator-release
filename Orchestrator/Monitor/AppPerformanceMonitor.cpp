//
//  UDPAppPerformanceMonitor.cpp
//  SampleMonitor
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//
#include <functional>
#include <numeric>

#include "Defines.h"
#include "AppPerformanceMonitor.h"



AppPerformanceMonitor::AppPerformanceMonitor():Monitor<AppPerformanceMonitorParameters>() {
}

AppPerformanceMonitor::AppPerformanceMonitor(std::string name):Monitor<AppPerformanceMonitorParameters>(name) {    
    sharedMemory->shmPtr->pid = getpid();
    sharedMemory->shmPtr->lastTime = std::chrono::steady_clock::now();

}

bool AppPerformanceMonitor::registerToOrchestrator(){
    return Monitor<AppPerformanceMonitorParameters>::actionToOrchestrator(PMRegistration);
}

bool AppPerformanceMonitor::deregisterToOrchestrator(){
    return Monitor<AppPerformanceMonitorParameters>::actionToOrchestrator(PMDeregistration);
}

void AppPerformanceMonitor::incrementHeartbeat(long rate){
    sharedMemory->shmPtr->totalHeartBeat += rate;
    sharedMemory->shmPtr->trip_count++;
    sharedMemory->shmPtr->cycleSinceLastManagerChange++;
    sharedMemory->shmPtr->globalCycleCount++;
    //update trhoughput
    double it = instantThroughput();
    double at = averageThroughput();
    std::cout << "instantThroughput:" << it << std::endl;
    std::cout << "averageThroughput:" << at << std::endl;

}

double AppPerformanceMonitor::instantThroughput(){
    
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point lastTime = sharedMemory->shmPtr->lastTime;
    
    auto deltaTime = (std::chrono::duration_cast<std::chrono::microseconds>(now - lastTime).count());
    
    long total = sharedMemory->shmPtr->totalHeartBeat;
    
    long deltaHeartBeat = (total - sharedMemory->shmPtr->lastHeartBeat);
    
    double throughput = (deltaHeartBeat/(deltaTime*1.0))*1000000;
    
    sharedMemory->shmPtr->lastTime = now;
    sharedMemory->shmPtr->lastHeartBeat = total;
    sharedMemory->shmPtr->instantThroughput = throughput;
    
    AppThroughputHistory history;
    history.heartBeat = deltaHeartBeat;
    history.heartBeatTime = now;
    
    throughputHistory.push_back(history);
    
    if (throughputHistory.size() > timeWindow)
        throughputHistory.erase(throughputHistory.begin());
    
    return throughput;
}


double AppPerformanceMonitor::averageThroughput() {
    
    if (throughputHistory.size() < 2) {
        return 0.0;
    }
    
    double averageThroughput;
    double acc = std::accumulate(throughputHistory.begin()+1, throughputHistory.end(), 0.0, [](const double &value, AppThroughputHistory elem){
        return value + elem.heartBeat;
    });
    
    //std::cout << "acc:" << acc << std::endl;

    auto deltaTime = (std::chrono::duration_cast<std::chrono::microseconds>((throughputHistory.end()-1)->heartBeatTime - throughputHistory.begin()->heartBeatTime).count());
    //std::cout << "deltaTime:" << deltaTime << std::endl;
    
    averageThroughput = acc/(deltaTime*1.0)*1000000.0;

    sharedMemory->shmPtr->averageThroughput = averageThroughput;

    //std::cout << "averageThroughput:" << averageThroughput << std::endl;
    
    return averageThroughput;
}

double AppPerformanceMonitor::getHeartbeat() {
    return sharedMemory->shmPtr->lastHeartBeat;
}

void AppPerformanceMonitor::setApplicationGoal(ApplicationGoal applicationGoal){
    sharedMemory->shmPtr->applicationGoal = applicationGoal;
}
void AppPerformanceMonitor::setApplicationConstraints(ApplicationConstraints applicationConstraints){
    sharedMemory->shmPtr->applicationConstraints = applicationConstraints;
}

void AppPerformanceMonitor::setApplicationManifest(ApplicationManifest applicationManifest){
    sharedMemory->shmPtr->applicationManifest = applicationManifest;
}
