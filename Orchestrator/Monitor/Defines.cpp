//
//  Defines.cpp
//  SampleMonitor
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include "Defines.h"


std::string registrationActionTypeToString(RegistrationActionType action) {
    switch (action) {
        case ActionTypePerformanceMonitorRegistration:
            return PMRegistration;
        case ActionTypePerformanceMonitorDeregistration:
            return PMDeregistration;
        case ActionTypeScheduleMonitorRegistration:
            return SMRegistration;
        case ActionTypeScheduleMonitorDeregistration:
            return SMDeregistration;
        default:
            break;
    }
}


RegistrationActionType registrationActionTypeFromString(std::string action) {
    std::map<std::string, RegistrationActionType> actions = {
        {"PMRegistration" , ActionTypePerformanceMonitorRegistration},
        {"PMDeregistration" , ActionTypePerformanceMonitorDeregistration},
        {"SMRegistration" , ActionTypeScheduleMonitorRegistration},
        {"SMDeregistration" , ActionTypeScheduleMonitorDeregistration}
    };
    
    return actions.at(action);
}

PolicyType policyTypeFromString(std::string policy) {
    std::map<std::string, PolicyType> policyTypeMap = {
        {"performance" , PolicyTypePerformance},
        {"power" , PolicyTypePower},
        {"simple" , PolicyTypeSimplePolicy},
        {"onlyCPU" , PolicyTypeOnlyCPU},
        {"onlyGPU" , PolicyTypeOnlyGPU}
    };
    
    return policyTypeMap.at(policy);
}


std::vector<std::string> stringsSeparatedByToken(std::string stringToBeSeparated, std::string token) {
    char str[1024];
    std::vector<std::string> separatedStrings;
    
    strcpy(str, stringToBeSeparated.c_str());
    char * pch = strtok (str, token.c_str());;
    
    while (pch != NULL) {
        separatedStrings.push_back(pch);
        pch = strtok (NULL, token.c_str());
    }
    
    return separatedStrings;
}

