//
//  CPUManager.cpp
//  UDPOrchestrator
//
//  Created by Raegal on 24/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include "DFEManager.h"



DFEManager::DFEManager():Manager() {
    
}

DFEManager::DFEManager(long mId):Manager(mId) {

}

DFEManager::~DFEManager() {
    
}


bool DFEManager::registerApplication(pid_t pid, MonitorInformation monitorSharedPtr) {
    return Manager::registerApplication(pid, monitorSharedPtr);
}

bool DFEManager::deregisterApplication(pid_t pid) {
    return Manager::deregisterApplication(pid);
}


/*

void DFEManager::checkApplicationStatus() {
    for(auto it = registeredApplications.begin(); it != registeredApplications.end(); ++it){

    }
}
*/
