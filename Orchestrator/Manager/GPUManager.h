//
//  CPUManager.h
//  UDPOrchestrator
//
//  Created by Raegal on 24/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __GPUManager__
#define __GPUManager__

#include "Manager.h"

typedef struct {
    long energy;
    long watt;
    long utilization;
    long temperature;
} GPUManagerConstraints;

typedef struct {
    long power;
    long maxMemory;
} GPUManagerFeatures;

class GPUManager : public Manager {
    
    GPUManagerConstraints constraints;
    GPUManagerFeatures features;
    
public:
    GPUManager();
    GPUManager(long mId);
    GPUManager(long mId, long power, long memory);
    
    ~GPUManager();

    bool registerApplication(pid_t pid, MonitorInformation monitorSharedPtr);
    bool deregisterApplication(pid_t pid);

    void checkApplicationStatus();

};



#endif /* defined(__CPUManager__) */
