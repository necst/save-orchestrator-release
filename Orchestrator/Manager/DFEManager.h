//
//  CPUManager.h
//  UDPOrchestrator
//
//  Created by Raegal on 24/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __DFEManager__
#define __DFEManager__

#include "Manager.h"

typedef struct {
    long energy;
    long watt;
    long utilization;
    long temperature;
} DFEManagerConstraints;


class DFEManager : public Manager {
    
    DFEManagerConstraints constraints;
    
    
public:
    DFEManager();
    DFEManager(long mId);
    
    ~DFEManager();

    bool registerApplication(pid_t pid, MonitorInformation monitorSharedPtr);
    bool deregisterApplication(pid_t pid);

    
};



#endif /* defined(__CPUManager__) */
