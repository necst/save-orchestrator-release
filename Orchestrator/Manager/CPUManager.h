//
//  CPUManager.h
//  UDPOrchestrator
//
//  Created by Raegal on 24/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __CPUManager__
#define __CPUManager__

#include "Manager.h"

typedef struct {
    long energy;
    long watt;
    long utilization;
    long temperature;
} CPUManagerConstraints;

typedef enum {
    CPUManagerPolicyDefault,
    CPUManagerPolicyPerformance,
    CPUManagerPolicyVersatile,
    CPUManagerPolicyOdroid
}CPUManagerPolicy;


class CPUManager : public Manager {
    
    CPUManagerConstraints constraints;
    
    unsigned int numberOfCPUs;
    
    //Policy
    void defaultPolicy();
    void performancePolicy();
    void odroidPolicy();
    void versatilePolicy();
    
    void setNumberOfThreadForApp(pid_t appId, int numberOfThreads);
    void setAssignedCoresForApp(pid_t appId, std::vector<int> assignedCores);
    
public:
    CPUManager();
    CPUManager(long mId);
    CPUManager(long mId, unsigned int numberOfCPUs);
    
    ~CPUManager();
    
    CPUManagerPolicy policy = CPUManagerPolicyDefault;

    bool registerApplication(pid_t pid, MonitorInformation monitorSharedPtr);
    bool deregisterApplication(pid_t pid);
    
    void checkApplicationStatus();
    
};



#endif /* defined(__CPUManager__) */
