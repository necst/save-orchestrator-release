//
//  CPUManager.cpp
//  UDPOrchestrator
//
//  Created by Raegal on 24/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include "GPUManager.h"



GPUManager::GPUManager():Manager() {
    
}

GPUManager::GPUManager(long mId):Manager(mId) {

}

GPUManager::GPUManager(long mId, long power, long memory):Manager(mId) {
    features.maxMemory = memory;
    features.power = power;
}

GPUManager::~GPUManager() {
    
}


bool GPUManager::registerApplication(pid_t pid, MonitorInformation monitorSharedPtr) {
    return Manager::registerApplication(pid, monitorSharedPtr);
}

bool GPUManager::deregisterApplication(pid_t pid) {
    return Manager::deregisterApplication(pid);
}



void GPUManager::checkApplicationStatus() {
    if (registeredApplications.size()) {
        for (auto app = registeredApplications.begin(); app != registeredApplications.end(); ++app) {
            
            pid_t appId = app->first;
            MonitorInformation appInfo = app->second;
            
            std::vector<int> cores = {0, 1, 2, 3};
            
            appInfo.setNumberOfThreadUsed(1);
        }
    }
}

