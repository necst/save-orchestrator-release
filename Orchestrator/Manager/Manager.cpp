//
//  Manager.cpp
//  UDPOrchestrator
//
//  Created by Raegal on 24/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include <math.h>

#include "Manager.h"

#ifdef __APPLE__
#include "Orchestrator.h"
#else
#include <Orchestrator.h>
#endif
Manager::Manager():Manager(-1) {

}

Manager::Manager(long mId) {
    managerId = mId;
}

Manager::~Manager() {

}

bool Manager::registerApplication(pid_t pid, MonitorInformation monitorSharedPtr) {

    auto registeredApplicationsIt = registeredApplications.find(pid);
    
    if (registeredApplicationsIt == registeredApplications.end()) {
        registeredApplications.emplace(pid,monitorSharedPtr); //insert new object
        
        //int count = registeredApplications[pid].use_count();
        
        return true;
    }
    return false;
}

std::pair<pid_t, MonitorInformation> Manager::getRegisteredApplication(pid_t pid, bool deleteApplication) {
    auto app = std::make_pair(pid, registeredApplications[pid]);
    if(deleteApplication)
        deregisterApplication(pid);
    return app;
}

bool Manager::deregisterApplication(pid_t pid) {
    
    
    auto registeredApplicationsIt = registeredApplications.find(pid);
    
    if (registeredApplicationsIt != registeredApplications.end()) {
        registeredApplicationsIt->second.performanceMonitor.reset();    //release shared memmory
        registeredApplicationsIt->second.heterogeneusScheduleMonitor.reset();    //release shared memmory
        registeredApplications.erase(registeredApplicationsIt);      //delete form array
        //int count = registeredApplications[pid].use_count();
        
        //__sync_synchronize();
        
        //std::cout << "deregisterApplication registeredApplications: " << registeredApplications.size() << " " << &registeredApplications << std::endl;
        
        return true;
    }
    
    
    return false;
}


bool Manager::appNotManageableByCurrentManager(pid_t appPid) {
    bool temp = registeredApplications[appPid].notManageableByCurrentManager;
    return temp;
}

void Manager::checkApplicationStatus() {
    
    for (auto app = registeredApplications.begin(); app != registeredApplications.end(); ++app) {
        
        /*****  Bad Patch for GPU/DFE: Limits their core to 1 for report until
                a true policy are found ***************************************/
        app->second.setNumberOfThreadUsed(1);
        app->second.setNumberOfCoresUsed(1);
        /**********************************************************************/

        
        if ( app->second.notManageableByCurrentManager == true) {
            std::cout << "not manageable" << std::endl;
            continue;
        }
        
        //std::cout << "appNotManageableByCurrentManager registeredApplications " << registeredApplications.size()<< " " << &registeredApplications << std::endl;
        
        bool cycle = app->second.performanceMonitor->shmPtr->cycleSinceLastManagerChange > 5;
        
        double deltaPerf = fabs(app->second.performanceMonitor->shmPtr->averageThroughput - app->second.performanceMonitor->shmPtr->applicationGoal.trhoughput);
        

        std::cout << "deltaPerf:" << deltaPerf << std::endl;
        
        double threshold = app->second.performanceMonitor->shmPtr->applicationGoal.trhoughput * 5 / 100;
        
        bool interval = deltaPerf > threshold;
        
        if (cycle && interval) {
            app->second.notManageableByCurrentManager = true;
        }
    }
}
