//
//  CPUManager.cpp
//  UDPOrchestrator
//
//  Created by Raegal on 24/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include <iostream>
#include <algorithm>

#include <sched.h>

#include "CPUManager.h"



CPUManager::CPUManager():Manager() {
    
}

CPUManager::CPUManager(long mId):Manager(mId) {

}

CPUManager::CPUManager(long mId, unsigned int numberOfCPUs):Manager(mId) {
    this->numberOfCPUs = numberOfCPUs;
}

CPUManager::~CPUManager() {
    
}


#pragma mark - Registration

bool CPUManager::registerApplication(pid_t pid, MonitorInformation monitorSharedPtr) {
    return Manager::registerApplication(pid, monitorSharedPtr);
}

bool CPUManager::deregisterApplication(pid_t pid) {
    return Manager::deregisterApplication(pid);
}


#pragma mark - Policies

void CPUManager::defaultPolicy() {
    if (registeredApplications.size()) {
        for (auto app = registeredApplications.begin(); app != registeredApplications.end(); ++app) {
            
            pid_t appId = app->first;
            MonitorInformation appInfo = app->second;

            std::vector<int> cores;
            
            for (int i =0; i< numberOfCPUs; i++) {
                cores.push_back(i);
            }
            
            setNumberOfThreadForApp(appId, numberOfCPUs);
            setAssignedCoresForApp(appId, cores);
        }
    }
}

void CPUManager::performancePolicy() {
    if (registeredApplications.size()) {
                
        bool bestEffort = false;
        int totalMinCores = 0;
        
        std::map<pid_t, std::pair<double, double>> appRequestedCores;
        
        for (auto app = registeredApplications.begin(); app != registeredApplications.end(); ++app) {
        
            pid_t appId = app->first;
            MonitorInformation appInfo = app->second;
            
            double minThr = appInfo.getMinThroughputConstraint();
            double maxThr = appInfo.getMaxThroughputConstraint();
            
            std::cout << "Application " << app->first << " minThr: " << minThr << " - maxThr: " << maxThr << std::endl;

            double nMin = 0;
            double nMax = this->numberOfCPUs;
            
            if (minThr > 0.0 || maxThr > 0.0) {
                double avgThr = appInfo.getAverageThroughput();

                unsigned int threads = appInfo.getNumberOfThreadUsed();
                //unsigned int cores = appInfo.getNumberOfCoresUsed();
                
                unsigned int n = threads;
                if (n == 0) {
                    n = this->numberOfCPUs;
                }
                
                if (minThr > 0.0) {
                    nMin = minThr / avgThr * n;
                    if (nMin < 1) {
                        nMin = 1;
                    }
                }
                if (maxThr > 0.0) {
                    nMax = maxThr / avgThr * n;
                    if (nMax < 1) {
                        nMax = 1;
                    }
                }
                
                //std::cout << "Application " << app->first << " Min: " << nMin << " - Max: " << nMax << std::endl;
                
                //totalMinCores += nMin;
                
            } else {
                
                // this applications have no constraints....
                nMin = 1;
                nMax = this->numberOfCPUs;

                //bestEffort = true;
            }
            
            totalMinCores += nMin;
            std::cout << "Application " << app->first << " Min: " << nMin << " - Max: " << nMax << std::endl;

            std::pair<double, double> minMaxCores = std::make_pair(nMin, nMax);
            appRequestedCores.emplace(appId, minMaxCores);
        }
        
        int maxCores = this->numberOfCPUs;
//        if (bestEffort)
//            maxCores--;
        
        if (totalMinCores > maxCores) {
            // Scale the number of min cores to the available number of cores
            for (auto app : appRequestedCores) {
                
                pid_t appId = app.first;

                double nMin = std::get<0>(appRequestedCores[appId]);
                double newMin = (double) maxCores * 1.0 / totalMinCores * nMin;
                std::get<0>(appRequestedCores[appId]) = newMin;
            }
        }
        
        int coresIndex = 0;
        
        std::map<pid_t, std::vector<int>> appAssignedCores;
        
        //assign cores to the applications
        for (auto app : appRequestedCores) {
            int appId = app.first;
            
            double nMin = std::get<0>(appRequestedCores[appId]);
            
//            if (nMin == 0) {
//                appAssignedCores[appId].push_back(this->numberOfCPUs - 1);
//            } else {
                int intCores = (int) nMin;
                if (intCores == 0)
                    intCores = 1;
                
                for (int i = 0; i < intCores; i++) {
                    int coreId = coresIndex % maxCores;
                    std::cout << "Assigning core " << coreId << " to " << appId << std::endl;
                    appAssignedCores[appId].push_back(coreId);
                    coresIndex++;
//                }
            }
        }
        
        //Get application performance
        std::vector<std::pair<pid_t, double>> appIdThroughputDelta;
        
        for (auto app = registeredApplications.begin(); app != registeredApplications.end(); ++app) {
            
            pid_t appId = app->first;
            MonitorInformation appInfo = app->second;
            
            double minThr = appInfo.getMinThroughputConstraint();
            double thr = appInfo.getAverageThroughput(); //TODO Conversion from timeUnit to sec
            std::pair<pid_t, double> p;
            
            p.first = appId;
            if (minThr > 0) {
                p.second = minThr - thr;
            } else {
                p.second = -1;
            }
            
            appIdThroughputDelta.push_back(p);
        }
        
        //sort application based on their throughput
        auto cmp = [](std::pair<double, int> const & a, std::pair<double, int> const & b) {
            return b.second < a.second;
        };
        
        std::sort(appIdThroughputDelta.begin(), appIdThroughputDelta.end(), cmp);

        //assign unused cores
        bool assigned = true;
        while (coresIndex < maxCores && assigned) {
            std::cout << "Assigning unused cores" << std::endl;
            assigned = false;
            for (auto app : appIdThroughputDelta) {
                int appId = app.first;
                std::cout << "Considering app " << appId << std::endl;
                if (coresIndex < maxCores) {
                    if (appAssignedCores[appId].size() < appRequestedCores[appId].second) {
                        std::cout << "Assigning Core to app " << appId << std::endl;
                        appAssignedCores[appId].push_back(coresIndex % maxCores);
                        coresIndex++;
                        assigned = true;
                    }
                }
            }
        }
        
        for (auto app : appAssignedCores) {
            
            pid_t appId = app.first;
            std::vector<int> cores = app.second;
            
            std::cout << "App Id " << appId << " cores: " << cores.size() << std::endl;
            
            setNumberOfThreadForApp(appId, (int)cores.size());
            setAssignedCoresForApp(appId, cores);
            std::cout << "Application " << appId << " - Threads " << cores.size() << std::endl;
        }
    } else {
        std::cout << "No CPU Applications" << std::endl;
    }
    
}

void CPUManager::setNumberOfThreadForApp(pid_t appId, int numberOfThreads) {
    MonitorInformation appInfo = registeredApplications[appId];
    appInfo.setNumberOfThreadUsed(numberOfThreads);
}

void CPUManager::setAssignedCoresForApp(pid_t appId, std::vector<int> assignedCores) {
    MonitorInformation appInfo = registeredApplications[appId];
    appInfo.setNumberOfCoresUsed((unsigned int)assignedCores.size());
    
    
    
#ifdef __linux__
    //Taskset
    cpu_set_t mask;
    
    std::cout << "Set mask to zero" << std::endl;
    CPU_ZERO(&mask);
    
    unsigned int cpuCount = assignedCores.size();
    
    std::cout << "Assagning cores" << std::endl;

    for (int i = 0; i < cpuCount ; ++i) {
        std::cout << "Add cores " << assignedCores[i] << " to mask" << std::endl;
        CPU_SET(assignedCores[i], &mask);
    }
    
    int affinity = sched_setaffinity(appId, sizeof(cpu_set_t), &mask);
    if (affinity == -1)
        std::cout << "Error set CPU affinity with" << std::endl;
    else
        std::cout << "Set Affinity correctly" << std::endl;

#endif
    
}


void CPUManager::odroidPolicy() {
    // wait for EMA....
}

void CPUManager::versatilePolicy() {
    if (registeredApplications.size()) {
        for (auto app = registeredApplications.begin(); app != registeredApplications.end(); ++app) {
            
            pid_t appId = app->first;
            MonitorInformation appInfo = app->second;
            
            std::vector<int> cores = {0};
            
            setNumberOfThreadForApp(appId, 1);
            setAssignedCoresForApp(appId, cores);
        }
    }
}


void CPUManager::checkApplicationStatus() {
    switch (policy) {
        case CPUManagerPolicyDefault:
            defaultPolicy();
            break;
            
        case CPUManagerPolicyPerformance:
            performancePolicy();
            break;
            
        case CPUManagerPolicyOdroid:
            odroidPolicy();
            break;
            
        case CPUManagerPolicyVersatile:
            versatilePolicy();
            break;
            
        default:
            break;
    }
}
