//
//  Manager.h
//  UDPOrchestrator
//
//  Created by Raegal on 24/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __Manager__
#define __Manager__
#include <sys/types.h>
#include <map>
#include <mutex>

#ifdef __APPLE__
#include "Defines.h"
#else
#include <Defines.h>
#endif

class Orchestrator;


class Manager {
    
public:
    long managerId;
    
    std::map<pid_t, MonitorInformation> registeredApplications;
    
    Orchestrator * delegate = nullptr;

    Manager();
    Manager(long mId);
    
    ~Manager();
    
    bool registerApplication(pid_t pid, MonitorInformation);
    bool deregisterApplication(pid_t pid);
    
    std::pair<pid_t, MonitorInformation> getRegisteredApplication(pid_t pid, bool deleteApplication = false);
    
    void virtual checkApplicationStatus();
    
    bool appNotManageableByCurrentManager(pid_t appPid);

};




#endif /* defined(__Manager__) */
