//
//  CSocket.cpp
//  UDPOrchestrator
//
//  Created by ing.conti on 15/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include <iostream>
#include "CSocket.h"



CSocket::CSocket():CSocket(DEFAULT_TEST_PORT) {
	
}

#pragma mark - Server

// VERIFY: bad patch for BSD....
#ifndef TCP_USER_TIMEOUT
#define TCP_USER_TIMEOUT 18  // how long for loss retry before timeout [ms]
#endif



CSocket::CSocket(int hostPort, int  timeoutSecs)
{
	m_hostPort = hostPort;
    m_sock = socket(AF_INET, SOCK_STREAM, 0);
	if(m_sock == -1){
		closeAll("Error initializing socket", 0);
		return;
	}
	
	bool option = 1;
    /*
	if( setsockopt(m_sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option)) == -1 ){
		closeAll("Error setting SO_REUSEADDR option", 0);
	}
	
	if( setsockopt(m_sock, SOL_SOCKET, SO_KEEPALIVE, &option, sizeof(option)) == -1 ){
		closeAll("Error setting SO_KEEPALIVE option", 0);
	}
    */
	// VERIFY:
	/*
	The socket option can be made during any state of a TCP connection, but
	is only effective during the synchronized states of a connection
	(ESTABLISHED, FIN-WAIT-1, FIN-WAIT-2, CLOSE-WAIT, CLOSING, or LAST-ACK).
	Moreover, when used with the TCP keepalive (SO_KEEPALIVE) option,
	TCP_USER_TIMEOUT will overtake keepalive to determine when to close a
	connection due to keepalive failure.
	
	The option does not change in anyway when TCP retransmits a packet, nor
	when a keepalive probe will be sent.

	int millisencondsTimeout = 10000;  // user timeout in milliseconds [ms]

	// IPPROTO_TCP on BSD, SOL_TCP on linux

	if( setsockopt(m_sock, SOL_SOCKET, TCP_USER_TIMEOUT, (char*) &millisencondsTimeout, sizeof (millisencondsTimeout)) == -1 ){
		closeAll("Error setting TCP_USER_TIMEOUT option", 0);
	}
*/
	
	// VERIFY:

	
	// to go deep:
/*	struct timeval timeout;
	timeout.tv_sec = timeoutSecs;
	timeout.tv_usec = 0;
	
	// verify if close ALL:
	if (setsockopt (m_sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) == -1)
	{
		closeAll("Error setting SO_RCVTIMEO option", 0);
	}
	
	if (setsockopt (m_sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) == -1)
	{
		closeAll("Error setting SO_SNDTIMEO option", 0);
	}
	
    */
	//
	
}

CSocket::~CSocket(){
	
	closeAll("", 0);
}


bool CSocket::isOpen(void){
	return m_sock != -1;
}


int CSocket::currSocket(void){
	return m_sock;
}

bool CSocket::bind(void){
	
	if (isOpen() == false)
		return false;
	
	//	struct sockaddr_in my_addr; IPV4 ONLY.
	// use sockaddr_storage
	
	//	struct sockaddr_storage storage;
	
	struct sockaddr_in my_addr;
	
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(m_hostPort);
	
	memset(&(my_addr.sin_zero), 0, 8);
	my_addr.sin_addr.s_addr = INADDR_ANY;
	
	if( ::bind( m_sock, (sockaddr*)&my_addr, sizeof(my_addr)) == -1 )
	{
        std::cout << stderr << " Error binding to socket, make sure nothing else is listening on this port " << errno << std::endl;
        return false;
	}
	
	return true;

}

bool CSocket::listen(void){
	if (isOpen() == false)
		return false;

	if(::listen( m_sock, 10) == -1 ){
        std::cout << stderr << " Error listening " << errno << std::endl;
		return false;
	}
	return true;
}

Parameters* CSocket::accept(void)
{
	socklen_t addr_size = 0;
	addr_size = sizeof(sockaddr_in);
	sockaddr_in clientAddr;
    
    int result = ::accept( m_sock, (sockaddr*)&clientAddr, &addr_size);
    
    Parameters * params = (Parameters*)malloc(sizeof(Parameters));
    params->clientSocket = result;
    params->clientAddr = clientAddr;	// TODO sadr should be client IP address... verify... and call it clientAddr
    
	return params;
}



void CSocket::closeAll(std::string msg, int err)
{
	if (msg.length())
        std::cout << msg.c_str() << " " << err << std::endl;
	
	if (m_sock == -1){
		// not yet opened
		return;
	}
	
	close(m_sock);
	m_sock=-1;
}




#pragma mark - Client


bool CSocket::connect(std::string host_name){
    
    struct sockaddr_in my_addr;
    my_addr.sin_family = AF_INET ;
    my_addr.sin_port = htons(m_hostPort);
    
    memset(&(my_addr.sin_zero), 0, sizeof(my_addr.sin_zero) );
    // deprecated my_addr.sin_addr.s_addr = inet_addr(host_name);
	inet_pton(AF_INET, host_name.c_str(), &my_addr.sin_addr);

    
    if( ::connect( currSocket(), (struct sockaddr*)&my_addr, sizeof(my_addr)) == -1 ){
        std::cout << stderr << " Error connecting socket " << errno << std::endl;
        return false;
    }
    
    return true;
}



