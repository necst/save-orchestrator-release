//
//  CSocket.h
//  UDPOrchestrator
//
//  Created by ing.conti on 15/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __CSocket__
#define __CSocket__

#include <fcntl.h>
//#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
//#include <stdio.h>
#include <netinet/in.h>
#include <resolv.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>
//#include <netinet/tcp.h>


#define DEFAULT_TEST_PORT       1101
#define DEFAULT_TIMEOUT         1000

typedef struct Parameters{
    int clientSocket;
    sockaddr_in clientAddr;
}Parameters;


class CSocket {
private:
	int m_sock = -1;
	int m_hostPort = -1;

public:
	CSocket(int hostPort, int  timeoutSecs = DEFAULT_TIMEOUT);
	CSocket();
	~CSocket();
	void closeAll(std::string msg, int err);
	bool isOpen(void);
	int currSocket(void);
	bool bind(void);
	bool listen(void);
    Parameters* accept(void);
    
    bool connect(std::string host_name);
};


#endif /* defined(__UDPOrchestrator__CSocket__) */
