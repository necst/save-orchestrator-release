set( component_SOURCES CSocket.cpp CSocket.h ) # Add the source-files for the component here
# Optionally you can use file glob (uncomment the next line)
# file( GLOB component_SOURCES *.cpp )below

add_library( CSocket ${component_SOURCES} )
