set( component_SOURCES jsoncpp.cpp json/json.h json/json-forwards.h) # Add the source-files for the component here
# Optionally you can use file glob (uncomment the next line)
# file( GLOB component_SOURCES *.cpp )below

add_library( JSONCPP ${component_SOURCES} )
