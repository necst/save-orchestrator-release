//
//  ImageProcessingImplementation.cpp
//  ImageProcessingApp1
//
//  Created by Raegal on 25/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//


#include <stdlib.h>
#include <iostream>

#include "OpenCLDevice.h"

/************* OpenCL ****************************/

volatile int ready = 0;


OpenCLDevice::OpenCLDevice():OpenCLDevice(CL_DEVICE_TYPE_GPU) {

}

OpenCLDevice::OpenCLDevice(cl_device_type dType):OpenCLDevice(dType, "") {
}

OpenCLDevice::OpenCLDevice(cl_device_id deviceId, std::string kernelFileName)
{
    device_id = deviceId;
    openFile(kernelFileName);
    buildProgramForDevice();
}

OpenCLDevice::OpenCLDevice(cl_device_type dType, std::string kernelFileName) {
    
    clType = dType;

    openFile(kernelFileName);
    
    cl_int ret;

    /* Get Platform/Device Information */
    ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    std::cout << "Platform " << platform_id << std::endl;
    fflush(NULL);

    /* Get Device */
    ret = clGetDeviceIDs(platform_id, dType, 1, &device_id, &ret_num_devices);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    
    buildProgramForDevice();
}

void OpenCLDevice::openFile(std::string kernelFileName)
{
    this->kernelFileName = kernelFileName;
    
    if (kernelFileName.compare("") == 0) {
        this->kernelFileName = "./Kernels.cl";
    }
    
    /* Open kernel file */
    FILE *fp;
    
    std::cout << "Almost open file" << std::endl;
    
    /* Load kernel source file */
    fp = fopen(this->kernelFileName.c_str(), "r");
    std::cout << "Open file" << std::endl;
    
    if (!fp) {
        std::cout << stderr << " Failed to load kernel." << std::endl;
        exit(1);
    }
    
    source_str  = (char *)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    
    fclose(fp);
    
    std::cout << "Close file" << std::endl;
    fflush(NULL);
}


void OpenCLDevice::buildProgramForDevice(){
    
    cl_int ret;

    ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_global_work_size), &max_global_work_size, NULL);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    
    ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(max_work_item_size_dimension), max_work_item_size_dimension, NULL);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    
    ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(max_compute_units), &max_compute_units, NULL);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    
    ret = clGetDeviceInfo(device_id, CL_DEVICE_VERSION, 128, clVersion, NULL);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    
    ret = clGetDeviceInfo(device_id, CL_DEVICE_NAME, 128, clDeviceName, NULL);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    
    /* Create context and queue */
    std::cout << "Create Context and Queue" << std::endl;
    
    context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    
    command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    /* Create kernel program from source file*/
    
    std::cout << "Build Programs" << std::endl;
    
    program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
    if (ret != CL_SUCCESS) {
        std::cout << "CL Error:" << ret << std::endl;
    }
    
    ret = clBuildProgram(program, 1, &device_id, NULL, [] (cl_program p, void *user_data) -> void {
        ready = 1;
    } , NULL);
    
    if (ret == CL_BUILD_PROGRAM_FAILURE) {
        std::cout << "Build Programs Failure" << std::endl;
        
        // Determine the size of the log
        size_t log_size;
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
        
        // Allocate memory for the log
        char *log = (char *) malloc(log_size);
        
        // Get the log
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
        
        // Print the log
        printf("%s\n", log);
        
        delete log;
    }
    
    std::cout << "Waiting..." << std::endl;
    
    while(!ready);
    
    std::cout << "Ready" << std::endl;
}




OpenCLDevice::~OpenCLDevice() {
    cl_int ret;
    
    ret = clFlush(command_queue);
    ret = clFinish(command_queue);

    for (auto item : kernels) {
        ret = clReleaseKernel(item.second);
    }
    
    kernels.clear();
    
    ret = clReleaseProgram(program);
    ret = clReleaseCommandQueue(command_queue);
    ret = clReleaseContext(context);
    
    free(source_str);
}


cl_int OpenCLDevice::addKernel(std::string kernelKey){
    cl_int ret;
    
    cl_kernel k = clCreateKernel(program, kernelKey.c_str(), &ret);
    
    if (ret != CL_SUCCESS)
        return ret;
    
    kernels.emplace(kernelKey.c_str(),k);
    
    return ret;
}

cl_int OpenCLDevice::removeKernel(std::string kernelKey) {
    cl_int ret;

    auto item = kernels.find(kernelKey);
    
    if (item != kernels.end()) {
        ret = clReleaseKernel(item->second);
        kernels.erase(item);
    }
    
    return ret;
}

void OpenCLDevice::executeKernel(std::string kernelKey, KernelExectionBlockType executionBlock, KernelCompletionBlockType completionBlock) {
    
    if (executionBlock) {
        executionBlock();
    }
    
    if (completionBlock) {
        completionBlock();
    }
}


/************* End OpenCL ************************/