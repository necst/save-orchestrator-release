//
//  ImageProcessingImplementation.h
//  ImageProcessingApp1
//
//  Created by Raegal on 25/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#ifndef __OpenCLDevice__
#define __OpenCLDevice__

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <vector>
#include <map>
#include <string>
#include <memory>

#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include "CL/cl.h"
#endif


#define MAX_VECTORSIZE 16
#define RGB2GREY_VECTORSIZE 4
#define MOTION_DETECTION_VECTORSIZE 16

//round up globalSize to the nearest multiple of baseSize
#define ROUNDUP(baseSize,globalSize)   ((globalSize%baseSize==0) ? (globalSize) : (globalSize + baseSize - globalSize%baseSize))

#define MAX_SOURCE_SIZE (0x100000)

#define NVIDIA_GPU 0
#define ARM_GPU 1

#ifndef TARGET_PLATFORM
#define TARGET_PLATFORM NVIDIA_GPU
#endif

typedef std::function<void(void)> KernelCompletionBlockType;
typedef std::function<void(void)> KernelExectionBlockType;

typedef struct {
    cl_kernel kernel;
    KernelExectionBlockType executionBlock;
    KernelCompletionBlockType completionBlock;
} KernelInfo;


class OpenCLDevice {
    
    
public:
    
    
    typedef std::shared_ptr<OpenCLDevice> OpenCLDevicePtr;
    typedef std::map<cl_device_type, std::vector<OpenCLDevicePtr>> OpenCLDeviceMap;
    
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;
    cl_context context = NULL;
    cl_command_queue command_queue = NULL;
    cl_program program = NULL;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_uint max_compute_units;
    
    size_t max_global_work_size;
    size_t max_work_item_size_dimension[3];
    
    char clVersion[128];
    char clDeviceName[128];
    
    size_t source_size;
    char *source_str;
    
    std::string kernelFileName;
    
    std::map<std::string,cl_kernel> kernels;
    cl_device_type clType;
    
    OpenCLDevice();
    OpenCLDevice(cl_device_type dType);
    OpenCLDevice(cl_device_type dType, std::string kernelFileName);
    OpenCLDevice(cl_device_id deviceId, std::string kernelFileName);
    ~OpenCLDevice();
    
    void buildProgramForDevice();
    void openFile(std::string kernelFileName);
    
    cl_int addKernel(std::string kernelKey);
    void executeKernel(std::string kernelKey, KernelExectionBlockType executionBlock, KernelCompletionBlockType completionBlock);
    cl_int removeKernel(std::string kernelKey);

    
    //TO DO:
    static OpenCLDeviceMap openCLDeviceList(std::string kernelFileName) {
        cl_int ret;
        
        OpenCLDeviceMap openCLDeviceMap;
        
        cl_uint ret_num_platforms;
        cl_uint ret_num_devices;

        // get all platform
        ret = clGetPlatformIDs(0, NULL, &ret_num_platforms);
        cl_platform_id * platform_ids = (cl_platform_id*) malloc(sizeof(cl_platform_id) * ret_num_platforms);
        ret = clGetPlatformIDs(ret_num_platforms, platform_ids, NULL);
        
        // get all devices for each platforms
        
        for (int i = 0; i < ret_num_platforms; ++i) {
            ret = clGetDeviceIDs(platform_ids[i], CL_DEVICE_TYPE_ALL, 0, NULL, &ret_num_devices);
            cl_device_id * device_ids = (cl_device_id*) malloc(sizeof(cl_device_id) * ret_num_devices);
            ret = clGetDeviceIDs(platform_ids[i], CL_DEVICE_TYPE_ALL, ret_num_devices, device_ids, NULL);

            for (int j = 0; j < ret_num_devices; ++j) {
                cl_device_type device_type;
                ret = clGetDeviceInfo(device_ids[j], CL_DEVICE_TYPE, sizeof(device_type), &device_type, NULL);
                
                //create openCLDevice
                OpenCLDevicePtr openCLDevice = OpenCLDevicePtr (new OpenCLDevice(device_ids[j], kernelFileName));
                openCLDevice->platform_id = platform_ids[i];
                openCLDevice->clType = device_type;

				if(openCLDeviceMap.find(device_type) == openCLDeviceMap.end())
					openCLDeviceMap.emplace(device_type, std::vector<OpenCLDevicePtr>());
                
				openCLDeviceMap.at(device_type).push_back(openCLDevice);
                
            }
        }

        
        fflush(NULL);
        
        return openCLDeviceMap;
    };


};


void print_app();
void initOpenCLDevice(cl_device_type dType);
void destroyOpenCLDevice();




#endif
