//
//  UDPOrchestrator.h
//  UDPOrchestrator
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __Orchestrator__
#define __Orchestrator__

#include <stdio.h>
#include <vector>
#include <string>
#include <map>
#include <mutex>

#ifdef __APPLE__
#include "Defines.h"
#include "CSocket.h"
#include "CPUManager.h"
#include "GPUManager.h"
#include "DFEManager.h"
#include "json.h"
#else
#include <Defines.h>
#include <CSocket.h>
#include <CPUManager.h>
#include <GPUManager.h>
#include <DFEManager.h>
#include <json.h>
#endif

#define COMMAND_IDX     0
#define PID_IDX         1
#define SHM_ID_IDX      2

typedef struct {
    long maxPowerBudget;
    long minPowerBudget;
}SystemUserConstraints;

typedef struct {
    long powerBudgetGoal;
}SystemUserGoal;


typedef enum {
    MonitorTypePerformance,
    MonitorTypeSchedule
}MonitorType;



typedef std::shared_ptr<Manager> ManagerPtr;
typedef std::shared_ptr<CPUManager> CPUManagerPtr;
typedef std::shared_ptr<GPUManager> GPUManagerPtr;
typedef std::shared_ptr<DFEManager> DFEManagerPtr;

typedef MonitorInformation* MonitorInformationPtr;
typedef std::map<pid_t, MonitorInformationPtr> PolicyMap;

typedef std::vector<std::tuple<ChosenScheduleType,int, double>> EstimationVector;



class Orchestrator {

private:
    
    PolicyType chosenPolicy;
    
    std::mutex registeredInfoMutex;
    
    //Monitors
    std::map<pid_t, ManagerPtr> monitorsInformation; //already running
    std::map<pid_t, MonitorInformation> applicationInformation; //only completed registration

    bool registerMonitorSHM(MonitorType monitorType, pid_t pid, int shmIdToAttach);
    bool deregisterMonitorSHM(MonitorType monitorType, pid_t pid);
    
    //Managers
    std::vector<CPUManagerPtr> cpuManagers;
    std::vector<GPUManagerPtr> gpuManagers;
    std::vector<DFEManagerPtr> dfeManagers;
    
    void createManagers(int CPUManagersQuantity, int GPUManagersQuantity, int DFEManagersQuantity);
    
    void createCPUManagers(Json::Value CPUMangers);
    void createGPUManagers(Json::Value GPUMangers);
    void createDFEManagers(Json::Value DFEMangers);
    
    void assignApplicationToManager(pid_t app, ManagerPtr manager, ChosenScheduleType scheduleType);
    bool deregisterApplicationFromManager(pid_t app);
    bool registerApplicationToPlatform(pid_t app, ChosenScheduleType scheduleType);
    void registerApplicationToPlatformAtIndex(pid_t app, ChosenScheduleType scheduleType, int index);

    //ODA Loop
    bool orchestratorODALoopIsRunning = false;
    
    //Server
    void SocketHandler(Parameters * params);
    void ApplicationServer();
    bool parseAction(RegistrationActionType action, pid_t pid, int shmIdToAttach);
    
    //Policy
    void chooseSchedule();
    void simplePolicy(PolicyMap &applications);
    void policyOnlyCPU(PolicyMap &applications);
    void policyOnlyGPU(PolicyMap &applications);
    void policy1(PolicyMap &applications);
    void profilingPolicy(PolicyMap &applications) ;
    
    //Policy utility
    ChosenScheduleType nextProfilingStateForState(ChosenScheduleType state, ChosenScheduleTypeMask mask);
    double getThroughputEstimation(std::vector<double> &v);
    
    EstimationVector getEstimationVectorForApp(MonitorInformationPtr monitorInformation);

    
public:
    Orchestrator();
    Orchestrator(int numberOfCPUManagers, int numberOfGPUManagers, int numberOfDFEManagers);
    Orchestrator(Json::Value CPUManagers, Json::Value GPUManagers, Json::Value DFEManagers);
    Orchestrator(Json::Value CPUManagers, Json::Value GPUManagers, Json::Value DFEManagers, PolicyType policy);
    ~Orchestrator();
    
    void OrchestratorODALoopStart();
    void OrchestratorODALoopStop();

//protocols

    void managerFailToAchieveConstraintsForApp(pid_t pid);

};


#endif /* defined(__UDPOrchestrator__UDPOrchestrator__) */
