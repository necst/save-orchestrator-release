//
//  UDPOrchestrator.cpp
//  UDPOrchestrator
//
//  Created by Raegal on 19/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include <thread>
#include <sys/types.h>
#include <memory.h>
#include <assert.h>
#include <tuple>
#include <algorithm>

#include "Orchestrator.h"


#pragma  mark - Initialization


Orchestrator::Orchestrator():Orchestrator(1,0,0) {
    //default initiaization: Only one CPU manager
}

Orchestrator::Orchestrator(int numberOfCPUManagers, int numberOfGPUManagers, int numberOfDFEManagers) {
    createManagers(numberOfCPUManagers, numberOfGPUManagers, numberOfDFEManagers);

    std::thread applicationServerThread(&Orchestrator::ApplicationServer,this);
    applicationServerThread.detach();
    
    //TODO: Get it from configuraoitn file
    chosenPolicy = PolicyTypePerformance;
}

Orchestrator::Orchestrator(Json::Value CPUManagers, Json::Value GPUManagers, Json::Value DFEManagers) {
    
    //create managers
    createCPUManagers(CPUManagers);
    createGPUManagers(GPUManagers);
    createDFEManagers(DFEManagers);
    
    //start server
    std::thread applicationServerThread(&Orchestrator::ApplicationServer,this);
    applicationServerThread.detach();
    
    //TODO: Get it from configuraoitn file
    chosenPolicy = PolicyTypePerformance;
}

Orchestrator::Orchestrator(Json::Value CPUManagers, Json::Value GPUManagers, Json::Value DFEManagers, PolicyType policy):Orchestrator(CPUManagers, GPUManagers, DFEManagers)
{
    if (policy == PolicyTypeNoPolicy) {
        chosenPolicy = PolicyTypePerformance;
        return;
    }
    
    chosenPolicy = policy;
}


Orchestrator::~Orchestrator() {
}

void Orchestrator::createCPUManagers(Json::Value CPUMangers) {
    
    int CPUManagersQuantity = CPUMangers.size();
    
    for (int i = 0; i < CPUManagersQuantity; ++i) {
        
        unsigned int numberOfCores = CPUMangers[i]["numberOfLogicalCores"].asInt();
        //unsigned int power = CPUMangers[i]["power"].asInt();
        
        CPUManagerPtr cpuManagerPtr = CPUManagerPtr(new CPUManager(i,numberOfCores));
        cpuManagerPtr->policy = CPUManagerPolicyDefault;
        cpuManagers.push_back(cpuManagerPtr);
    }
}

void Orchestrator::createGPUManagers(Json::Value GPUMangers) {
    
    int GPUManagersQuantity = GPUMangers.size();

    for (int i = 0; i < GPUManagersQuantity; ++i) {
        
        unsigned int power = GPUMangers[i]["power"].asInt();
        unsigned int maxMemory = GPUMangers[i]["maxMemory"].asInt();
        
        GPUManagerPtr gpuManager = GPUManagerPtr(new GPUManager(i,power,maxMemory));
        gpuManagers.push_back(gpuManager);
    }
}

void Orchestrator::createDFEManagers(Json::Value DFEMangers) {
    
    int DFEManagersQuantity = 0 /* DFEMangers.size()*/;

    for (int i = 0; i < DFEManagersQuantity; ++i) {
        DFEManagerPtr dfeManager = DFEManagerPtr(new DFEManager(i));
        dfeManagers.push_back(dfeManager);
    }
}


void Orchestrator::createManagers(int CPUManagersQuantity, int GPUManagersQuantity, int DFEManagersQuantity) {
    
    //create CPU Managers
    
    for (int i = 0; i < CPUManagersQuantity; ++i) {
        CPUManagerPtr cpuManagerPtr = CPUManagerPtr(new CPUManager(i));
        cpuManagerPtr->policy = CPUManagerPolicyDefault;
        cpuManagers.push_back(cpuManagerPtr);
    }
    
    //add GPU managers
    for (int i = 0; i < GPUManagersQuantity; ++i) {
        GPUManagerPtr gpuManager = GPUManagerPtr(new GPUManager(i));
        gpuManagers.push_back(gpuManager);
    }
    
    //add DFE managers
    for (int i = 0; i < DFEManagersQuantity; ++i) {
        DFEManagerPtr dfeManager = DFEManagerPtr(new DFEManager(i));
        dfeManagers.push_back(dfeManager);
    }
}

#pragma  mark - ODA Loop



void Orchestrator::OrchestratorODALoopStart() {
    orchestratorODALoopIsRunning = true;
    
    //a map for each managers or only one?
    
    struct timespec sleepTime;
    sleepTime.tv_sec = 0;
    sleepTime.tv_nsec = 500000000L;
    
    while (orchestratorODALoopIsRunning) {
        
        //critical section: shared map, protect it
        registeredInfoMutex.lock();
        
        //orchestrator decide
        chooseSchedule();
        
        //managers check status of the application
        for (auto manager = cpuManagers.begin(); manager != cpuManagers.end(); ++manager) {
            manager->get()->checkApplicationStatus();
        }
        
        for (auto manager = gpuManagers.begin(); manager != gpuManagers.end(); ++manager) {
            manager->get()->checkApplicationStatus();
        }
        
        for (auto manager = dfeManagers.begin(); manager != dfeManagers.end(); ++manager) {
            manager->get()->checkApplicationStatus();
        }
        
        registeredInfoMutex.unlock();
        //end critical seciton
        
        std::cout << "tick" << std::endl;
        nanosleep(&sleepTime, NULL);
    }
}

void Orchestrator::OrchestratorODALoopStop() {
    orchestratorODALoopIsRunning = false;
}


#pragma  mark - Managers


void Orchestrator::assignApplicationToManager(pid_t app, ManagerPtr manager, ChosenScheduleType scheduleType) {
    
    bool registered = manager->registerApplication(app, applicationInformation[app]);
    if (registered) {
        //map applicaiton to manager
        monitorsInformation.emplace(app,manager);
        //set schedule type inside shm
        applicationInformation[app].setChosenScheduleType(scheduleType);
        //unlock the application
        applicationInformation[app].appState = AppStateDispatched;
        applicationInformation[app].semaphorePtr->post();
    }
}



bool Orchestrator::deregisterApplicationFromManager(pid_t app) {
    
    auto monitorsInformationIt = monitorsInformation.find(app);
    
    if (monitorsInformationIt != monitorsInformation.end()) {
        monitorsInformationIt->second->deregisterApplication(app);
        monitorsInformation.erase(monitorsInformationIt);
        return true;
    }
    
    return false;
}

bool Orchestrator::registerApplicationToPlatform(pid_t app, ChosenScheduleType scheduleType){
    switch (scheduleType) {
        case CPUScheduleType:
            //return registerApplicationToCPUManagers(app);
            break;

        case GPUScheduleType:
            //return registerApplicationToGPUManagers(app);
            break;

        default:
            break;
    }
    
    return false;
}

void Orchestrator::registerApplicationToPlatformAtIndex(pid_t app, ChosenScheduleType scheduleType, int index) {
    switch (scheduleType) {
        case CPUScheduleType:
            assignApplicationToManager(app, cpuManagers[index], scheduleType);
            return;
            
        case GPUScheduleType:
            assignApplicationToManager(app, gpuManagers[index], scheduleType);
            return;
            
        case DFEScheduleType:
            assignApplicationToManager(app, dfeManagers[index], scheduleType);
            return;
            
        default:
            break;
    }
    
    return;
}

#pragma  mark - Policy


void Orchestrator::simplePolicy(PolicyMap &applications) {
    
    for (auto app = applications.begin(); app != applications.end(); ++app) {
        
        pid_t appPid = app->first;
        MonitorInformationPtr monitorInformation = app->second;
        
        switch (monitorInformation->appState) {
                
            case AppStateRegistered:
                //for now always dispatch first on CPU
                //registerApplicationToCPUManagers(appPid);
                break;
                
            case AppStateDispatched:
                //check if the throughput is achieved or the throughput rise
                //if not change resources
                
                if (monitorInformation->heterogeneusScheduleMonitor->shmPtr->fastestImplementation == NOScheduleType) {
                    break;
                }
                
                if (monitorInformation->heterogeneusScheduleMonitor->shmPtr->slowestImplementation == NOScheduleType) {
                    break;
                }
                
                if (monitorInformation->heterogeneusScheduleMonitor->shmPtr->fastestImplementation == monitorInformation->heterogeneusScheduleMonitor->shmPtr->slowestImplementation) {
                    break;
                }
                
                monitorInformation->notManageableByCurrentManager = monitorsInformation[appPid]->appNotManageableByCurrentManager(appPid);
                
                if (monitorInformation->notManageableByCurrentManager) {
                    
                    monitorInformation->setChosenScheduleType(NOScheduleType);
                    
                    bool deregistered = deregisterApplicationFromManager(appPid);
                    
                    if (!deregistered) {
                        std::cout << "AppStateDispatched: Failed deregister application: " << appPid << std::endl;
                    }
                    
                    ChosenScheduleType schedule;
                    if (monitorInformation->performanceMonitor->shmPtr->averageThroughput < monitorInformation->performanceMonitor->shmPtr->applicationGoal.trhoughput )
                        schedule = monitorInformation->heterogeneusScheduleMonitor->shmPtr->fastestImplementation;
                    else
                        schedule = monitorInformation->heterogeneusScheduleMonitor->shmPtr->slowestImplementation;
                    
                    monitorInformation->notManageableByCurrentManager = false;
                    
                    bool registered = registerApplicationToPlatform(appPid, schedule);
                    
                    if (!registered) { //if no other manager can take it
                        monitorInformation->appState = AppStateRegistered;
                    }
                    else {
                        monitorInformation->setChosenScheduleType(schedule);
                        monitorInformation->performanceMonitor->shmPtr->cycleSinceLastManagerChange = 0;
                        monitorInformation->semaphorePtr->post();
                    }
                }
                
            default:
                break;
        }
    }
    
}

void Orchestrator::policyOnlyCPU(PolicyMap &applications) {
    for (auto app = applications.begin(); app != applications.end(); ++app) {
        
        pid_t appPid = app->first;
        MonitorInformationPtr monitorInformation = app->second;
        
        switch (monitorInformation->appState) {
                
            case AppStateRegistered:
                //always dispatch on CPU
                
                assignApplicationToManager(appPid, cpuManagers[0], CPUScheduleType);
                
                //registerApplicationToCPUManagers(appPid);
                break;
            default:
                break;
        }
    }
}

void Orchestrator::policyOnlyGPU(PolicyMap &applications) {
    for (auto app = applications.begin(); app != applications.end(); ++app) {
        
        pid_t appPid = app->first;
        MonitorInformationPtr monitorInformation = app->second;
        
        switch (monitorInformation->appState) {
                
            case AppStateRegistered:
                //always dispatch on GPU
                
                assignApplicationToManager(appPid, gpuManagers[0], GPUScheduleType);

                //registerApplicationToGPUManagers(appPid);
                break;
            default:
                break;
        }
    }
}


double Orchestrator::getThroughputEstimation(std::vector<double> &v) {
    
    double thrEstimation = v[0];
    
    for(auto elem = v.begin() + 1; elem != v.end(); elem++)
        thrEstimation = ALPHA * (*elem) + (1 - ALPHA) * thrEstimation;
    
    return thrEstimation;
}


EstimationVector Orchestrator::getEstimationVectorForApp(MonitorInformationPtr monitorInformation) {
    
    //get estimations vector
    EstimationVector estimations;
    
    ChosenScheduleTypeMask availableImplementationMask = monitorInformation->getAvailableImplementationMask();
    ChosenScheduleType state = nextProfilingStateForState(monitorInformation->profilingInformation.profilingState, availableImplementationMask) ;
    
    for (; state != NOScheduleType; state = nextProfilingStateForState(state,availableImplementationMask)) {
        size_t size = monitorInformation->profilingInformation.clustersInformation[state].size();
        
        for (int i = 0; i < size; i++) {
            double estimation = getThroughputEstimation(monitorInformation->profilingInformation.clustersInformation[state][i]);
            
            estimations.push_back(std::make_tuple(state,i,estimation));
        }
    }
    //order it
    
    std::sort(estimations.begin(), estimations.end(), [](const std::tuple<ChosenScheduleType, int, double> & a, const std::tuple<ChosenScheduleType, int, double> & b) -> bool {
        
        auto aValue = std::get<2>(a);
        auto bValue = std::get<2>(b);
        
        return aValue < bValue;
    });

    return estimations;
}


void Orchestrator::policy1(PolicyMap &applications) {
    
    std::vector<std::pair<pid_t, double>> applicationsThroughputDelta;
    
    //sort applications
    for (auto elem = applications.begin(); elem != applications.end(); elem++) {
        double elemThr = elem->second->getAverageThroughput();
        double elemMin = elem->second->getMinThroughputConstraint();
        double elemDiff = elemMin - elemThr;
        
        applicationsThroughputDelta.push_back(std::make_pair(elem->first, elemDiff));
    }
    
    std::sort(applicationsThroughputDelta.begin(), applicationsThroughputDelta.end(), [](const std::pair<pid_t, double> & a, const std::pair<pid_t, double> & b) -> bool {
        return a.second < b.second;
    });
    
    
    for (auto app = applicationsThroughputDelta.begin(); app != applicationsThroughputDelta.end(); ++app) {
        
        pid_t appPid = app->first;
        MonitorInformationPtr monitorInformation = applications[appPid];
        EstimationVector estimations;
        
        switch (monitorInformation->appState) {
                
            case AppStateDispatched:
                
                //check if the aplication has increased its heartbeat
                if (monitorInformation->appGlobalCycleCount < monitorInformation->getGlobalCycleCount()) {
                    
                    monitorInformation->appGlobalCycleCount = monitorInformation->getGlobalCycleCount();
                    //take instant trhoughput and update estimation of the application
                    double instantThroughput = monitorInformation->getInstantThroughput();
                    ChosenScheduleType schedule = monitorInformation->getScheduledType();
                    
                    long index = monitorsInformation[appPid]->managerId;
                    
                    auto first = monitorInformation->profilingInformation.clustersInformation[schedule][index].begin();
                    
                    monitorInformation->profilingInformation.clustersInformation[schedule][index].push_back(instantThroughput);
                    monitorInformation->profilingInformation.clustersInformation[schedule][index].erase(first);
                    
                    //if negative throughput is higher than the minimum
                    if (app->second < 0) {
                        break;
                    }
                    
                    monitorInformation->setChosenScheduleType(NOScheduleType);
                    deregisterApplicationFromManager(appPid);
                }
                
            case AppStateRegistered:
                
                estimations = getEstimationVectorForApp(monitorInformation);
                
                if (estimations.size() == 0) {
                    std::cout << "No implementations!!" << std::endl;
                    break;
                }
                
                if (estimations.size() == 1) {
                    registerApplicationToPlatformAtIndex(appPid, std::get<0>(estimations[0]), 0);
                    break;
                }
                
                for (auto elem = estimations.begin(); elem != estimations.end(); elem++) {
                    
                    ChosenScheduleType type = std::get<0>(*elem);
                    int index = std::get<1>(*elem);
                    double est = std::get<2>(*elem);
                    
                    //Assign to the first implementation that meet the constraints, for now...
                    if (est > monitorInformation->getMinThroughputConstraint()) {
                        registerApplicationToPlatformAtIndex(appPid, type, index);
                        return;
                    }
                }
                
                std::cout << "No available resources to run thi kernel." << std::endl;
                std::cout << "Orchestrator will terminate it." << std::endl;
                
                
                monitorInformation->setHasNoResourcesFlag(true);
                monitorInformation->semaphorePtr->post();
                
                deregisterMonitorSHM(MonitorTypePerformance, appPid);
                deregisterMonitorSHM(MonitorTypeSchedule, appPid);
                
                break;
                
            default:
                break;
        }
    }
}

ChosenScheduleType Orchestrator::nextProfilingStateForState(ChosenScheduleType state, ChosenScheduleTypeMask mask) {
    
    switch (state) {
        case NOScheduleType:
            if (mask & CPUScheduleType)
                return CPUScheduleType;
            else if (mask & GPUScheduleType)
                return GPUScheduleType;
            else if (mask & DFEScheduleType)
                return DFEScheduleType;
            else
                return NOScheduleType;
            
        case CPUScheduleType:
            if (mask & GPUScheduleType)
                return GPUScheduleType;
            else if (mask & DFEScheduleType)
                return DFEScheduleType;
            else
                return NOScheduleType;
            
        case GPUScheduleType:
            if (mask & DFEScheduleType)
                return DFEScheduleType;
            else
                return NOScheduleType;
            
        case DFEScheduleType:
            return NOScheduleType;
        default:
            break;
    }
    
    return NOScheduleType;
}

void Orchestrator::profilingPolicy(PolicyMap &applications) {
    
    for (auto app = applications.begin(); app != applications.end(); ++app) {

        pid_t appPid = app->first;
        MonitorInformationPtr monitorInformation = app->second;
        
        ChosenScheduleType state = monitorInformation->profilingInformation.profilingState;
        ChosenScheduleTypeMask availableImplementationMask = monitorInformation->getAvailableImplementationMask();

        int profilingIndex = monitorInformation->profilingInformation.profilingIndex;
        size_t historySize;
        int cycleSinceLastManagerChange = monitorInformation->performanceMonitor->shmPtr->cycleSinceLastManagerChange;
        
        switch (monitorInformation->appState) {
                
            case AppStateRegistered:
                
                //register the application to the first availbale platform
                profilingIndex = 0;
                
                state = nextProfilingStateForState(state, availableImplementationMask);
                
                if (state == NOScheduleType) {
                    std::cout << "No Implementation Available" << std::endl;
                    break;
                }
                
                monitorInformation->profilingInformation.profilingState = state;

                registerApplicationToPlatformAtIndex(appPid, state, profilingIndex);
                
                break;
                
            case AppStateDispatched:
            
                historySize = monitorInformation->profilingInformation.clustersInformation[state][profilingIndex].size();
                
                // if the application has incremented its heartbeat
                if (cycleSinceLastManagerChange > historySize) {
                    
                    //if the history size not reached the time window size
                    if (historySize < TIME_WINDOW) {
                        
                        //take instant trhoughput of the application
                        double instantThroughput = monitorInformation->getInstantThroughput();
                        monitorInformation->profilingInformation.clustersInformation[state][profilingIndex].push_back(instantThroughput);
                    } else {
                        
                        //if the time window is reached deregister frome the current manager
                        monitorInformation->setChosenScheduleType(NOScheduleType);
                        bool deregistered = deregisterApplicationFromManager(appPid);
                        
                        //check if there is another manager of the same cluster type
                        size_t numberOfCluster = monitorInformation->profilingInformation.clustersInformation[state].size();
                        profilingIndex++;
                        
                        if (profilingIndex < numberOfCluster) {
                            //if yes register to it
                            monitorInformation->profilingInformation.profilingIndex = profilingIndex;
                            registerApplicationToPlatformAtIndex(appPid, state, profilingIndex);
                        } else {
                            
                            //if not register it to the first one of the next cluster
                            
                            profilingIndex = 0;
                            monitorInformation->profilingInformation.profilingIndex = 0;
                            state = nextProfilingStateForState(state, availableImplementationMask);
                            
                            monitorInformation->profilingInformation.profilingState = state;
                            
                            monitorInformation->performanceMonitor->shmPtr->cycleSinceLastManagerChange = 0;
                            
                            // Profiling ended
                            if (state == NOScheduleType) {
                                monitorInformation->heterogeneusScheduleMonitor->shmPtr->hasToBeProfiled = false;
                                monitorInformation->appState = AppStateRegistered;
                                break;
                            }
                            
                            registerApplicationToPlatformAtIndex(appPid, state, profilingIndex);
                        }
                    }
                }
                
                break;
                
            default:
                break;
        }
    }
}

void Orchestrator::chooseSchedule() {
    
    if (applicationInformation.size()) {
        
        PolicyMap appToBeProfiled;
        PolicyMap appToBeScheduled;
        PolicyMap appToBeDeleted;
        
        for (auto app = applicationInformation.begin(); app != applicationInformation.end(); ++app) {
            pid_t appId = app->first;
            
            MonitorInformationPtr monitorPtr = &applicationInformation[appId];
            
            if (monitorPtr->appState != AppStateRegistering) {
                
                int perfNumberAttach = monitorPtr->performanceMonitor->numberOfProcessAttachedToSHMObject(appId);
                int hetNumberAttach = monitorPtr->heterogeneusScheduleMonitor->numberOfProcessAttachedToSHMObject(appId);
                
                if (perfNumberAttach == 1 && hetNumberAttach == 1) {
                    std::cout << "App " << appId << " is crashed. Memory will be freed." << std::endl;
                    appToBeDeleted[appId] = monitorPtr;
                    continue;
                }
                
                std::cout << "To be profiled? " << monitorPtr->heterogeneusScheduleMonitor->shmPtr->hasToBeProfiled << std::endl;
                
                if (monitorPtr->heterogeneusScheduleMonitor->shmPtr->hasToBeProfiled && chosenPolicy != PolicyTypeOnlyCPU && chosenPolicy != PolicyTypeOnlyGPU) {
                    std::cout << "App to be profiled " << appId << std::endl;
                    appToBeProfiled[appId] = monitorPtr;
                } else {
                    std::cout << "App to be runned " << appId << std::endl;
                    appToBeScheduled[appId] = monitorPtr;
                }
            }
        }
        
        //find all the application correctly registered
        if (appToBeProfiled.size()) {
            profilingPolicy(appToBeProfiled);
        }
        
        if (appToBeScheduled.size()) {
            switch (chosenPolicy) {
                case PolicyTypeSimplePolicy:
                    //simplePolicy(appToBeScheduled);
                    break;
                    
                case PolicyTypePerformance:
                    policy1(appToBeScheduled);
                    break;
                    
                case PolicyTypePower:
                    break;
                    
                case PolicyTypeOnlyCPU:
                    policyOnlyCPU(appToBeScheduled);
                    break;
                
                case PolicyTypeOnlyGPU:
                    policyOnlyGPU(appToBeScheduled);
                    break;
                    
                default:
                    break;
            }
        }
        
        if (appToBeDeleted.size()) {
            for (auto app = appToBeDeleted.begin(); app != appToBeDeleted.end(); ++app) {
                pid_t appId = app->first;
                
                deregisterApplicationFromManager(appId);
                applicationInformation.erase(appId);
                
                deregisterMonitorSHM(MonitorTypePerformance, appId);
                deregisterMonitorSHM(MonitorTypeSchedule, appId);
                
            }
            
            appToBeDeleted.clear();
        }
        
    }
}





#pragma  mark - Server

void Orchestrator::ApplicationServer() {
    
    CSocket socket(DEFAULT_TEST_PORT);
    
    //open socket
    if (socket.isOpen()==false) {
        socket.closeAll("not open", 0);
        exit(1);
    }
    
    //bind socket
    bool bound = socket.bind();
    if (bound == false) {
        socket.closeAll("not bound", 0);
        exit(1);
    }
    
    //listening socket
    bool listening = socket.listen();
    if (listening == false) {
        socket.closeAll("not listening", 0);
        exit(1);
    }

    //Now lets do the server stuff
    
    while(true) {
        std::cout << "waiting for a connection" << std::endl;
        
        Parameters * params = socket.accept(); // params must be deallocated with free(). we do it in thread.
        
        if( params->clientSocket != -1) {
            
            struct in_addr inaddr = params->clientAddr.sin_addr;
            char buf[INET_ADDRSTRLEN];
            if (inet_ntop(AF_INET, &inaddr, buf, sizeof(buf)) != NULL)
                std::cout << "---------------------\nReceived connection from " << buf << std::endl;
            else
                std::cout << "inet error" << std::endl;
            
            std::thread t(&Orchestrator::SocketHandler,this, params);
            t.detach(); //thread must dealocate
        }
        else {
            std::cout << stderr << " Error accepting " << errno << std::endl;
            // clear memory:
            free(params);
        }
    }
}


void Orchestrator::SocketHandler(Parameters * params) {
    
    char buffer[1024];
    int buffer_len = 1024;
    size_t bytecount;
    
    //receive socket
    memset(buffer, 0, buffer_len);
    bytecount = recv(params->clientSocket, buffer, buffer_len, 0);
    if (bytecount == -1) { // error:
        std::cout << stderr << " Error receiving data " << errno << std::endl;
    }
    else { // go on:
        std::cout << "Received bytes " << bytecount << std::endl;
        std::cout << "Received string " << buffer << std::endl;
        
        //shmId is the last component of thge string
        std::string temp = buffer;
        std::string token = "_";
        std::vector<std::string> separatedStrings = stringsSeparatedByToken(temp, token.c_str());
        
        //separatedStrings[0] command
        //separatedStrings[1] pid
        //separatedStrings[2] shared memory id
        
        std::string command = separatedStrings[COMMAND_IDX];
        RegistrationActionType regAction = registrationActionTypeFromString(command);

        pid_t pid = std::stoi(separatedStrings[PID_IDX]);
        int shmIdToAttach = std::stoi(separatedStrings[SHM_ID_IDX]);
        
        bool registered = parseAction(regAction, pid, shmIdToAttach);
        
        if (!registered) {
            return;
        }
        
        //if all ok send the answer
        bytecount = send(params->clientSocket, buffer, strlen(buffer), 0);
        if (bytecount == -1) { // error:
             std::cout << stderr << " Error sending data " << errno << std::endl;
            //TODO: deregister
        }
        else {
            std::cout << "Sent bytes " << bytecount << std::endl;
        }
    }
    
    //free params
    close(params->clientSocket);
    free(params);
}


bool Orchestrator::registerMonitorSHM(MonitorType monitorType, pid_t pid, int shmIdToAttach){
    //find the monitor to be added
    auto registeredInfoIt = applicationInformation.find(pid);

    if(registeredInfoIt == applicationInformation.end()) {
        //add new shared object for application monitor

        MonitorInformation mInfo;

        if(monitorType == MonitorTypePerformance)
            mInfo.performanceMonitor = PerformanceMonitorSHMPtr (new PerformanceMonitorSHM(shmIdToAttach));
        else
            mInfo.heterogeneusScheduleMonitor = ScheduleMonitorSHMPtr (new ScheduleMonitorSHM(shmIdToAttach));

        mInfo.semaphorePtr = BinarySemaphorePtr(new BinarySemaphore());
        mInfo.appState = AppStateRegistering;
        applicationInformation.emplace(pid,mInfo);

    } else {
        MonitorInformation *mInfo = &applicationInformation[pid];

        if ((monitorType == MonitorTypePerformance && mInfo->performanceMonitor) ||
            (monitorType == MonitorTypeSchedule && mInfo->heterogeneusScheduleMonitor))
            //object already registered for this service
            return false;

        if(monitorType == MonitorTypePerformance)
            mInfo->performanceMonitor = PerformanceMonitorSHMPtr (new PerformanceMonitorSHM(shmIdToAttach));
        else
            mInfo->heterogeneusScheduleMonitor = ScheduleMonitorSHMPtr (new ScheduleMonitorSHM(shmIdToAttach));
        
        mInfo->semaphorePtr->attach(std::to_string(pid));

        mInfo->appState = AppStateRegistered;
        
        //initialize profiling information
        for (int i = 0; i < cpuManagers.size(); i++) {
            mInfo->profilingInformation.clustersInformation[CPUScheduleType][i];
        }
        
        for (int i = 0; i < gpuManagers.size(); i++) {
            mInfo->profilingInformation.clustersInformation[GPUScheduleType][i];
        }
        
        for (int i = 0; i < dfeManagers.size(); i++) {
            mInfo->profilingInformation.clustersInformation[DFEScheduleType][i];
        }
    }

    return true;

}

bool Orchestrator::deregisterMonitorSHM(MonitorType monitorType, pid_t pid){
    //find the monitor to be deleted

    auto registeredInfoIt = applicationInformation.find(pid);

    if (registeredInfoIt != applicationInformation.end()) {
        //delete it
        MonitorInformation *mInfo = &applicationInformation[pid];
        auto dispatchedApp = monitorsInformation.find(pid);
        //no more execution, just in case
        mInfo->setChosenScheduleType(NOScheduleType);
        
        switch (mInfo->appState) {
        case AppStateDispatched:
            if(dispatchedApp != monitorsInformation.end())
                deregisterApplicationFromManager(pid);
        case AppStateRegistered:
            if(monitorType == MonitorTypePerformance)
                mInfo->performanceMonitor.reset();
            else
                mInfo->heterogeneusScheduleMonitor.reset();
            mInfo->appState = AppStateDeregistering;
            break;
        case AppStateDeregistering: //remove from vector
            if(monitorType == MonitorTypePerformance)
                mInfo->performanceMonitor.reset();
            else
                mInfo->heterogeneusScheduleMonitor.reset();
            applicationInformation.erase(pid);
            break;
        default:
            break;
        }

        return true;
    }

    return false;
}


bool Orchestrator::parseAction(RegistrationActionType action, pid_t pid, int shmIdToAttach) {
    
    bool successfully = false;
    
    //shared map, protect it
    registeredInfoMutex.lock();
        
    switch (action) {
        case ActionTypePerformanceMonitorRegistration:
            successfully = registerMonitorSHM(MonitorTypePerformance,pid,shmIdToAttach);
            break;
            
        case ActionTypePerformanceMonitorDeregistration:
            successfully = deregisterMonitorSHM(MonitorTypePerformance, pid);
            break;

        case ActionTypeScheduleMonitorRegistration:
            successfully = registerMonitorSHM(MonitorTypeSchedule,pid,shmIdToAttach);
            break;
            
        case ActionTypeScheduleMonitorDeregistration:
            successfully = deregisterMonitorSHM(MonitorTypeSchedule, pid);
            break;
            
        default:
            break;
    }
    
    //release lock
    registeredInfoMutex.unlock();
    
    return successfully;
}
