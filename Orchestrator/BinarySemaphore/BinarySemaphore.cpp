//
//  CSocket.cpp
//  UDPOrchestrator
//
//  Created by ing.conti on 15/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#include <cerrno>
#include <iostream>
#include "BinarySemaphore.h"



BinarySemaphore::BinarySemaphore() {
}


BinarySemaphore::~BinarySemaphore() {
    sem_close(semaphoreName);
    sem_unlink(name);
    
    //semaphoreName.reset();
}

bool BinarySemaphore::wait() {
    int value = sem_wait(semaphoreName);
    
    if (value == 0)
        return true;

    return false;
}


bool BinarySemaphore::open(std::string sname) {
    
    sprintf(name, "%s",sname.c_str());
    
    semaphoreName = sem_open(sname.c_str(), O_CREAT | O_EXCL, S_IRUSR | S_IWUSR,0);
    
    if(semaphoreName == SEM_FAILED)
    {
        perror("unable to create semaphore");
        sem_unlink(sname.c_str());
        return false;
    }
    
    return true;
}


bool BinarySemaphore::attach(std::string sname) {
    
    sprintf(name, "%s",sname.c_str());

    semaphoreName = sem_open(sname.c_str(), 0);
    
    if(semaphoreName == SEM_FAILED)
    {
        perror("unable to create semaphore");
        sem_unlink(sname.c_str());
        return false;
    }
    
    return true;

}


bool BinarySemaphore::post() {

    if (sem_post(semaphoreName) == 0)
        return true;

    return false;
}
