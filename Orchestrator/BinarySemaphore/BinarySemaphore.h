//
//  CSocket.h
//  UDPOrchestrator
//
//  Created by ing.conti on 15/02/15.
//  Copyright (c) 2015 ing.conti. All rights reserved.
//

#ifndef __BinarySemaphore__
#define __BinarySemaphore__

#include <string>
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

typedef enum {
    SemaphoreSharedNo,
    SemaphoreSharedYes
}SemaphoreShared;

class BinarySemaphore {
private:
    sem_t * semaphoreName;
    
    char name[20];

public:
    BinarySemaphore();
    ~BinarySemaphore();

    bool wait();
    bool post();
    
    bool open(std::string name);
    bool attach(std::string sname);

};


#endif
