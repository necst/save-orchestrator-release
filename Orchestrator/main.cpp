#include <iostream>
#include <fstream>
#include <getopt.h>


#ifdef __APPLE__
#include "json.h"
#include "Orchestrator.h"
#include "ReportMonitor.h"
#else
#include <json.h>
#include <Orchestrator.h>
#endif

#include "OpenCLDevice.h"

/***************** Options ***********************/

typedef struct globalArgs_t{
    PolicyType policy = PolicyTypeNoPolicy;
} GlobalArgs;

static const char *optString = "p:";

static struct option longOpts[] = {
    { "policy", required_argument, NULL, 'p' },
};


GlobalArgs globalArgs;


void parseCommmand(int argc, char** argv){
    int longIndex;
    int opt;
    
    while((opt = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1 ) {
        switch( opt ) {
                
            case 'p':
                globalArgs.policy = policyTypeFromString(optarg);
                break;
                
            default:
                break;
        }
    }
}

/***************** End Options ********************/


//Orchestrator Main
int main(int argc, char** argv)
{
    parseCommmand(argc, argv);
    
    
    //parse config.json
    Json::Value root;   // will contains the root value after parsing.
    Json::Reader reader;
    std::ifstream test("config.json", std::ifstream::binary);
    bool parsingSuccessful = reader.parse( test, root, false );
    if ( !parsingSuccessful ) {
        // report to the user the failure and their locations in the document.
        std::cout << reader.getFormatedErrorMessages() << std::endl;
    }
    
    //take from json number of hardware resources manager
    Json::Value managers = root["Managers"];
    Json::Value CPUManagers = managers["CPUManagers"];
    Json::Value GPUManagers = managers["GPUManagers"];
    Json::Value DFEManagers = managers["DFEManagers"];

    Orchestrator orchestrator(CPUManagers, GPUManagers, DFEManagers, globalArgs.policy);
    
    orchestrator.OrchestratorODALoopStart();
    
    return 0;
}